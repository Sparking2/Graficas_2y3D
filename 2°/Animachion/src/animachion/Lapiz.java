/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animachion;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

/**
 *
 * @author Cortes Marquez
 */
public class Lapiz extends JFrame{
    
    private final BufferedImage buffer;
    private final Graphics graPixel;
    
    public Lapiz(){
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
    }
    
    public void putPixel(int x, int y, Color c){
        buffer.setRGB(0, 0, c.getRGB());
        this.getGraphics().drawImage(buffer, x, y, this);
    }
    
    public void DibujarRectangulo(int x0,int y0, int x1, int y1){
        
         DibujarBresenham(x0,y0,x1,y0);
         DibujarBresenham(x1,y0,x1,y1);
         DibujarBresenham(x1,y1,x0,y1);
         DibujarBresenham(x0,y1,x0,y0);       
     }
    
    public void DibujarBresenham (int x0,int y0, int x1, int y1)
    {
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1)){
            
            int p = 2 * rodzil;
            if (p > -dy)
            {
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            if (p < dx) {
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
        }
        putPixel(x0, y0, Color.BLACK);  
        }
    }
    
    public void CirculoBresenham(int Xc, int Yc, int Rd){
        
        putPixel(Xc, Yc + Rd, Color.BLUE);
        double p = 3 - 2* Rd;
        
             for (int xk = 0,yk = Rd;xk <= yk; xk+=1){
                if(p < 0){
                    ocholados(Xc,Yc,xk,yk);
                    p += 2*xk + 3;                       
                }else{
                    yk -= 1;
                    ocholados(Xc,Yc,xk,yk);
                    p += 2 * ( xk -yk) + 5;
                }
        } 
    }
    
    public void ocholados(int Xc, int Yc, int xk, int yk){
        
          putPixel((int)Xc + xk,(int)Yc + yk,Color.BLUE); //1
          putPixel((int)Xc - xk,(int)Yc + yk,Color.BLUE); // 8
          putPixel((int)Xc + xk,(int)Yc - yk,Color.BLUE); // 4
          putPixel((int)Xc - xk,(int)Yc - yk,Color.BLUE); // 5
          putPixel((int)Xc + yk,(int)Yc + xk,Color.BLUE); // 2
          putPixel((int)Xc - yk,(int)Yc + xk,Color.BLUE); // 7
          putPixel((int)Xc + yk,(int)Yc - xk,Color.BLUE); // 3
          putPixel((int)Xc - yk,(int)Yc - xk,Color.BLUE); // 6
    }
}
