/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animachion;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;
import javax.swing.JFrame;

public class Animachion extends JFrame{
    
    JFrame frame;
    Buffer figura;
    BufferedImage buffer,bufferRespaldo;
    int a=0,b=0,c=700,d=500;
    
    public Animachion(){
        frame = new JFrame("Animachion");
        buffer = new BufferedImage(700, 500, BufferedImage.TYPE_INT_RGB);
        bufferRespaldo = new BufferedImage(700,500,BufferedImage.TYPE_INT_RGB);
        frame.setSize(700,500);       
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        figura = new Buffer();

        System.out.println("no");
        repaint();
     
        
    }
    
    public void repaint(){
        int i = 0;
        int gyro = 1;
        Random rnd = new Random();
        //bufferRespaldo = frame.getBufferStrategy();
        while(true){
          frame.getGraphics().drawImage(buffer, 0, 0, this);
          buffer.getGraphics().clearRect(0, 0, 700, 500);
          
          CosaqueGira();
          GenteCaminando(i);
          Estrellados(i);
          cabina1(gyro);
          cabina2();
          cabina3();
          cabina4();
          Luna(i);
          PuertoYMar();
          
          i++;
          gyro++;
          if(i > 750)
              i=1;
          if(gyro > 366)
              gyro = 1;
          System.out.println(i);
        }
      
    }
    
    void Luna(int x){
        //Luna
        if(x < 400){
          figura.CirculoBresenham(x+350, 70+(x/3), 20, buffer.getGraphics(), Color.WHITE, 1);
          if(x < 330)
            figura.Inundar(x+350, 70+(x/3), Color.gray, Color.white, buffer);
        }
        if(x > 400){
            figura.CirculoBresenham(x-400, 300-(x/3), 20, buffer.getGraphics(), Color.WHITE, 1);
            if(x > 425)
            figura.Inundar(x-400, 300-(x/3), Color.gray, Color.white, buffer);
        }
        
        //figura.Inundar(x+1, 70, Color.LIGHT_GRAY, Color.WHITE, buffer);
    }
    
    void cabina1(int x){
        figura.DibujarRectangulo(180, 255, 210, 285, buffer.getGraphics(), Color.RED);
        figura.Inundar(181, 256, Color.GRAY, Color.RED, buffer);
    }
    
    void cabina2(){
       //cabina 2
        figura.DibujarRectangulo(380, 255, 410, 285, buffer.getGraphics(), Color.RED);
        figura.Inundar(381, 256, Color.GRAY, Color.RED, buffer); 
    }
    
    void cabina3(){
        //cabina 3
        figura.DibujarRectangulo(290, 350, 320, 380, buffer.getGraphics(), Color.RED);
        figura.Inundar(291, 351, Color.GRAY, Color.RED, buffer);
    }
    
    void cabina4(){
        //cabina 4
        figura.DibujarRectangulo(290, 155, 320, 185, buffer.getGraphics(), Color.RED);
        figura.Inundar(291, 156, Color.GRAY, Color.RED, buffer);
    }
    
    void PuertoYMar(){
        //Puerto
        figura.DibujarRectangulo(0,400,700,414,buffer.getGraphics(),Color.YELLOW);
        //figura.Inundar(1, 302, Color.BLUE, Color.YELLOW, buffer);
        //Agua
        figura.DibujarRectangulo(1,415,698,499,buffer.getGraphics(),Color.CYAN);
        figura.Inundar(51, 451, Color.BLUE, Color.CYAN, buffer);
        
        figura.DibujarBresenham(10 , 385, 10, 500, 20, Color.YELLOW,buffer.getGraphics());
        figura.DibujarBresenham(100, 385, 100, 500, 20, Color.YELLOW,buffer.getGraphics());
        figura.DibujarBresenham(200, 385, 200, 500, 20, Color.YELLOW,buffer.getGraphics());
        figura.DibujarBresenham(300, 385, 300, 500, 20, Color.YELLOW,buffer.getGraphics());
        figura.DibujarBresenham(400, 385, 400, 500, 20, Color.YELLOW,buffer.getGraphics());
        figura.DibujarBresenham(500, 385, 500, 500, 20, Color.YELLOW,buffer.getGraphics());
        figura.DibujarBresenham(600, 385, 600, 500, 20, Color.YELLOW,buffer.getGraphics());
        figura.DibujarBresenham(700, 385, 700, 500, 20, Color.YELLOW,buffer.getGraphics());
    }
    
    void CosaqueGira(){
        //Ruleta o cosa que gira
        figura.CirculoBresenham(300, 250, 100, buffer.getGraphics(), Color.CYAN, 3);
        figura.DibujarBresenham(300, 250, 300, 350, buffer.getGraphics(), Color.CYAN);
        figura.DibujarBresenham(300, 250, 200, 250, buffer.getGraphics(), Color.CYAN);
        figura.DibujarBresenham(300, 250, 300, 150, buffer.getGraphics(), Color.CYAN);
        figura.DibujarBresenham(300, 250, 400, 250, buffer.getGraphics(), Color.CYAN);
        figura.DibujarBresenham(200, 250, 100, 400, buffer.getGraphics(), Color.CYAN);
        figura.DibujarBresenham(400, 250, 500, 400, buffer.getGraphics(), Color.CYAN);
    }
    
    void GenteCaminando(int x){
        x = x + 50;
        int largo = x + 10;
        int y = 700 - x;
        figura.DibujarRectangulo(x, 380, largo, 399, buffer.getGraphics(), Color.magenta);
        largo = (700 - x) - 10;
        figura.DibujarRectangulo(y, 380, largo, 399, buffer.getGraphics(), Color.magenta);
        x = x - 50;
        largo = x + 10;
        figura.DibujarRectangulo(x, 380, largo, 399, buffer.getGraphics(), Color.magenta);
        y = 700 - x;
        largo = y + 10;
        figura.DibujarRectangulo(y, 380, largo, 399, buffer.getGraphics(), Color.magenta);
        y = 700 - 400;
        largo = y - x;
        //figura.DibujarRectangulo(y, 380, largo, 399, buffer.getGraphics(), Color.magenta);
        x = x - 10;
        largo = x + 10;
        figura.DibujarRectangulo(x, 380, largo, 399, buffer.getGraphics(), Color.magenta);
        y = 300 - x;
        largo = y + 10;
        figura.DibujarRectangulo(y, 380, largo, 399, buffer.getGraphics(), Color.magenta);
        
        y = 200 + x;
        largo = y - 10;
        figura.DibujarRectangulo(y, 380, largo, 399, buffer.getGraphics(), Color.BLUE);
        
        //figura.DibujarRectangulo(x, 380, largo, 399, buffer.getGraphics(), Color.magenta);
        //figura.Inundar(551, 398, Color.darkGray, Color.magenta, buffer);
    }
    
    void Estrellados(int x){
      
        figura.DibujarCirculo(301, 251, 104,x);
    }
    public static void main(String[] args) {
        new Animachion();
    }
    
}
