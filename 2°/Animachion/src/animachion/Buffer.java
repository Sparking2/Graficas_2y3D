/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animachion;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Queue;

public class Buffer {
    Graphics gra;
    BufferedImage buffer;
    
    public Buffer(){
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
    }
    
    public void PutPixel(int x, int y, Color c){
           buffer.setRGB(0, 0, c.getRGB());
           gra.drawImage(buffer,x,y,null);
    }
    
    public void DibujarBresenham (int x0,int y0, int x1, int y1,Graphics g, Color col){
        gra = g;
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1)){
            int p = 2 * rodzil;
            if (p > -dy){
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            
            if (p < dx){
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
            }
        PutPixel(x0, y0, col);  
        }
    }
    
       public void Inundar(int Xc,int Yc, Color draw, Color limit, BufferedImage g){
           gra = g.getGraphics();
           
     Queue<Point>ListPuntos = new LinkedList<Point>();
       
       if(g.getRGB(Xc, Yc) != limit.getRGB())
        {
                ListPuntos.add(new Point(Xc, Yc));
                while(ListPuntos.isEmpty() == false)
                {
                Point p = ListPuntos.remove();
                if ((g.getRGB(p.x, p.y) != limit.getRGB()) && (g.getRGB(p.x, p.y) != draw.getRGB())) 
                    {
                    PutPixel(p.x, p.y, draw);
                    ListPuntos.add(new Point(p.x, p.y-1)); 
                    ListPuntos.add(new Point(p.x-1, p.y)); 
                    ListPuntos.add(new Point(p.x+1, p.y)); 
                    ListPuntos.add(new Point(p.x, p.y+1)); 
                    }
                }
        }
 
    }
    
    public void DibujarRectangulo(int x0,int y0, int x1, int y1,Graphics g, Color col){
         gra = g;
         DibujarBresenham(x0,y0,x1,y0,g,col);
         DibujarBresenham(x1,y0,x1,y1,g,col);
         DibujarBresenham(x1,y1,x0,y1,g,col);
         DibujarBresenham(x0,y1,x0,y0,g,col);     
    }
    
    public void CirculoBresenham(int Xc, int Yc, int Rd, Graphics g, Color cosa, int grosor){
        gra = g;
        PutPixel(Xc, Yc + Rd, cosa);
        double p = 3 - 2* Rd;
        for (int xk = 0,yk = Rd;xk <= yk; xk+=1){
            if(p < 0){
                ocholados(Xc,Yc,xk,yk,g,cosa,grosor);
                p += 2*xk + 3;                       
            }else{
                yk -= 1;
                ocholados(Xc,Yc,xk,yk,g,cosa,grosor);
                 p += 2 * ( xk -yk) + 5;
                }
        } 
    } 

    public void ocholados(int Xc, int Yc, int xk, int yk,Graphics g, Color cosa, int grosor){
          gra = g;
          for(int i = 0;i <grosor; i++){
             PutPixel((int)Xc + i + xk,(int)Yc + i + yk,cosa); //1 
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i + xk,(int)Yc + i + yk,cosa); //1
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i - xk,(int)Yc + i + yk,cosa); // 8
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i + xk,(int)Yc + i - yk,cosa); // 4
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i - xk,(int)Yc + i - yk,cosa); // 5
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i + yk,(int)Yc + i + xk,cosa); // 2
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i - yk,(int)Yc + i + xk,cosa); // 7
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i + yk,(int)Yc + i - xk,cosa); // 3
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i - yk,(int)Yc + i - xk,cosa); // 6
          }
    }

    public void DibujarElipses (int Xc, int Yc, int Rx, int Ry,Color c){
        int x = 0;
        int y = Ry;
        int p1 = (Ry * Ry) - (Ry *(Rx * Rx)) + ((Rx * Rx)/4);
        
        while ( ( 2 *( Ry * Ry ) ) < ( 2 * ( ( Rx * Rx ) * y ) ) ){
            PutPixel(Xc + x,Yc + y,c);
            PutPixel(Xc + x,Yc - y,c);
            PutPixel(Xc - x,Yc + y,c);
            PutPixel(Xc - x,Yc - y,c);
            x = x + 1;
            if (p1 < 0){
                p1 = p1 + ( 2 * x * (Ry * Ry) + (Ry * Ry) );
            } else {
                y = y - 1;
                p1 = p1 + ( 2 * x * (Ry * Ry) ) + (Ry * Ry) - ( 2 * (Rx * Rx) * y);
            }
        }
        int Ry2 = Ry * Ry;
        int Rx2 = Rx * Rx;
        int p2 = (int) (Ry2 * (Math.pow((x + 1/2), 2)) + (Rx2 * (Math.pow(y - 1, 2))) - (Rx2 * Ry2));
        while (y > 0){
            y = y - 1;
            if(p2 < 0){
                x = x + 1;
                p2 = p2 + (2 * x * Ry2) - (2 * Rx2 * y) + Rx2;
            }else{
                p2 = p2 - (2 * y * Rx2) + Rx2;
            }
            PutPixel(Xc + x, Yc + y, c);
            PutPixel(Xc + x, Yc - y, c);
            PutPixel(Xc - x, Yc + y, c);
            PutPixel(Xc - x, Yc - y, c);
        }
    }
    
    public void DibujarBresenham (int x0,int y0, int x1, int y1,int grosor,Color c,Graphics g){
        gra = g;
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1))
        {
            int p = 2 * rodzil;
            if (p > -dy)
            {
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            if (p < dx) {
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
        }
        PutPixel(x0, y0, c);
        for(int i = 0;i < grosor; i++)
                {
                PutPixel(x0 + i, y0 + i, c);
                }
            
        }
        
    }
    
          public void DibujarEstrella (int x0,int y0, int x1, int y1)
    {
        
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        Boolean[] mascara;
        mascara = new Boolean[] {true,false,false,false,false,false,true};
               
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        int i = 0;
        while ((x0 != x1) || (y0 != y1))
        {
            
            int p = 2 * rodzil;
            if (p > -dy)
            {
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            if (p < dx) {
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
            }
            if(mascara[i] == true)
            {
                PutPixel(x0, y0, Color.WHITE);
            }
            i++;
            if(i == 7)
            {
                i = 0;
            }
            
        
        
        }
    }
          
    public void DibujarCirculo(int Xc, int Yc, int Rd,int x)
    {
      Boolean[] mascara;
      mascara = new Boolean[] {};
      if(x % 2 == 0){
          mascara = new Boolean[] {true,false,false,true,false,false,true};
          int lenght = mascara.length;
      int i = 0;
        
        for(float t =0;t <= 160;t++)
        {
            if(mascara[i] == true)
            {
              ocholados(Xc,Yc,(int)(Rd * Math.cos(t)),(int)(Rd * Math.sin(t)));
              
            }
            i++;
            if(i == lenght)
            {
                i = 0;
            }
                      
        } 
      }
      if(x % 3 == 0){
          mascara = new Boolean[] {false,true,true,false,true,true,false};
          int lenght = mascara.length;
      int i = 0;
        
        for(float t =0;t <= 160;t++)
        {
            if(mascara[i] == true)
            {
              ocholados(Xc,Yc,(int)(Rd * Math.cos(t)),(int)(Rd * Math.sin(t)));
              
            }
            i++;
            if(i == lenght)
            {
                i = 0;
            }
                      
        } 
      }
    }
    
     public void ocholados(int Xc, int Yc, int xk, int yk)
      {
          PutPixel((int)Xc + xk,(int)Yc + yk,Color.ORANGE); //1
          
          PutPixel((int)Xc - xk,(int)Yc + yk,Color.ORANGE); // 8
          
          PutPixel((int)Xc + xk,(int)Yc - yk,Color.ORANGE); // 4
          
          PutPixel((int)Xc - xk,(int)Yc - yk,Color.ORANGE); // 5
          
          PutPixel((int)Xc + yk,(int)Yc + xk,Color.ORANGE); // 2
          
          PutPixel((int)Xc - yk,(int)Yc + xk,Color.ORANGE); // 7
          
          PutPixel((int)Xc + yk,(int)Yc - xk,Color.ORANGE); // 3
          
          PutPixel((int)Xc - yk,(int)Yc - xk,Color.ORANGE); // 6
          
      }
    public int GetPixel(){
        gra.getColor();
        int color = 0;
        
        return color;
    }
}
