package pixel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public final class Pixel extends JFrame {

    private final BufferedImage buffer;
    private final Graphics graPixel;
    

    public Pixel() {
        
        setPreferredSize(new Dimension(480, 360));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        
        pack();
        setVisible(true);
    }

    public void putPixel(int x, int y, Color c) {

        buffer.setRGB(0, 0, c.getRGB());
        this.getGraphics().drawImage(buffer, x, y, this);

    }
    
    public void dibujarLinea(int x0,int y0, int x1, int y1)
    {
        float m = 0;
        int x = 0;
        m = (float)(y1 - y0)/(x1-x0);
        float b = y0 - m*x0;
        
        for(x = x0;x < x1; x++)
        {
           float y = m*x + b;
           putPixel(x,Math.round(y),Color.BLACK); 
        }       
    }
    
    public static void main(String[] args) 
    {
        Pixel ventana = new Pixel();
         ventana.dibujarLinea(0,200,100,100);
    }

}
