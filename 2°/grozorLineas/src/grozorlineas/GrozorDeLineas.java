package grozorlineas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class GrozorDeLineas {
    Graphics gra;
    BufferedImage buffer;
    
    public GrozorDeLineas(){
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
    }
    
    public void DibujarLinea(int x0, int y0,int x1, int y1,int grozor, Graphics g){
        gra=g;
        int x, y, dx, dy, p, incDer, incArrDer, stepx=1, stepy=1;
        dx = (x1 - x0);
        dy = (y1 - y0);

        if (dy < 0) { //determinamos puntos de inicio y fin e.e
            dy = -dy; 
            stepy = -1; 
        } 
        if (dx < 0) {  
             dx = -dx;  
            stepx = -1; 
        }
        x = x0;
        y = y0;
        putPixel(x,y,Color.RED);

        if(dx>dy){//dibujamos en torno a Y
            p = 2*dy - dx;
              incDer = 2*dy;
            incArrDer = 2*(dy-dx);
            while (x != x1){
                x += stepx;
                if (p < 0){
                     p = p + incDer;
                }
                else {
                      y = y + stepy;
                     p += incArrDer;
                }
                putPixel(x,y,Color.RED);
                for(int xG=0,contEl=1,estGrozor=1;xG<grozor;xG++){
                    if(estGrozor==1){
                        putPixel(x,y+contEl,Color.RED);
                        estGrozor=-1;
                    }                    
                    else{
                        putPixel(x,y-contEl,Color.RED);
                        contEl++;
                        estGrozor=1;
                    }
                }
            }
        }
        else{//dibujamos en torno a X
             p = 2*dx - dy;
             incDer = 2*dx;
            incArrDer = 2*(dx-dy);
            while (y != y1){
                y = y + stepy;
                if (p < 0){
                   p += incDer;
                 }
            else {
                x = x + stepx;
                  p +=  incArrDer;
            }
            putPixel(x,y,Color.RED);
            for(int xG=0,contEl=1,estGrozor=1;xG<grozor;xG++){
                if(estGrozor==1){
                    putPixel(x+contEl,y,Color.RED);
                    estGrozor=-1;
                }                    
                else{
                    putPixel(x-contEl,y,Color.RED);
                    contEl++;
                    estGrozor=1;
                }
            }
        }
    }
        
    }
    
    public void dibujarCirculoBrhm(int xc, int yc, float radio,int grozor, Graphics g){
        gra=g;
        int pk=0;
        int xk=(int)radio,yk=0;
        if(grozor%2==0){
            grozor++;
        }
        while(yk<=xk){
            putPixel(xk+xc,yk+yc,Color.RED);
            putPixel(xk+xc,(yc-yk),Color.RED);
            putPixel((xc-xk),yk+yc,Color.RED);              
            putPixel((xc-xk),(yc-yk),Color.RED);            
            putPixel(xc+yk,yc+xk,Color.RED);
            putPixel(xc+yk,(yc-xk),Color.RED);
            putPixel((xc-yk),yc+xk,Color.RED);              
            putPixel((xc-yk),(yc-xk),Color.RED);
            for(int xG=0,contEl=1,estGrozor=1;xG<grozor;xG++){
                if(estGrozor==1){              
                    putPixel(xc+yk,yc+xk+contEl,Color.RED);//
                    putPixel(xc+yk,(yc-xk+contEl),Color.RED);
                    putPixel((xc-yk),yc+xk+contEl,Color.RED);              
                    putPixel((xc-yk),(yc-xk+contEl),Color.RED);
                    putPixel(xk+xc+contEl,yk+yc,Color.RED);//lateral
                    putPixel(xk+xc+contEl,(yc-yk),Color.RED);//lateral
                    putPixel((xc-xk)+contEl,yk+yc,Color.RED);//lateral           
                    putPixel((xc-xk)+contEl,(yc-yk),Color.RED);//lateral
                    estGrozor=-1;
                }                    
                else{
                    putPixel(xc+yk,yc+xk-contEl,Color.RED);//
                    putPixel(xc+yk,(yc-xk-contEl),Color.RED);
                    putPixel((xc-yk),yc+xk-contEl,Color.RED);              
                    putPixel((xc-yk),(yc-xk-contEl),Color.RED);  
                    putPixel(xk+xc-contEl,yk+yc,Color.RED);//lateral
                    putPixel(xk+xc-contEl,(yc-yk),Color.RED);//lateral
                    putPixel((xc-xk-contEl),yk+yc,Color.RED);//lateral           
                    putPixel((xc-xk-contEl),(yc-yk),Color.RED);//lateral
                    contEl++;
                    estGrozor=1;
                }
            }
            pk+=2*yk+1;
            yk++;
            if(2*pk>(2*xk-1)){
                xk--;
                pk=pk-2*xk+1;
            }
        }
        
    }
    
    private void putPixel(int x, int y, Color c){
        buffer.setRGB(0,0,c.getRGB());
        gra.drawImage(buffer, x, y, null);
    }
}
