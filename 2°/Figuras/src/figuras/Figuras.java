/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public class Figuras extends JFrame{
    
    private final BufferedImage buffer;
    private final Graphics graPixel;

    
    public Figuras()
    {
        setPreferredSize(new Dimension(800,600));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        pack();
        setVisible(true);
    }
    
     public void putPixel(int x, int y, Color c) 
    {
        buffer.setRGB(0, 0, c.getRGB());
        this.getGraphics().drawImage(buffer, x, y, this);
    }
     
     public void DibujarRectangulo(int x0,int y0, int x1, int y1)
     {
         DibujarBresenham(x0,y0,x1,y0);
         DibujarBresenham(x1,y0,x1,y1);
         DibujarBresenham(x1,y1,x0,y1);
         DibujarBresenham(x0,y1,x0,y0);
                 
     }
     
     public void DibujarBresenham (int x0,int y0, int x1, int y1)
    {
        
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1))
        {
            int p = 2 * rodzil;
            if (p > -dy)
            {
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            if (p < dx) {
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
        }
        putPixel(x0, y0, Color.BLACK);  
        
        }
        
    }
    
        public void CirculoBresenham(int Xc, int Yc, int Rd)
    {
        putPixel(Xc, Yc + Rd, Color.BLUE);
        double p = 3 - 2* Rd;
        
             for (int xk = 0,yk = Rd;xk <= yk; xk+=1)  {
                if(p < 0){
                    ocholados(Xc,Yc,xk,yk);
                    p += 2*xk + 3;                       
                }else{
                    yk -= 1;
                    ocholados(Xc,Yc,xk,yk);
                    p += 2 * ( xk -yk) + 5;
                }

        } 
        
    } 
     
    public void ocholados(int Xc, int Yc, int xk, int yk)
      {
          putPixel((int)Xc + xk,(int)Yc + yk,Color.BLUE); //1
          
          putPixel((int)Xc - xk,(int)Yc + yk,Color.BLUE); // 8
          
          putPixel((int)Xc + xk,(int)Yc - yk,Color.BLUE); // 4
          
          putPixel((int)Xc - xk,(int)Yc - yk,Color.BLUE); // 5
          
          putPixel((int)Xc + yk,(int)Yc + xk,Color.BLUE); // 2
          
          putPixel((int)Xc - yk,(int)Yc + xk,Color.BLUE); // 7
          
          putPixel((int)Xc + yk,(int)Yc - xk,Color.BLUE); // 3
          
          putPixel((int)Xc - yk,(int)Yc - xk,Color.BLUE); // 6
          
      }
    
    public void DibujarElipses (int Xc, int Yc, int Rx, int Ry)
    {
        int x = 0;
        int y = Ry;
        int p1 = (Ry * Ry) - (Ry *(Rx * Rx)) + ((Rx * Rx)/4);
        
        while ( ( 2 *( Ry * Ry ) ) < ( 2 * ( ( Rx * Rx ) * y ) ) )
        {
            putPixel(Xc + x,Yc + y,Color.GREEN);
            putPixel(Xc + x,Yc - y,Color.GREEN);
            putPixel(Xc - x,Yc + y,Color.GREEN);
            putPixel(Xc - x,Yc - y,Color.GREEN);
            x = x + 1;
            if (p1 < 0)
            {
                p1 = p1 + ( 2 * x * (Ry * Ry) + (Ry * Ry) );
            } else {
                y = y - 1;
                p1 = p1 + ( 2 * x * (Ry * Ry) ) + (Ry * Ry) - ( 2 * (Rx * Rx) * y);
            }
        }
        int Ry2 = Ry * Ry;
        int Rx2 = Rx * Rx;
        int p2 = (int) (Ry2 * (Math.pow((x + 1/2), 2)) + (Rx2 * (Math.pow(y - 1, 2))) - (Rx2 * Ry2));
        while (y > 0)
        {
            y = y - 1;
            if(p2 < 0)
            {
                x = x + 1;
                p2 = p2 + (2 * x * Ry2) - (2 * Rx2 * y) + Rx2;
            }else{
                p2 = p2 - (2 * y * Rx2) + Rx2;
            }
            putPixel(Xc + x, Yc + y, Color.GREEN);
            putPixel(Xc + x, Yc - y, Color.GREEN);
            putPixel(Xc - x, Yc + y, Color.GREEN);
            putPixel(Xc - x, Yc - y, Color.GREEN);
        }
    }
    public static void main(String[] args) {
        Figuras cosa = new Figuras();
        //lineas
        cosa.DibujarBresenham(0, 0, 160, 160);
        cosa.DibujarBresenham(200, 100, 350, 100);
        cosa.DibujarBresenham(400, 160, 500, 50);
        cosa.DibujarBresenham(780, 100, 600, 100);
        
        //circulo
        cosa.CirculoBresenham(130, 400, 120);
        cosa.CirculoBresenham(130, 400, 80);
        cosa.CirculoBresenham(130, 400, 40);
        cosa.CirculoBresenham(130, 400, 10);
        
        //Rectangulo
        cosa.DibujarRectangulo(300, 350, 500, 480);
        cosa.DibujarRectangulo(480, 460, 320, 370);
        
        //Elipse
        cosa.DibujarElipses(650,425,125,50);
        cosa.DibujarElipses(650,425,85,20);
               
    }
    
}
