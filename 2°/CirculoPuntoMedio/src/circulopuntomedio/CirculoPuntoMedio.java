/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package circulopuntomedio;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

/**
 *
 * @author spark
 */
public class CirculoPuntoMedio extends JFrame {
    
    private final BufferedImage buffer;
    private final Graphics graPixel;

    public CirculoPuntoMedio()
    {
        super("Bresenham");
        setPreferredSize(new Dimension(480, 360));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        pack();
        setVisible(true);
    }
    
    public void PuntoMedio(int Xc, int Yc, int Rd)
    {
        putPixel(Xc,Yc + Rd,Color.BLUE);
        int p = 1 - Rd;
        
        for (int xk = 0, yk = Rd;xk <= yk + 14;xk++)
        {
            if(p<0)
            {
            ocholados(Xc,Yc,xk,yk);   
            p += 2*xk +1;
            }else{
            yk -= 1;
            ocholados(Xc,Yc,xk,yk);
            p += 2*(xk-yk)+5;
            }
        }
    }
    
    public void putPixel(int x, int y, Color c) 
        {
            buffer.setRGB(0, 0, c.getRGB());
            this.getGraphics().drawImage(buffer, x, y, this);
        }
   
    public void ocholados(int Xc, int Yc, int xk, int yk)
      {
          putPixel((int)Xc + xk,(int)Yc + yk,Color.BLACK); //1
          
          putPixel((int)Xc - xk,(int)Yc + yk,Color.BLACK); // 8
          
          putPixel((int)Xc + xk,(int)Yc - yk,Color.BLACK); // 4
          
          putPixel((int)Xc - xk,(int)Yc - yk,Color.BLACK); // 5
          
          putPixel((int)Xc + yk,(int)Yc + xk,Color.BLACK); // 2
          
          putPixel((int)Xc - yk,(int)Yc + xk,Color.BLACK); // 7
          
          putPixel((int)Xc + yk,(int)Yc - xk,Color.BLACK); // 3
          
          putPixel((int)Xc - yk,(int)Yc - xk,Color.BLACK); // 6
          
      }
    
    public static void main(String[] args) {
        new CirculoPuntoMedio().PuntoMedio(200,200,50);
    }
    
}
