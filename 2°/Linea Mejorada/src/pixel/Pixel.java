package pixel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public final class Pixel extends JFrame {

    private final BufferedImage buffer;
    private final Graphics graPixel;
    

    public Pixel() {
        
        setPreferredSize(new Dimension(480, 360));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        
        pack();
        setVisible(true);
    }

    public void putPixel(int x, int y, Color c) {

        buffer.setRGB(0, 0, c.getRGB());
        this.getGraphics().drawImage(buffer, x, y, this);

    }
    
    public void dibujarLinea(int x0,int y0, int x1, int y1)
    {
        float m = 0;
        
        if((x1 - x0) != 0)
        {
            m = (float)(y1 - y0)/(x1-x0);
            float b = y0 - m*x0;
            if(m >= 0)
            {
                
                if(x0 < x1)
                {
                    for ( int x = x0; x < x1; x++)
                    {
                        float y = m*x + b;
                        putPixel(x,Math.round(y),Color.BLACK);
                    }
                }
                else
                {
                    for ( int x = x0; x > x1; x--)
                        {
                        float y = m*x + b;
                        putPixel(x,Math.round(y),Color.BLACK);
                        } 
                }
                
            }
            else
            {
                if(y0 > y1)
                {
                  
                  for ( int x = x0; x <= x1; x++)
                    {
                        float y = m*x + b;
                        putPixel(x,Math.round(y),Color.BLACK);
                    }   
                } 
                else
                {
                    for ( int x = x1; x <= x0; x++)
                    {
                        float y = m*x + b;
                        putPixel(x,Math.round(y),Color.BLACK);
                    }   
                } 
                
            }
        }
        else
        {
            if(y0 <= y1)
            {
                
                float b = x0;
                for(int y = y0; y <= y1; y++)
                    {
                    putPixel((int) b, y, Color.BLACK);
                    }  
            }
            else 
            {
                
                float b = x0;
                for(int y = y0; y >= y1; y--)
                    {
                    putPixel((int) b, y, Color.BLACK);
                    } 
                   
            }
            
        }
            
    }
    
    public static void main(String[] args) 
    {
        Pixel ventana = new Pixel();
        ventana.dibujarLinea(0, 0, 100, 100);
        ventana.dibujarLinea(100, 100, 200, 100);
        ventana.dibujarLinea(200, 100, 200, 200);
        ventana.dibujarLinea(200, 200, 100, 300);
        ventana.dibujarLinea(100, 300, 100, 200);
        ventana.dibujarLinea(100,200,0,200);
        ventana.dibujarLinea(0,200,100,100);
    }

}
