/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebainundacion;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Prueba {
    Graphics gra;
    BufferedImage buffer;
    
    public Prueba(){
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
    }
    
      public void putPixel(int x, int y, Color c) 
        {
            buffer.setRGB(0, 0, c.getRGB());
            gra.drawImage(buffer,x,y,null);
        }
      
      public void DibujarBresenham (int x0,int y0, int x1, int y1,Graphics g)
    {
        gra = g;
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1))
        {
            int p = 2 * rodzil;
            if (p > -dy)
            {
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            if (p < dx) {
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
        }
        putPixel(x0, y0, Color.WHITE);  
        
        }
    }
      
      public void Inundar(int Xc,int Yc, Color Borde, Graphics g, int Relleno)
   {
       //int Relleno =  g.getColor().getRGB();//buffer.getRGB(Xc, Yc);
       
       try
       {
           if(getPixelColor(Xc,Yc) == Relleno)
            {
                Color c = Borde;
                putPixel(Xc,Yc,c);
            
                if(getPixelColor(Xc,Yc-1) ==  Relleno)
                Inundar(Xc,Yc-1,c,g,Relleno);
            
                if(getPixelColor(Xc-1,Yc) ==  Relleno)
                Inundar(Xc-1,Yc,c,g,Relleno);
            
                if(getPixelColor(Xc,Yc+1) ==  Relleno)
                Inundar(Xc,Yc+1,c,g,Relleno);
            
                if(getPixelColor(Xc+1,Yc-1) ==  Relleno)
                Inundar(Xc+1,Yc-1,c,g,Relleno);
            }
       }
        catch(StackOverflowError e){
            Inundar(Xc,Yc,Borde,g,Relleno);
            return;
        }
       
       
   }
      
       
   public int getPixelColor(int x, int y)
    {
       
   
       //.getGraphics().drawImage(temp, x, y, this);
       
       
       int red = (clr >> 16) & 0x000000FF;
       int green = (clr >>8 ) & 0x000000FF;
       int blue = (clr) & 0x000000FF;
       //System.out.println(temp.getRGB(x,y));
       return clr;
       
   }
}
