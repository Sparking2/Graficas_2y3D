/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebainundacion;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public class PruebaInundacion extends JFrame{

    JFrame frame;
    Prueba figura;
    BufferedImage buffer;
    
    public PruebaInundacion()
    {
        frame = new JFrame("Bresenham Circulo");
        buffer = new BufferedImage(700, 500, BufferedImage.TYPE_INT_RGB);
        frame.setSize(700,500);       
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        figura=new Prueba();
        
        //buffer.getGraphics().clearRect(0, 0, 700, 500);
        for (int i = 0; i< 10 ; i++)
        {
        figura.DibujarBresenham(50, 50, 300, 50,buffer.getGraphics());
        figura.DibujarBresenham(70, 50, 70, 300,buffer.getGraphics());
        figura.DibujarBresenham(70, 300, 200, 100,buffer.getGraphics());
        figura.DibujarBresenham(200, 100, 300, 50,buffer.getGraphics());
        figura.Inundar(50, 50, Color.PINK, buffer.getGraphics());
        int clr = buffer.getRGB(50, 50);
        frame.getGraphics().drawImage(buffer, 0, 0, this, clr);
        }
    }
    public static void main(String[] args) {
        new PruebaInundacion();
    }
    
}
