/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grosorcirculo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public class GrosorCirculo extends JFrame{

    private final BufferedImage buffer;
    private final Graphics graPixel;

    public GrosorCirculo()
    {
        super("Circulo Gordis");
        setPreferredSize(new Dimension(480, 360));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        pack();
        setVisible(true);
    }
    
    public void putPixel(int x, int y, Color c) 
        {
            buffer.setRGB(0, 0, c.getRGB());
            this.getGraphics().drawImage(buffer, x, y, this);
        }
    
    public void DibujarCirculo(int Xc, int Yc, int Rd, int grosor)
    {
        
        for(float t = 0;t <= 160;t++)
            {
                ocholados(Xc,Yc,(int)(Rd * Math.cos(t)),(int)(Rd * Math.sin(t)),grosor);
            }
    }
    
    public void ocholados(int Xc, int Yc, int xk, int yk,int grosor)
      {
          for(int i = 0;i <grosor; i++)
          {
              putPixel((int)Xc + i + xk,(int)Yc+ i + yk,Color.BLACK); //1 
          }
         
          for(int i = 0;i <grosor; i++)
          {
              putPixel((int)Xc + i - xk,(int)Yc + i + yk,Color.BLACK); // 8 
          }
          
          for(int i = 0;i <grosor; i++)
          {
               putPixel((int)Xc +i + xk,(int)Yc + i - yk,Color.BLACK); // 4
          }
          
         for(int i = 0;i <grosor; i++)
          {
               putPixel((int)Xc + i - xk,(int)Yc + i - yk,Color.BLACK); // 5
          }
          
          for(int i = 0;i <grosor; i++)
          {
               putPixel((int)Xc + i + yk,(int)Yc + i + xk,Color.BLACK); // 2
          }
          
           for(int i = 0;i <grosor; i++)
          {
               putPixel((int)Xc + i - yk,(int)Yc + i + xk,Color.BLACK); // 7
          }
          
          for(int i = 0;i <grosor; i++)
          {
               putPixel((int)Xc + i + yk,(int)Yc + i - xk,Color.BLACK); // 3
          }
          
          for(int i = 0;i <grosor; i++)
          {
               putPixel((int)Xc + i - yk,(int)Yc + i - xk,Color.BLACK); // 6
          }
          
          
          
          
      }
    
    public static void main(String[] args) {
        GrosorCirculo ventana = new GrosorCirculo();
        ventana.DibujarCirculo(70, 100, 50, 1);
        ventana.DibujarCirculo(140, 100, 50, 2);
        ventana.DibujarCirculo(210, 100, 50, 3);
        ventana.DibujarCirculo(280, 100, 50, 4);
        ventana.DibujarCirculo(350, 100, 50, 5);
        ventana.DibujarCirculo(420, 100, 50, 6);
    }
    
}
