/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scanline;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public class ScanLine extends JFrame{
    private final BufferedImage buffer;
    private final Graphics graPixel;

    public ScanLine()
    {
        super("Scan-Line");
        setPreferredSize(new Dimension(480, 360));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        pack();
        setVisible(true);
    }
    
    public void putPixel(int x, int y, Color c) 
        {
            buffer.setRGB(0, 0, c.getRGB());
            this.getGraphics().drawImage(buffer, x, y, this);
        }
    
    public void DibujarBresenham (int x0,int y0, int x1, int y1)
    {
        
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1))
        {
            int p = 2 * rodzil;
            if (p > -dy)
            {
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            if (p < dx) {
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
        }
        putPixel(x0, y0, Color.BLACK);  
        
        }
    }
    
    public void InundadoScanLine()
    {
        
    }
    public static void main(String[] args) {
        ScanLine ventana = new ScanLine();
        ventana.DibujarBresenham(50, 50, 300, 50);
        ventana.DibujarBresenham(70, 50, 70, 300);
        ventana.DibujarBresenham(70, 300, 200, 100);
        ventana.DibujarBresenham(200, 100, 300, 50);
    }
    
}
