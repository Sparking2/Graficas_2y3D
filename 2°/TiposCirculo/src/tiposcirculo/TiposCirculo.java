/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiposcirculo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public class TiposCirculo extends JFrame{

    private final BufferedImage buffer;
    private final Graphics graPixel;
    
     public TiposCirculo()
    {
        super("Circulo");
        setPreferredSize(new Dimension(480, 360));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        pack();
        setVisible(true);
    }
     
     public void putPixel(int x, int y, Color c) 
        {
            buffer.setRGB(0, 0, c.getRGB());
            this.getGraphics().drawImage(buffer, x, y, this);
        }
     
     
    public void DibujarCirculo(int Xc, int Yc, int Rd)
    {
      Boolean[] mascara;
      mascara = new Boolean[] {true,true,false,false,false,false,false};
      //mascara = new Boolean[] {true,true,true,true,true,true,true};
      int lenght = mascara.length;
      int i = 0;
        
        for(float t =0;t <= 160;t++)
        {
            if(mascara[i] == true)
            {
              ocholados(Xc,Yc,(int)(Rd * Math.cos(t)),(int)(Rd * Math.sin(t)));
              
            }
            i++;
            if(i == lenght)
            {
                i = 0;
            }
                      
        }
        
         
    }
      public void ocholados(int Xc, int Yc, int xk, int yk)
      {
          putPixel((int)Xc + xk,(int)Yc + yk,Color.BLACK); //1
          
          putPixel((int)Xc - xk,(int)Yc + yk,Color.BLACK); // 8
          
          putPixel((int)Xc + xk,(int)Yc - yk,Color.BLACK); // 4
          
          putPixel((int)Xc - xk,(int)Yc - yk,Color.BLACK); // 5
          
          putPixel((int)Xc + yk,(int)Yc + xk,Color.BLACK); // 2
          
          putPixel((int)Xc - yk,(int)Yc + xk,Color.BLACK); // 7
          
          putPixel((int)Xc + yk,(int)Yc - xk,Color.BLACK); // 3
          
          putPixel((int)Xc - yk,(int)Yc - xk,Color.BLACK); // 6
          
      }
      
    public static void main(String[] args) {
        TiposCirculo ventana = new TiposCirculo();
        ventana.DibujarCirculo(240, 150, 100);
    }
    
}
