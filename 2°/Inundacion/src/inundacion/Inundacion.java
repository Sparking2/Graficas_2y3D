/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inundacion;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import static javafx.scene.paint.Color.color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import static jdk.nashorn.internal.objects.NativeArray.push;
public class Inundacion extends JFrame{

    private BufferedImage buffer,temp;
    private Graphics graPixel;
    //private JPanel panel = new JPanel();
    
   public Inundacion()
    {
        setTitle("Inundacion");
        setSize(new Dimension(480, 360));
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        temp = (BufferedImage) this.createImage(this.getWidth(),this.getHeight());
        
        
        Inundar(120,90,Color.BLACK,Color.RED);
        
        
    }
   
   @Override
   public void paint(Graphics g)
   {
        DibujarBresenham(50, 50, 300, 50);
        DibujarBresenham(70, 50, 70, 300);
        DibujarBresenham(70, 300, 200, 100);
        DibujarBresenham(200, 100, 300, 50);
        System.out.println("Termino");
        
   }
   
   public void putPixel(int x, int y, Color c) 
        {
            buffer.setRGB(0, 0, c.getRGB());
            this.getGraphics().drawImage(buffer, x, y, this);
            temp.getGraphics().drawImage(buffer, x, y, this);
        }
   
   public void DibujarBresenham (int x0,int y0, int x1, int y1)
    {
        
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1))
        {
            int p = 2 * rodzil;
            if (p > -dy)
            {
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            if (p < dx) {
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
        }
        putPixel(x0, y0, Color.BLACK);  
        
        }
    }
   
   public void Inundar(int Xc,int Yc, Color ref, Color draw)
   {
       Queue<Point>ListPuntos = new LinkedList<Point>();
       
       if(temp.getRGB(Xc, Yc) != ref.getRGB())
        {
                ListPuntos.add(new Point(Xc, Yc));
                while(ListPuntos.isEmpty() == false)
                {
                Point p = ListPuntos.remove();
                if ((temp.getRGB(p.x, p.y) != ref.getRGB()) && (temp.getRGB(p.x, p.y) != draw.getRGB())) 
                    {
                    putPixel(p.x, p.y, draw);
                    ListPuntos.add(new Point(p.x, p.y-1)); 
                    ListPuntos.add(new Point(p.x-1, p.y)); 
                    ListPuntos.add(new Point(p.x+1, p.y)); 
                    ListPuntos.add(new Point(p.x, p.y+1)); 
                    }
                }
        }
       System.out.println("Finish");
    }
   
    public static void main(String[] args) {
        Inundacion ventana = new Inundacion();
    }
    
}
