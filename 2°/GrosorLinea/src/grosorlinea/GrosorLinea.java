/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grosorlinea;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public class GrosorLinea extends JFrame{

   private final BufferedImage buffer;
   private final Graphics graPixel;
    
    public GrosorLinea()
    {
        super("Grosor Linea");
        setPreferredSize(new Dimension(480, 360));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        pack();
        setVisible(true);
    }
    
    public void putPixel(int x, int y, Color c) 
        {
            buffer.setRGB(0, 0, c.getRGB());
            this.getGraphics().drawImage(buffer, x, y, this);
        }
    
    public void DibujarBresenham (int x0,int y0, int x1, int y1,int grosor)
    {
        
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1))
        {
            int p = 2 * rodzil;
            if (p > -dy)
            {
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            if (p < dx) {
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
        }
        putPixel(x0, y0, Color.BLACK);
        for(int i = 0;i < grosor; i++)
                {
                putPixel(x0 + i, y0 + i, Color.BLACK);
                }
            
        }
        
    }
    
    public static void main(String[] args) {
        GrosorLinea ventana = new GrosorLinea();
        ventana.DibujarBresenham(0, 0, 480, 130,0);
        ventana.DibujarBresenham(0, 0, 480, 180,2);
        ventana.DibujarBresenham(0, 0, 480, 240,4);
        ventana.DibujarBresenham(0, 0, 480, 290,6);
        ventana.DibujarBresenham(0, 0, 480, 340,8);
        ventana.DibujarBresenham(0, 0, 480, 390,10);
        ventana.DibujarBresenham(50, 0, 50, 500,6);
    }
    
}
