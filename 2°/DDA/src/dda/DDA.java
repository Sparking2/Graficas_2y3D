/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dda;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import static java.lang.Math.abs;
import javax.swing.JFrame;

/**
 *
 * @author spark
 */
public class DDA extends JFrame{
    
    private final BufferedImage buffer;
    private final Graphics graPixel;
    
    public DDA()
    {
        super("DDA");
        setPreferredSize(new Dimension(480, 360));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        
        pack();
        setVisible(true);
    }

     public void putPixel(int x, int y, Color c) {

        buffer.setRGB(0, 0, c.getRGB());
        this.getGraphics().drawImage(buffer, x, y, this);

    }
     
     public void DibujarDDA(int x0,int y0, int x1, int y1)
    {
        int steps = 0;
        float dx = x1 - x0;
        float dy = y1 - y0;
        
        if(Math.abs(dy) <= Math.abs(dx))
        {
            steps = Math.abs(Math.round(dx));
        }
        else
        {
            steps = Math.abs(Math.round(dy));
        }
        
        float xinc = dx/steps;
        float yinc = dy/steps;
        
        float x = x0;
        float y = y0;
        
        putPixel(Math.round(x),Math.round(y),Color.BLACK);
        for (int i = 0; i < steps; i++)
        {
            
            x = x + xinc;
            y = y + yinc;
            putPixel(Math.round(x),Math.round(y),Color.BLACK);
        }
    System.out.println("termino");
    }
    
   
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DDA ventana = new DDA();
        ventana.DibujarDDA(0, 0,400, 130);
        
    }
    
}
