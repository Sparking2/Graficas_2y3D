package circulo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public class Circulo extends JFrame{

    private final BufferedImage buffer;
    private final Graphics graPixel;

    public Circulo()
    {
        setPreferredSize(new Dimension(480, 360));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        pack();
        setVisible(true);
    }
    
    public void putPixel(int x, int y, Color c) 
    {
        buffer.setRGB(0, 0, c.getRGB());
        this.getGraphics().drawImage(buffer, x, y, this);
    }
    
    public void CirculoBresenham(int Xc, int Yc, int Rd)
    {
        putPixel(Xc, Yc + Rd, Color.BLUE);
        double p = 3 - 2* Rd;
        
             for (int xk = 0,yk = Rd;xk <= yk; xk+=1)  {
                if(p < 0){
                    ocholados(Xc,Yc,xk,yk);
                    p += 2*xk + 3;                       
                }else{
                    yk -= 1;
                    ocholados(Xc,Yc,xk,yk);
                    p += 2 * ( xk -yk) + 5;
                }

        } 
        
    }
       public void ocholados(int Xc, int Yc, int xk, int yk)
      {
          putPixel((int)Xc + xk,(int)Yc + yk,Color.BLUE); //1
          
          putPixel((int)Xc - xk,(int)Yc + yk,Color.BLUE); // 8
          
          putPixel((int)Xc + xk,(int)Yc - yk,Color.BLUE); // 4
          
          putPixel((int)Xc - xk,(int)Yc - yk,Color.BLUE); // 5
          
          putPixel((int)Xc + yk,(int)Yc + xk,Color.BLUE); // 2
          
          putPixel((int)Xc - yk,(int)Yc + xk,Color.BLUE); // 7
          
          putPixel((int)Xc + yk,(int)Yc - xk,Color.BLUE); // 3
          
          putPixel((int)Xc - yk,(int)Yc - xk,Color.BLUE); // 6
          
      }
    
    public static void main(String[] args) {
      Circulo ventana = new Circulo();
      ventana.CirculoBresenham(240, 200, 100);
    }
    
}
