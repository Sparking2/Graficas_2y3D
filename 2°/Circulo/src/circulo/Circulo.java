package circulo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public class Circulo extends JFrame{

    private final BufferedImage buffer;
    private final Graphics graPixel;

    public Circulo()
    {
        setPreferredSize(new Dimension(480, 360));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        graPixel = (Graphics2D) buffer.createGraphics();
        
        pack();
        setVisible(true);
    }
    
    public void putPixel(int x, int y, Color c) 
    {
        buffer.setRGB(0, 0, c.getRGB());
        this.getGraphics().drawImage(buffer, x, y, this);
    }
    
    public void DibujarCirculo(int Xc, int Yc, int Rd)
    {
        int y = 0;
        
        for(int x = (Xc - Rd); x < (Xc + Rd);x++)
        {
            y = (int) (Yc + Math.sqrt(Math.pow(Rd,2) - Math.pow((x - Xc),2)));
            putPixel(x,Math.round(y),Color.BLACK);
            y = (int) (Yc - Math.sqrt(Math.pow(Rd,2) - Math.pow((x - Xc),2)));
            putPixel(x,Math.round(y),Color.BLACK);
        }
        System.out.println("termino");
        
        
        
        System.out.println("termino");
        
    }
    public static void main(String[] args) {
      Circulo ventana = new Circulo();
      ventana.DibujarCirculo(240, 130, 100);
    }
    
}
