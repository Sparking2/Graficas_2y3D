import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by xXGNWXxNero on 5/23/2016.
 */
public class MainClass extends JFrame{
    BufferedImage buffer;
    CCubo cuboParalelo;
    CCubo cuboPerspectiva;
    public static void main(String[] args) {
        new MainClass();
    }
    public MainClass(){
        super("Proyeccion");
        this.setSize(800,600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(false);
        buffer=new BufferedImage(800,600,BufferedImage.TYPE_INT_RGB);

        cuboParalelo=new CCubo(35,10,-10,150);
        cuboParalelo.setPlano(3,1,2);//relacion unidades 10/1

        cuboPerspectiva=new CCubo(10,40,-5,50);//esta controla de que tan lejos inicia
        cuboPerspectiva.setPlano(45,30,10);//esta controla que tan lejos estoy de la figura
        while (true){

            for(int x=0;x<100;x++){
                buffer.getGraphics().clearRect(0,0,800,600);
                cuboPerspectiva.setPlano(45-x,30,10+x);
                cuboPerspectiva.setInicio(10,40,-5,50+x);
                cuboPerspectiva.dibujarCubo(buffer,"perspectiva", new Color(200,200,200));
                cuboParalelo.dibujarCubo(buffer,"paralela", new Color(200,200,200));
                this.getGraphics().drawImage(buffer,0,0,null);
            }
            for(int x=100;x>0;x--){
                buffer.getGraphics().clearRect(0,0,800,600);
                cuboPerspectiva.setPlano(45-x,30,10+x);
                cuboPerspectiva.setInicio(10,40,-5,50+x);
                cuboPerspectiva.dibujarCubo(buffer,"perspectiva", new Color(200,200,200));
                cuboParalelo.dibujarCubo(buffer,"paralela", new Color(200,200,200));
                this.getGraphics().drawImage(buffer,0,0,null);
            }

        }

    }
}
