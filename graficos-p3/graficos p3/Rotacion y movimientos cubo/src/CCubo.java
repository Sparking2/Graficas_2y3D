import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by xXGNWXxNero on 5/24/2016.
 */
public class CCubo {
    int px=0, py=0,pz=0,dx=0,dy=0;
    float m=0,b=0;
    ArrayList<Point3D> puntosCubo=new ArrayList<Point3D>();
    Graphics gra;

    public CCubo(int cuboX, int cuboY, int cuboZ, int distX, int distY,int distZ){
        setInicio(cuboX, cuboY,cuboZ,distX,distY,distZ);
    }

    public void setInicio(int cuboX, int cuboY, int cuboZ, int distX, int distY,int distZ){
        puntosCubo.clear();
        puntosCubo.add(new Point3D(cuboX+distX, cuboY-distY,cuboZ-distZ));
        puntosCubo.add(new Point3D(cuboX+distX, cuboY-distY,cuboZ+distZ));
        puntosCubo.add(new Point3D(cuboX-distX, cuboY-distY,cuboZ+distZ));
        puntosCubo.add(new Point3D(cuboX-distX, cuboY-distY,cuboZ-distZ));

        puntosCubo.add(new Point3D(cuboX+distX, cuboY+distY,cuboZ-distZ));
        puntosCubo.add(new Point3D(cuboX+distX, cuboY+distY,cuboZ+distZ));
        puntosCubo.add(new Point3D(cuboX-distX, cuboY+distY,cuboZ+distZ));
        puntosCubo.add(new Point3D(cuboX-distX, cuboY+distY,cuboZ-distZ));
    }
    public ArrayList<Point3D> getPuntos3D(){
        return puntosCubo;
    }
    public void setPuntos3D(ArrayList<Point3D> puntos){
        this.puntosCubo=puntos;
    }
    public void dibujarCubo(BufferedImage b, String proyeccion, Color c){
        gra=b.getGraphics();
        ArrayList<Point> puntos2D=puntos3D2D(proyeccion);
        lineaDDA(puntos2D.get(0).x,puntos2D.get(0).y,puntos2D.get(1).x,puntos2D.get(1).y,c);
        lineaDDA(puntos2D.get(0).x,puntos2D.get(0).y,puntos2D.get(3).x,puntos2D.get(3).y,c);
        lineaDDA(puntos2D.get(0).x,puntos2D.get(0).y,puntos2D.get(4).x,puntos2D.get(4).y,c);
        lineaDDA(puntos2D.get(1).x,puntos2D.get(1).y,puntos2D.get(2).x,puntos2D.get(2).y,c);
        lineaDDA(puntos2D.get(1).x,puntos2D.get(1).y,puntos2D.get(5).x,puntos2D.get(5).y,c);
        lineaDDA(puntos2D.get(2).x,puntos2D.get(2).y,puntos2D.get(3).x,puntos2D.get(3).y,c);
        lineaDDA(puntos2D.get(2).x,puntos2D.get(2).y,puntos2D.get(6).x,puntos2D.get(6).y,c);
        lineaDDA(puntos2D.get(3).x,puntos2D.get(3).y,puntos2D.get(7).x,puntos2D.get(7).y,c);
        lineaDDA(puntos2D.get(4).x,puntos2D.get(4).y,puntos2D.get(5).x,puntos2D.get(5).y,c);
        lineaDDA(puntos2D.get(4).x,puntos2D.get(4).y,puntos2D.get(7).x,puntos2D.get(7).y,c);
        lineaDDA(puntos2D.get(5).x,puntos2D.get(5).y,puntos2D.get(6).x,puntos2D.get(6).y,c);
        lineaDDA(puntos2D.get(6).x,puntos2D.get(6).y,puntos2D.get(7).x,puntos2D.get(7).y,c);
    }

    private ArrayList<Point> puntos3D2D(String proyeccion){
        ArrayList<Point> puntos2D=new ArrayList<Point>();
        switch (proyeccion){
            case "paralela":
                for(int x=0;x<puntosCubo.size();x++)
                    puntos2D.add(paralela(puntosCubo.get(x)));
                break;
            case "perspectiva":
                for(int x=0;x<puntosCubo.size();x++)
                    puntos2D.add(perspectiva(puntosCubo.get(x)));
                break;
        }
        return puntos2D;
    }
    private Point perspectiva(Point3D punto){
        double u=-pz/(punto.z-pz);
        double x=px+(punto.x-px)*u;
        double y=py+(punto.y-py)*u;
        return  new Point((int)x,(int)y);
    }
    private Point paralela(Point3D punto){
        double x=punto.x-(px*punto.z)/pz;
        double y=punto.y-(py*punto.z)/pz;
        return new Point((int)x,(int)y);
    }


    private void lineaDDA(int x0, int y0, int x1, int y1, Color c){
        dx = x1 - x0;
        dy = y1 - y0;//new Color(1,1,1);
        //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
        putPixel(x0,y0,c);

        if (Math.abs(dx) > Math.abs(dy)) {
            m = (float) dy / (float) dx;
            b = y0 - m*x0;
            if(dx<0)
                dx =  -1;
            else
                dx =  1;
            while (x0 != x1) {
                x0 += dx;
                y0 = Math.round(m*x0 + b);
                //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
                putPixel(x0,y0,c);
            }
        }
        else if (dy != 0) {
            m= (float) dx / (float) dy;
            b = x0 - m*y0;
            if(dy<0)
                dy =  -1;
            else
                dy =  1;
            while (y0 != y1) {
                y0 += dy;
                x0 = Math.round(m*y0 + b);
                //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
                putPixel(x0,y0,c);
            }
        }
    }

    private void putPixel(int x, int y, Color c){
        gra.setColor(c);
        gra.drawLine(x, y, x, y);

    }

    public void setPlano(int px, int py, int pz){
        this.px=px;
        this.py=py;
        this.pz=pz;
    }
}
