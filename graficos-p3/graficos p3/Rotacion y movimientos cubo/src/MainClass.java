import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

/**
 * Created by xXGNWXxNero on 5/24/2016.
 */
public class MainClass extends JFrame implements KeyListener{
    BufferedImage buffer;
    CCubo cubo;
    Transformaciones trans;
    int TRX=0,TRY=0,TRZ=0, ETRA=0;
    String estadoTransformacion="012";//0=translacion, 1= escala, 2= rotacion
    public static void main(String[] args) {
        new MainClass();
    }

    public MainClass() {
        super("Transformaciones");
        this.setSize(800, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(false);
        this.addKeyListener(this);
        trans=new Transformaciones();
        buffer = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
        cubo=new CCubo(400, 300, 0, 20,20,20);
        cubo.setPlano(1,1,1);
        cubo.dibujarCubo(buffer,"paralela", new Color(200,200,200));
        for(int x=0;x<50;x++)
            this.getGraphics().drawImage(buffer,0,0,this);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyChar()=='|'){
            ETRA=ETRA==0?1:ETRA==1?2:0;
        }

        switch (e.getKeyChar()){
            case 'd':
                TRX+=1;
                break;
            case 'a':
                TRX-=1;
                break;
            case 's':
                TRY+=1;
                break;
            case 'w':
                TRY-=1;
                break;
            case'q':
                TRZ+=1;
                break;
            case 'e':
                TRZ-=1;
                break;
        }
        switch (ETRA){
            case 0:
                cubo.setPuntos3D(trans.traslacion(TRX*5,TRY*5,TRZ*5,cubo.getPuntos3D()));
                break;
            case 1:
                cubo.setPuntos3D(trans.escala(TRX,TRY,TRZ,cubo.getPuntos3D()));
                break;
            case 2:
                cubo.setPuntos3D(trans.rotacion(TRX*1,TRY*1,TRZ*1 ,cubo.getPuntos3D()));
                break;
        }
        buffer.getGraphics().clearRect(0,0,800,600);
        cubo.dibujarCubo(buffer,"paralela", new Color(200,200,200));
        this.getGraphics().drawImage(buffer,0,0,this);
        TRX=0;
        TRY=0;
        TRZ=0;
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
