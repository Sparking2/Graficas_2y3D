import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by xXGNWXxNero on 5/22/2016.
 */
public class CPerspectiva {
    int px=0, py=0,pz=0,dx=0,dy=0,aux=0, avance=0, seed=0,col= Color.BLACK.getRGB();
    float m=0,b=0;
    Graphics gra;

    public void lineaDDA3D(int xo, int yo, int zo, int xd, int yd, int zd, BufferedImage b){
        gra=b.getGraphics();

        lineaDDA(G3D2D(xo,px,zo,pz),G3D2D(yo,py,zo,pz),G3D2D(xd,px,zd,pz),G3D2D(yd,py,zd,pz), new Color(200,200,200));
    }

    private void lineaDDA(int x0, int y0, int x1, int y1, Color c){
        dx = x1 - x0;
        dy = y1 - y0;//new Color(1,1,1);
        //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
        putPixel(x0,y0,c);

        if (Math.abs(dx) > Math.abs(dy)) {
            m = (float) dy / (float) dx;
            b = y0 - m*x0;
            if(dx<0)
                dx =  -1;
            else
                dx =  1;
            while (x0 != x1) {
                x0 += dx;
                y0 = Math.round(m*x0 + b);
                //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
                putPixel(x0,y0,c);
            }
        }
        else if (dy != 0) {
            m= (float) dx / (float) dy;
            b = x0 - m*y0;
            if(dy<0)
                dy =  -1;
            else
                dy =  1;
            while (y0 != y1) {
                y0 += dy;
                x0 = Math.round(m*y0 + b);
                //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
                putPixel(x0,y0,c);
            }
        }
    }

    private int G3D2D(int pTrans, int pPlano, int zTrans, int zPlano){
        if(zTrans-zPlano==0)
            zTrans++;
        return pPlano-((pTrans-pPlano)*zPlano)/(zTrans-zPlano);
    }
    private void putPixel(int x, int y, Color c){
        gra.setColor(c);
        gra.drawLine(x, y, x, y);
    }
    public void setPlano(int px, int py, int pz){
        this.px=px;
        this.py=py;
        this.pz=pz;
    }
    public CPerspectiva(int px, int py, int pz){
        this.px=px;
        this.py=py;
        this.pz=pz;
    }
}
