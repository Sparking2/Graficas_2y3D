import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

/**
 * Created by xXGNWXxNero on 5/27/2016.
 */
public class MainClass extends JFrame implements KeyListener{
    BufferedImage buffer;
    int TRX=0,TRY=0,TRZ=0, ETRA=0;
    int [][]z_Buffer;
    Boolean estilo;
    Plataforma plataforma;
    Transformaciones trans;
    String proyeccion;
    Color c;
    Point3D origen;
    public static void main(String[] args) {
        new MainClass();
    }

    public MainClass() {
        super("Transformaciones");
        this.setSize(800, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(false);
        this.addKeyListener(this);
        c=new Color(200,200,200);
        proyeccion="paralela";
        estilo=true;
        trans=new Transformaciones();
        z_Buffer=new int[this.getWidth()][this.getHeight()];
        origen=new Point3D(400,100,0);
        buffer = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);

        //en esta perspectiva las lineas de x representaran las de y y viceversa, z es z
        //cada unidad en la seccion y representa media onda de la figura
        plataforma=new Plataforma(1,1,-100, 80, 2, 30, origen, proyeccion);//plano(3 variables), lineas x, lineas y(veces que se repetira el ciclo onda), puntos por linea



        for(int x=0;x<1;x++){
            buffer.getGraphics().clearRect(0,0,800,600);
            //limpiar, dibujar, colorear, sino da error
            plataforma.setMalla(trans.rotacion(0,TRY,TRZ ,plataforma.getMalla()));
            plataforma.dibujarPlataforma(estilo, false,  buffer, c);//se sale de los bordes porque aun no esta en pantalla
            this.getGraphics().drawImage(buffer,0,0,null);
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyChar()=='|'){
            ETRA=ETRA==0?1:ETRA==1?2:0;
        }

        switch (e.getKeyChar()){
            case 'd':
                TRX+=1;
                break;
            case 'a':
                TRX-=1;
                break;
            case 's':
                TRY+=1;
                break;
            case 'w':
                TRY-=1;
                break;
            case'q':
                TRZ+=1;
                break;
            case 'e':
                TRZ-=1;
                break;
            case ' ':
                estilo=!estilo;
                break;
        }
        switch (ETRA){
            case 0:
                plataforma.setMalla(trans.traslacion(TRX*5,TRY*5,TRZ*5,plataforma.getMalla()));
                break;
            case 1:
                plataforma.setMalla(trans.escala(TRX,TRY,TRZ,plataforma.getMalla()));
                break;
            case 2:
                plataforma.setMalla(trans.rotacion(TRX*4,TRY*4,TRZ*4 ,plataforma.getMalla()));
                break;
        }

        buffer.getGraphics().clearRect(0,0,800,600);
        plataforma.dibujarPlataforma(estilo, false, buffer, c);
        this.getGraphics().drawImage(buffer,0,0,this);
        TRX=0;
        TRY=0;
        TRZ=0;
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
