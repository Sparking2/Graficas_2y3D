import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by xXGNWXxNero on 5/28/2016.
 */
public class Plataforma {
    ArrayList<Curva> malla;
    int tamX=0;
    float puntosPorLinea;

    public Plataforma(int px, int py, int pz, int tamX, int tamY,int puntosPorLinea, Point3D puntoInicial, String proyeccion){
        malla=new ArrayList<Curva>();
        this.tamX=tamX;
        this.puntosPorLinea=puntosPorLinea/tamY;
        for(int x=0;x<this.tamX;x++){
            malla.add(new Curva(px, py, pz, (int)this.puntosPorLinea ,tamY, 50*x+1,puntoInicial, proyeccion));//establecemos el plano, la funcion curva le da la forma
        }
    }

    public  void dibujarPlataforma(Boolean estilo, Boolean colorear, BufferedImage b, Color c){
        enlazarMalla(estilo, b, c);//despues de aqui el buffer ya tiene la  imagen dibujada
        if(colorear){
            colorearMalla( b, c);

        }

    }
    private  void colorearMalla(BufferedImage b, Color c){
        if(malla.size()>1){
            for (int x=1;x<malla.size();x++){
                malla.get(x).dibujarPuntosMalla(malla.get(x).getPuntos3D(), malla.get(x-1).getPuntos3D(), b, c);
            }
        }
    }
    private  void enlazarMalla(Boolean estilo, BufferedImage b, Color c){
        if(malla.size()>1){
            for (int x=1;x<malla.size();x++){//el sig dibujara hacia su anterior
                malla.get(x).enlazarPuntosMalla(malla.get(x).getPuntos3D(), malla.get(x-1).getPuntos3D(),estilo, b, c);
                //||ddddddibujarPlataforma(true, true, b , c);
            }
        }
    }

    public void setMalla(ArrayList<Curva> malla){
        this.malla=malla;
    }

    public ArrayList<Curva> getMalla(){
        return this.malla;
    }
}
