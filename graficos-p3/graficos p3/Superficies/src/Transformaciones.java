import java.util.ArrayList;

/**
 * Created by xXGNWXxNero on 5/24/2016.
 */
public class Transformaciones {

    public ArrayList<Curva> traslacion(float Tx, float Ty, float Tz,ArrayList<Curva> malla){

        for (int x=0;x<malla.size();x++){
            for(int y=0;y<malla.get(x).getPuntos3D().size();y++){
                malla.get(x).getPuntos3D().get(y).x+=Tx;
                malla.get(x).getPuntos3D().get(y).y+=Ty;
                malla.get(x).getPuntos3D().get(y).z+=Tz;
            }
        }
        return malla;
    }

    public ArrayList<Curva> escala(int Ex, int Ey, int Ez,ArrayList<Curva> malla){
        float x=Ex*.02f,y=Ey*.02f, z=Ez*.02f, centroMalla=malla.size()/2f, centroPuntos=malla.get(0).getPuntos3D().size()/2;
        float distX=x*malla.get((int)centroMalla).getPuntos3D().get((int)centroPuntos).x,
              distY=y*malla.get((int)centroMalla).getPuntos3D().get((int)centroPuntos).y,
              distZ=z*malla.get((int)centroMalla).getPuntos3D().get((int)centroPuntos).x;//420,280,-20--380,280,-20////428.4,280,-20--387.6,280,-20

        for (int m=0;m<malla.size();m++){
            for(int n=0;n<malla.get(m).getPuntos3D().size();n++){
                malla.get(m).getPuntos3D().get(n).x = x*malla.get(m).getPuntos3D().get(n).x + malla.get(m).getPuntos3D().get(n).x;
                malla.get(m).getPuntos3D().get(n).y = y*malla.get(m).getPuntos3D().get(n).y + malla.get(m).getPuntos3D().get(n).y;
                malla.get(m).getPuntos3D().get(n).z = z*malla.get(m).getPuntos3D().get(n).z + malla.get(m).getPuntos3D().get(n).z;
            }
        }
        traslacion(-distX,-distY,-distZ,malla);
        return malla;
    }

    public ArrayList<Curva> rotacion(float Rx, float Ry, float Rz,ArrayList<Curva> malla){
        Point3D centro=distanciaOrigenCentro(malla);//regresa la posicion del centro de mi figura en el mapa

        traslacion(-centro.x,-centro.y,-centro.z,malla);//movemos al origen
        if(Rx!=0){
            Rx=(float)Math.toRadians(Rx);

            for (int x=0;x<malla.size();x++){
                for(int y=0;y<malla.get(x).getPuntos3D().size();y++){
                    malla.get(x).getPuntos3D().get(y).y=(float)(
                            malla.get(x).getPuntos3D().get(y).y * Math.cos(Rx) -
                            malla.get(x).getPuntos3D().get(y).z * Math.sin(Rx)
                            );
                    malla.get(x).getPuntos3D().get(y).z = (float)(
                            malla.get(x).getPuntos3D().get(y).y * Math.sin(Rx) +
                            malla.get(x).getPuntos3D().get(y).z * Math.cos(Rx)
                            );
                }
            }

            /*for(int x=0;x<puntos.size();x++){
                puntos.get(x).y =(float) (puntos.get(x).y * Math.cos(Rx) - puntos.get(x).z * Math.sin(Rx));
                puntos.get(x).z =(float) (puntos.get(x).y * Math.sin(Rx) + puntos.get(x).z * Math.cos(Rx));
            }*/
        }
        if(Ry!=0){
            Ry=(float)Math.toRadians(Ry);
            for (int x=0;x<malla.size();x++){
                for(int y=0;y<malla.get(x).getPuntos3D().size();y++){
                    malla.get(x).getPuntos3D().get(y).x=(float)(
                            malla.get(x).getPuntos3D().get(y).x * Math.cos(Ry) +
                            malla.get(x).getPuntos3D().get(y).z * Math.sin(Ry)
                    );
                    malla.get(x).getPuntos3D().get(y).z = (float)(
                            malla.get(x).getPuntos3D().get(y).z * Math.cos(Ry)
                            - malla.get(x).getPuntos3D().get(y).x * Math.sin(Ry)

                    );
                }
            }

            /*Ry=(float)Math.toRadians(Ry);
            for(int x=0;x<puntos.size();x++){
                puntos.get(x).x = (float) (puntos.get(x).x * Math.cos(Ry) + puntos.get(x).z * Math.sin(Ry));
                puntos.get(x).z = (float) ( - puntos.get(x).x *  Math.sin(Ry) + puntos.get(x).z * Math.cos(Ry));
            }*/
        }
        if(Rz!=0){
            Rz=(float)Math.toRadians(Rz);
            for (int x=0;x<malla.size();x++){
                for(int y=0;y<malla.get(x).getPuntos3D().size();y++){
                    malla.get(x).getPuntos3D().get(y).x=(float)(
                            malla.get(x).getPuntos3D().get(y).x * Math.cos(Rz) -
                            malla.get(x).getPuntos3D().get(y).y * Math.sin(Rz)
                    );
                    malla.get(x).getPuntos3D().get(y).y = (float)(
                            malla.get(x).getPuntos3D().get(y).x * Math.sin(Rz) +
                            malla.get(x).getPuntos3D().get(y).y * Math.cos(Rz)
                    );
                }
            }
            /*for(int x=0;x<puntos.size();x++){
                puntos.get(x).x =(float) (puntos.get(x).x * Math.cos(Rz) - puntos.get(x).y * Math.sin(Rz));
                puntos.get(x).y =(float) (puntos.get(x).x * Math.sin(Rz) + puntos.get(x).y * Math.cos(Rz));
            }*/
        }
        System.out.println(centro.x );
        System.out.println(centro.y );
        System.out.println(centro.z );

        traslacion(centro.x,centro.y,centro.z,malla);//movemos al origen
        return malla;
    }

    private Point3D distanciaOrigenCentro(ArrayList<Curva> malla){//ver de todos los puntos cual es el centrico en x, en y y en z
        int centroX=malla.size()/2;
        int centroY=malla.get(0).getPuntos3D().size()/2;
        return  new Point3D(malla.get(centroX).getPuntos3D().get(centroY).x,malla.get(centroX).getPuntos3D().get(centroY).y,malla.get(centroX).getPuntos3D().get(centroY).z);
        //int puntoCentrico= (int)puntos.size()/2;
        //return  new Point3D(puntos.get(puntoCentrico).x,puntos.get(puntoCentrico).y,puntos.get(puntoCentrico).z); //obtenemos la posicion del centro en el mapa
    }
}
