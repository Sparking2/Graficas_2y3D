/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;


public class Buffer {
    Graphics gra;
    BufferedImage buffer;

public Buffer(){
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
    }

      public void PutPixel(int x, int y, Color c){
           buffer.setRGB(0, 0, c.getRGB());
           gra.drawImage(buffer,x,y,null);
    }
      
        public void LineD(int x0,int y0, int x1, int y1,Graphics g, Color col){
        gra = g;
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1)){
            int p = 2 * rodzil;
            if (p > -dy){
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            
            if (p < dx){
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
            }
        PutPixel(x0, y0, col);  
        }
    }
}

 
