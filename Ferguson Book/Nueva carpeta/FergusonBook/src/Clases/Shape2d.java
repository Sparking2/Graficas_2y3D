/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.Vector;
import java.util.*;
import java.awt.*;

class Shape2d{
    private Point2d localOrigin = new Point2d(0,0);
    private Vector lines = new Vector();
    private int numberOfLines = 0;
    
    public void addLine(Line2d inLine){
        lines.add(inLine);
        numberOfLines = numberOfLines +1;
    }
    
    public void addLine(float x1, float y1, float x2, float y2){
        Line2d myLine = new Line2d(x1,y1,x2,y2);
        lines.add(myLine);
        numberOfLines = numberOfLines + 1;
    }
    
    public void draw(Graphics g){
        for (int i = 0; i < numberOfLines; i = i + 1){
            ((Line2d)lines.get(i)).draw(g);
        }//fin for
    }//fin draw
}//end class
