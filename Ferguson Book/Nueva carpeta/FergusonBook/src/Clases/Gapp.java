/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Gapp extends JFrame {
    
    private Shape2d myShape = new Shape2d();
    private Drawing2d myDrawing = new Drawing2d();
    private Gcanvas myCanvas = new Gcanvas();
    
    public Gapp(){
        setBackground(Color.white);
        myDrawing.addShape(myShape);
        myCanvas.setDrawing(myDrawing);
        myShape.addLine((float)50,(float) 50,(float)50, (float)150);
        myShape.addLine((float)50,(float) 100,(float)100, (float)100);  //<-----
        myShape.addLine((float)100,(float) 100,(float)100, (float)150);
        myShape.addLine((float)150,(float) 150,(float)150, (float)100);
       
    }
    
    public void initComponents(){
        
        setLocation(new Point(0,30));
        setSize(650,400);
        setTitle("Graphics Application");
        getContentPane().add(myCanvas);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    static public void main(String[] args){
        try{
             Gapp myGapp = new Gapp();
            myGapp.initComponents();
            myGapp.setVisible(true);
        }catch (Exception e){
            e.printStackTrace();
        }
       
    }
}