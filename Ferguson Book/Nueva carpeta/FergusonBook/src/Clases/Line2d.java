
package Clases;

import java.awt.Color;
import java.awt.Graphics;

class Line2d {
    private Point2d src;
    private Point2d dest;
    
    public Line2d (float x1, float y1, float x2, float y2){
        src = new Point2d(x1,y1);
        dest = new Point2d(x2,y2);
    }//fin constructor
    
    public void draw(Graphics g){
        g.drawLine((int)src.x(), (int)src.y(), (int)dest.x(), (int)dest.x());
        //new Buffer().LineD((int)src.x(), (int)src.y(), (int)dest.x(), (int)dest.x(), g, Color.red);
    }
}

        