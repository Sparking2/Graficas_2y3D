/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.Vector;
import java.util.*;
import java.awt.*;

class Drawing2d{
    private Vector shapes = new Vector();
    int numberOfShapes = 0;
    
    public void addShape(Shape2d inShape){
        shapes.add(inShape);
        numberOfShapes = numberOfShapes + 1;
    }
    
    public void draw(Graphics g){
        for(int i = 0; i < numberOfShapes; i = i + 1){
            ((Shape2d)shapes.get(i)).draw(g);
        }
    }
}