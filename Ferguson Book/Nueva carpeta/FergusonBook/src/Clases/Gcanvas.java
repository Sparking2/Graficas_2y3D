
package Clases;

import java.awt.*;
import javax.swing.*;

public class Gcanvas extends JPanel{
    private Drawing2d myDrawing;
    
    public void setDrawing(Drawing2d inDrawing){
        myDrawing = inDrawing;
    }
    
    @Override
    public void paintComponent(Graphics g){
        myDrawing.draw(g);
    }
}
