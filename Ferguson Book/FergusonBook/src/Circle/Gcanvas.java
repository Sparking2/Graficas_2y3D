/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Circle;

import java.awt.*;
import javax.swing.*;
import javax.swing.JPanel;

/**
 *
 * @author spark
 */
public class Gcanvas extends JPanel{
    
    public void eightPlot(int x, int y, int xoffset,int yoffset, Graphics g){
        int xPlot = Math.round(xoffset + x);
        int yPlot = Math.round(yoffset + y);
        g.drawLine(xPlot, yPlot, xPlot, yPlot);
        
        xPlot = Math.round(xoffset + x );
        yPlot = Math.round(yoffset - y);
        g.drawLine(xPlot,yPlot,xPlot,yPlot);
        
        xPlot = Math.round(xoffset - x);
        yPlot = Math.round(yoffset - y);
        g.drawLine(xPlot,yPlot,xPlot,yPlot);
        
        xPlot = Math.round(xoffset - x);
        yPlot = Math.round(yoffset + y);
        g.drawLine(xPlot,yPlot,xPlot,yPlot);
        
        xPlot = Math.round(xoffset + y);
        yPlot = Math.round(yoffset + x);
        g.drawLine(xPlot,yPlot,xPlot,yPlot);
        
        xPlot = Math.round(xoffset + y );
        yPlot = Math.round(yoffset - x);
        g.drawLine(xPlot,yPlot,xPlot,yPlot);
        
        xPlot = Math.round(xoffset - y);
        yPlot = Math.round(yoffset - x);
        g.drawLine(xPlot,yPlot,xPlot,yPlot);
        
        xPlot = Math.round(xoffset - y);
        yPlot = Math.round(yoffset + x);
        g.drawLine(xPlot,yPlot,xPlot,yPlot);
    }//eightPlot
    
    
 }

