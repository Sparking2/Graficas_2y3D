
package Circle;

//Archivo Gapp.java

import BresenhamLine.*;
import Apollo13.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Gapp extends JFrame{
    
    public Gapp(){
        setBackground(Color.white);
        setSize(400,500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Bresenham");
        
    }//Constructor
    
    public void initComponents(){
        getContentPane().add(new Gcanvas());
    }
    
        public static void main(String[] args){
            
                Gapp myGapp = new Gapp();
                myGapp.initComponents();
                myGapp.setVisible(true);
          
        }   
    }
