
package BresenhamLine;

import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.*;

public class Gcanvas extends JPanel {
    
    int deltaX,deltaY,DY2,DX2,Di;
    
    public void bresenhamLine(int x1, int y1,int x2,int y2,Graphics g){
        deltaX = x2-x1;
        deltaY = y2-y1;
        DY2 = 2* deltaY;
        DX2 = 2* deltaX;
        
        Di = DY2 - deltaX;
        
        int x = x1;
        int y = y1;
        int prevy;
        
        while(x<x2){
            x++;
            prevy = y;
            if(Di > 0){
                y++;
            }
            g.drawLine(x, y, x, y);
            Di = Di + DY2 - (DX2 * (y - prevy));
        }//while
    }//bresenham
    
    public void paintComponent(Graphics g){
        bresenhamLine(50,50,150,60,g);
        bresenhamLine(50,50,150,120,g);
        bresenhamLine(50,50,150,140,g);
        bresenhamLine(50,50,150,200,g);
    }//paintComponent
}
