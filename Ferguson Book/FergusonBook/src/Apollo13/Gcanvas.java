package Apollo13;

import java.awt.*;
import javax.swing.*;

public class Gcanvas extends JPanel{
    
    public void paintComponent(Graphics g){
        g.drawLine(50, 50, 50, 150);
        g.drawLine(50,150,100,100);
        g.drawLine(100, 100, 100, 150);
        g.drawLine(100, 150, 200, 150);
        g.drawLine(200, 150, 200, 50);
        g.drawLine(200, 50, 100, 50);
        g.drawLine(100,50,100,100);
        g.drawLine(100, 100, 50, 50);
        g.drawLine(200,50,250,100);
        g.drawLine(250, 100, 200, 150);
    }//paintComponent
}
