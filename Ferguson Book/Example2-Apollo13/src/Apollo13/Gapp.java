
package Apollo13;

//Archivo Gapp.java

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Gapp extends JFrame{
    
    public Gapp(){
        setBackground(Color.white);
    }//Constructor
    
    public void initComponents() throws Exception{
        setLocation(new java.awt.Point(0,30));
        setSize(new java.awt.Dimension(350,400));
        setTitle("Graphics Application");
        getContentPane().add(new Gcanvas());
        
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e){
                thisWindowClosing(e);
            }

            private void thisWindowClosing(WindowEvent e) {
                //Cierra la ventana cuando se le pica a cerrar
                setVisible(false);
                dispose();
                System.exit(0);
            }
        });
    }
        public static void main(String[] args){
            try{
                Gapp myGapp = new Gapp();
                myGapp.initComponents();
                myGapp.setVisible(true);
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }   
    }
