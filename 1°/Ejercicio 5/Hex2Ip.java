/*

	Ejercicio 5
	Desarrolle una clase Hex2IPconvertidora de direcciones IP
	a formato hexadecimal y viceversa.
	La clase recibe dos parámetros:
		El primer parámetro es "-hex" (dirección -> IP) o
		"-ip" (convertir de IP -> hexadecimal)

		El segundo parametro es una cadena hex/ip

*/
		import java.io.*;
		import java.util.Scanner;

		public class Hex2Ip
		{
			public static void main(String [] args)
			{
				String cadena = "";
				cadena = args[0];
				String otro = "";
				otro = args[1];
				System.out.println("");

				switch(cadena)
				{
					case "-ip":
					System.out.println("entro a ip " + otro);
					String[] codigo = {"","","",""};
					
					codigo[0] = otro.substring(0,2);
					codigo[1] = otro.substring(2,4);
					codigo[2] = otro.substring(4,6);
					codigo[3] = otro.substring(6,8);

					String salida = "";
					for(int i = 0; i < 4; i++)
					{
						salida += Integer.valueOf(codigo[i],16);
						salida += '.';
					}
					System.out.println("Resultado " + salida);					

					break;

					case "-hex":
					System.out.println("entro a hexadecimal " + otro);
					String[] ip = otro.split("\\.");
					int var = 0;
					String hexadecimal = "";
					for (int i = 0;i < 4; i++)
					{
						var = Integer.parseInt(ip[i]);
						if(Integer.toHexString(var).length() < 2)
						{
							hexadecimal += "0";
						}
						hexadecimal += Integer.toHexString(var);
					}
					System.out.println("Resultado " + hexadecimal);	
					break;

					default:
					System.out.println("Error");
					break;
				}

			}
		}