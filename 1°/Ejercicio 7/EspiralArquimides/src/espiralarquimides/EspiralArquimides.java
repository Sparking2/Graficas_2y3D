/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package espiralarquimides;
import java.awt.Graphics;
import javax.swing.*;
import java.lang.Runnable;
import java.math.*;


/**
 *
 * @author Cortes Marquez
 */
public class EspiralArquimides extends JFrame implements Runnable {
    
    int entero = 1;
    int centerX = 400;
    int centerY = 300;
    
    int arcWidth = 1;
    int arcGrowDelta = 5;
    
    //Hilo
    private Thread thr;

    public EspiralArquimides(){
        super("Espiral de Arquimides");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(800,600);
        //add(new Button("Hola"));
        //add(new Panel());
        show();
        thr = new Thread(this);
        thr.start();
        }
    
    @Override
    public void run() {
        while(entero < 50){
            try{
                thr.sleep(150);
            } catch (InterruptedException es){
            }
            repaint();
        }
    }
    @Override
    public void paint(Graphics g)
        {  
            entero++;
             //funciona asi lugar en X,Lugar en Y,Ancho,Alto,angulo de inicio,Angulo del arco
            g.drawArc(centerX - arcWidth, centerY - arcWidth, 2 * arcWidth, 2 * arcWidth, 0, 180);
            arcWidth += arcGrowDelta;
            g.drawArc(centerX - arcWidth, centerY - arcWidth, 2 * arcWidth - arcGrowDelta, 2 * arcWidth, 180, 180);
            //g.drawArc(400,300,entero,entero, 0+entero, 360+entero);
        }
    
    public void spiral(int entero)
    {
        
    }
    
   /* //@Override
    public void repaint(Graphics g)
    {
        
    }*/
       /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         new EspiralArquimides();
         
    }
    
}
