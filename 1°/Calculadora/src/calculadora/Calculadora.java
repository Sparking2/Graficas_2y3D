// Práctica 13 - Calculadora.

// Declaración de Paquetes.
package calculadora;

// Declaración de Importaciones.
import java.awt.*;
import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// @author José Ricardo

// Declaración de Clase.
public class Calculadora extends JFrame {

    public Calculadora() {
        super ("Calculadora");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Panel Calculadora.
        CalculadoraPanel CPanel = new CalculadoraPanel();
        add(CPanel);
    }
        
    public static void main(String[] args) {
        // Frame Calculadora.
        Calculadora CFrame = new Calculadora();
        CFrame.setSize(270, 320);
        CFrame.setResizable(false);
        CFrame.show();
    }
}

class CalculadoraPanel extends JPanel {
    // Propiedades.
    private double Resultado;
    private boolean Start;
    private String LastCommand;
    private JButton ResultadoBox;
    private JPanel TableroPanel;

    public CalculadoraPanel() {
        setLayout(new BorderLayout());
                  
        ActionListener Insert = new InsertAction();
        ActionListener Command = new CommandAction();
        
        // Inicializar Valores
        Resultado = 0;
        LastCommand = "=";
        Start = true;
        
        // Caja Resultado
        ResultadoBox = new JButton("0");
        ResultadoBox.setEnabled(false);
        ResultadoBox.setHorizontalAlignment(SwingConstants.LEFT);
        add(ResultadoBox, BorderLayout.NORTH);
        
        // Panel
        GridBagLayout GBL = new GridBagLayout();
        GridBagConstraints Celda = new GridBagConstraints();
        TableroPanel = new JPanel();
        TableroPanel.setLayout(GBL);
        add(TableroPanel, BorderLayout.CENTER);
        
        // Botones
        JButton BtnC = new JButton("C");
        Celda.gridx = 0; // Columna.
        Celda.gridy = 0; // Fila.
        Celda.gridwidth = 1; // Area en Columnas.
        Celda.gridheight = 1; // Area en Filas.
        Celda.weightx = 1.0; //100% Ancho.
        Celda.weighty = 1.0; //100% Altura.
        Celda.fill = GridBagConstraints.BOTH;
        BtnC.addActionListener(Command);
        TableroPanel.add(BtnC, Celda);
        
        JButton BtnDiv = new JButton("/");
        Celda.gridx = 1; 
        Celda.gridy = 0; 
        Celda.gridwidth = 1; 
        Celda.gridheight = 1; 
        Celda.weightx = 1.0;
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        BtnDiv.addActionListener(Command);
        TableroPanel.add(BtnDiv, Celda);
        
        JButton BtnMul = new JButton("*");
        Celda.gridx = 2; 
        Celda.gridy = 0; 
        Celda.gridwidth = 1;
        Celda.gridheight = 1; 
        Celda.weightx = 1.0;
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        BtnMul.addActionListener(Command);
        TableroPanel.add(BtnMul, Celda);
        
        JButton BtnRes = new JButton("-");
        Celda.gridx = 3;
        Celda.gridy = 0;
        Celda.gridwidth = 1;
        Celda.gridheight = 1;
        Celda.weightx = 1.0;
        Celda.weighty = 1.0;
        Celda.fill = GridBagConstraints.BOTH;
        BtnRes.addActionListener(Command);
        TableroPanel.add(BtnRes, Celda);
        
        JButton Btn7 = new JButton("7");
        Celda.gridx = 0;
        Celda.gridy = 1;
        Celda.gridwidth = 1;
        Celda.gridheight = 1;
        Celda.weightx = 1.0;
        Celda.weighty = 1.0;
        Celda.fill = GridBagConstraints.BOTH;
        Btn7.addActionListener(Insert);
        TableroPanel.add(Btn7, Celda);
        
        JButton Btn8 = new JButton("8");
        Celda.gridx = 1; 
        Celda.gridy = 1; 
        Celda.gridwidth = 1;
        Celda.gridheight = 1;
        Celda.weightx = 1.0; 
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        Btn8.addActionListener(Insert);
        TableroPanel.add(Btn8, Celda);
        
        JButton Btn9 = new JButton("9");
        Celda.gridx = 2;
        Celda.gridy = 1;
        Celda.gridwidth = 1; 
        Celda.gridheight = 1; 
        Celda.weightx = 1.0;
        Celda.weighty = 1.0;
        Celda.fill = GridBagConstraints.BOTH;
        Btn9.addActionListener(Insert);
        TableroPanel.add(Btn9, Celda);
        
        JButton BtnSum = new JButton("+");
        Celda.gridx = 3; 
        Celda.gridy = 1; 
        Celda.gridwidth = 2; 
        Celda.gridheight = 2; 
        Celda.weightx = 1.0;
        Celda.weighty = 1.0;
        Celda.fill = GridBagConstraints.BOTH;
        BtnSum.addActionListener(Command);
        TableroPanel.add(BtnSum, Celda);
        
        JButton Btn4 = new JButton("4");
        Celda.gridx = 0;
        Celda.gridy = 2;
        Celda.gridwidth = 1;
        Celda.gridheight = 1; 
        Celda.weightx = 1.0; 
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        Btn4.addActionListener(Insert);
        TableroPanel.add(Btn4, Celda);
        
        JButton Btn5 = new JButton("5");
        Celda.gridx = 1; 
        Celda.gridy = 2;
        Celda.gridwidth = 1; 
        Celda.gridheight = 1; 
        Celda.weightx = 1.0; 
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        Btn5.addActionListener(Insert);
        TableroPanel.add(Btn5, Celda);
        
        JButton Btn6 = new JButton("6");
        Celda.gridx = 2;
        Celda.gridy = 2;
        Celda.gridwidth = 1; 
        Celda.gridheight = 1; 
        Celda.weightx = 1.0;
        Celda.weighty = 1.0;
        Celda.fill = GridBagConstraints.BOTH;
        Btn6.addActionListener(Insert);
        TableroPanel.add(Btn6, Celda);
        
        JButton Btn1 = new JButton("1");
        Celda.gridx = 0;
        Celda.gridy = 3;
        Celda.gridwidth = 1;
        Celda.gridheight = 1;
        Celda.weightx = 1.0;
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        Btn1.addActionListener(Insert);
        TableroPanel.add(Btn1, Celda);
        
        JButton Btn2 = new JButton("2");
        Celda.gridx = 1; 
        Celda.gridy = 3; 
        Celda.gridwidth = 1; 
        Celda.gridheight = 1; 
        Celda.weightx = 1.0;
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        Btn2.addActionListener(Insert);
        TableroPanel.add(Btn2, Celda);
        
        JButton Btn3 = new JButton("3");
        Celda.gridx = 2; 
        Celda.gridy = 3; 
        Celda.gridwidth = 1; 
        Celda.gridheight = 1; 
        Celda.weightx = 1.0; 
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        Btn3.addActionListener(Insert);
        TableroPanel.add(Btn3, Celda);
        
        JButton BtnIgual = new JButton("=");
        Celda.gridx = 3; 
        Celda.gridy = 3; 
        Celda.gridwidth = 2; 
        Celda.gridheight = 2;
        Celda.weightx = 1.0; 
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        BtnIgual.addActionListener(Command);
        TableroPanel.add(BtnIgual, Celda);
        
        JButton Btn0 = new JButton("0");
        Celda.gridx = 0;
        Celda.gridy = 4; 
        Celda.gridwidth = 2; 
        Celda.gridheight = 1; 
        Celda.weightx = 1.0; 
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        Btn0.addActionListener(Insert);
        TableroPanel.add(Btn0, Celda);
        
        JButton BtnPunto = new JButton(".");
        Celda.gridx = 2; 
        Celda.gridy = 4; 
        Celda.gridwidth = 1; 
        Celda.gridheight = 1; 
        Celda.weightx = 1.0; 
        Celda.weighty = 1.0; 
        Celda.fill = GridBagConstraints.BOTH;
        BtnPunto.addActionListener(Insert);
        TableroPanel.add(BtnPunto, Celda);
    }
    
    private class InsertAction implements ActionListener {
        
        public void actionPerformed(ActionEvent Event) {
            String Input = Event.getActionCommand();
            if (Start) {
                ResultadoBox.setText("");
                Start = false;
            }
            ResultadoBox.setText(ResultadoBox.getText() + Input);
        }
    }
    
    private class CommandAction implements ActionListener {
        
        public void actionPerformed(ActionEvent Event) {
            String Command = Event.getActionCommand();
            if (Start) {
                if (Command.equals("-")) {
                    ResultadoBox.setText(Command);
                    Start = false;
                }
                else LastCommand = Command;
            }
            else {
                Calculadora(Double.parseDouble(ResultadoBox.getText()));
                LastCommand = Command;
                Start = true;
            }
        }
    }
    
    public void Calculadora(double x) {
        if (LastCommand.equals("+")) 
            Resultado += x;
        else if (LastCommand.equals("-")) 
            Resultado -= x;
        else if (LastCommand.equals("*")) 
            Resultado *= x;
        else if (LastCommand.equals("/")) 
            Resultado /= x;
        else if (LastCommand.equals("=")) 
            Resultado = x;
        else if (LastCommand.equals("C")) { 
            Resultado = 0;  
            ResultadoBox.setText(""); 
        }
        else if (LastCommand.equals("%")) 
            Resultado = (x*x)/100;
            
        ResultadoBox.setText("" + Resultado);
    }
}

