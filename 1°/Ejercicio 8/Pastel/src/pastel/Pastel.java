/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pastel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.ActionEvent;
import static java.lang.Boolean.TRUE;
import java.util.ArrayList;
import javax.swing.*;

/**
 *
 * @author spark_000
 */
public class Pastel extends JFrame {
    int i = 0;
    
    int centerX = 350;
    int centerY = 360;
    int total = 0;
    int arcWidth = 100;
    int arcGrowDelta = 5;
    Boolean bandera = new Boolean(false);
    
    public Pastel()
    {
        
        super("Pastel de no chocolatin");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(400,300);
        
        List nombres = new List();
        List cantidades = new List();
        JPanel panel = new JPanel();
        JTextField caja_Nombre = new JTextField(25);
        JTextField caja_Cantidad = new JTextField(25);
        JLabel txt1 = new JLabel("Nombre");
        JLabel txt2 = new JLabel("Cantidad");
        JButton jp_butt = new JButton("Agregar dato");
        JButton jp_butt2 = new JButton("Dibujar");
        jp_butt.setPreferredSize(new Dimension(110,25));
        jp_butt.setPreferredSize(new Dimension(110,25));        
        add(panel);
        
        /*GridLayout gl = new GridLayout(1,2);
        panel.setLayout(gl);*/
        //panel.setLayout(new FlowLayout());
        panel.add(txt1);
        panel.add(caja_Nombre);
        panel.add(txt2);
        panel.add(caja_Cantidad);
        panel.add(jp_butt);
        panel.add(jp_butt2);
        show();
        
        
        
        
        jp_butt2.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                              
               setSize(800,600);
               remove(panel);
               JTextArea textArea = new JTextArea();
               textArea.setSize(50, 50);
               
               String insertar = new String();
               
               for(int i = 0; i<nombres.getItemCount(); i++)
                {
                    insertar += nombres.getItem(i) + " " + cantidades.getItem(i) + "\n";
                }
               
               textArea.setText(insertar);
                add(textArea);
                bandera = true;
                repaint();
            }        });

        
        
        jp_butt.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nombres.add(caja_Nombre.getText());
                cantidades.add(caja_Cantidad.getText());
                int cantidad = Integer.parseInt(caja_Cantidad.getText());
                total = total + cantidad;
                caja_Cantidad.setText(null);
                caja_Nombre.setText(null);
                i++;
            }
        });
    }
    
    
    public void paint(Graphics g)
    {
        if(bandera == true)
        {
           g.setColor(Color.red);
        g.fillArc(centerX - arcWidth, centerY - arcWidth,300, 300, 0, 50);
        g.setColor(Color.BLUE);
        g.fillArc(centerX - arcWidth, centerY - arcWidth,300, 300, 50, 50);
        g.setColor(Color.green);
        g.fillArc(centerX - arcWidth, centerY - arcWidth,300, 300, 100, 260); 
        }
        //public abstract void setColor(Color c);
        //no tocar los primeros 4!!!
       
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new Pastel();
    }
      
}
