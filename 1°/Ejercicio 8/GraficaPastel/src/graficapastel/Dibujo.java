/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graficapastel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.List;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

/**
 *
 * @author spark_000
 */
public class Dibujo extends JFrame{
    
    int centerX = 400;
    int centerY = 300;
    int total = 0;
    int arcWidth = 100;
    int arcGrowDelta = 5;
    
   
    
    List cantidad= new List();
    int totales = 0;
    
    public Dibujo(List cantidades, int total)
    {
        super("El pastel");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(800,600);
        
        JTextArea txtArea = new JTextArea();
        txtArea.setSize(25,25);
        add(txtArea,BorderLayout.PAGE_START);
        
        String insertar = "";
        for(int i = 0; i<cantidades.getItemCount(); i++)
        {
            insertar += "Cantidad" + " No. " + i + "  " + cantidades.getItem(i) + ", ";
        }
        txtArea.setText(insertar);
        show();
        
        cantidad = cantidades;
        totales = total;
        
    }
    
     public void paint(Graphics g)
    
    {
        int R=10,G=20,B=30;
        
        super.paintComponents(g);
        float anterior = 0;
        for(int i = 0; i < cantidad.getRows();i++)
        {
            
            int numero = Integer.parseInt(cantidad.getItem(i));
            float color = (float)i/cantidad.getRows();
            float porciento = ((float)numero/totales)*360;
             Color MyColor = Color.getHSBColor(color,1,1);
            g.setColor(MyColor);
            g.fillArc(centerX - arcWidth, centerY - arcWidth,300, 300, (int)anterior,(int)porciento);
            anterior = anterior + porciento;
            
        }
        
        /*for(int i = 0; i<cantidad.getItemCount(); i++)
        {
            int numero = Integer.parseInt(cantidad.getItem(i));
            g.drawArc(centerX - arcWidth, centerY - arcWidth,300, 300, 0, (numero/totales)*360);
        }*/
          
    }
     
}
