/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graficapastel;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
 *
 * @author spark_000
 */
public class GraficaPastel extends JFrame {

    //Variables Globales para operaciones
    int total = 0;
    int numElem = 0;
    
    public GraficaPastel()
    {
        //Ventana
        super("Elementos");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(200,150);
        
        //Objetos
        List cantidades = new List();
        
        //Componentes
        JPanel pane = new JPanel();
        JButton btnAgregar = new JButton("Agregar");
        JButton btnDibujar = new JButton("Dibujar");
        JTextField TxtCantidad = new JTextField(14);
        
        //Agregar los Componentes al Frame
        pane.add(new JLabel("Agrega cada Porcion"));
        pane.add(TxtCantidad);
        pane.add(btnAgregar);
        pane.add(btnDibujar);
        add(pane);
        
        //mostrar
        show();
        
        //Evento Boton Agregar
        btnAgregar.addActionListener((ActionEvent e) -> {
            cantidades.add(TxtCantidad.getText());
            int cantidad = Integer.parseInt(TxtCantidad.getText());
            total = total + cantidad;
            TxtCantidad.setText(null);
            numElem++;
        });
        
        btnDibujar.addActionListener((ActionEvent e) -> {
            new Dibujo(cantidades,total);
        });
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        new GraficaPastel();
        
    }
    
}
