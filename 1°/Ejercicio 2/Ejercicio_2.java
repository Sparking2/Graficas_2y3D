/*
	Ejercicio 2
	Generar dos numeros aleatorios y decir cual es el mayor de ambos
	SOLO usar math
*/
import java.lang.Math;


public class Ejercicio_2
{
    public static void main (String [] args){
        double a;
        double b;
        
        //system.out.println(Math.max(Math.random(),Math.random()));
        
        a = Math.random();
        b = Math.random();
        
        System.out.println("El primer numero Random: "+ a);
        System.out.println("El segundo numero Random: " + b);
        
        System.out.println("Obviamente el más grande es " + Math.max(a,b));
        
            
    }
}
