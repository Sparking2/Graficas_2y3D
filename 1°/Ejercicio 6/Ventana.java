/*

	Ejercicio 6
	Crear una ventana que se pueda cerrar

*/

	import javax.swing.JFrame;
	import javax.swing.WindowConstants;

	public class Ventana
	{
		public static void main(String args[])
		{
			JFrame miFrame = new JFrame("Mi primer ventan en Java");
			miFrame.setSize(400,300);
			miFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			miFrame.setVisible(true);

		}
	}