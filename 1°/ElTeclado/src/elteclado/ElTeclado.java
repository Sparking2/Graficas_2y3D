/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elteclado;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author INFORMATICA
 */
public class ElTeclado extends JFrame implements KeyListener {
        JLabel elLabel;
        public ElTeclado()
        {
         JFrame cosa = new JFrame();
        cosa.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        cosa.setSize(400,300);
        elLabel = new JLabel("Hola soy el señor teclado");
        cosa.add(elLabel);
        cosa.addKeyListener(this);    
        cosa.show();  
        }
        
        
    public static void main(String[] args) {
        new ElTeclado();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //elLabel.setText("HEY no toques mi  " + e.getKeyChar());
        System.out.println("HEY no toques mi " + e.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent e) {
       elLabel.setText("SUELTA MI " + e.getKeyChar());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        elLabel.setText("Gracias por dejar mi " + e.getKeyChar());
    }
    
}
