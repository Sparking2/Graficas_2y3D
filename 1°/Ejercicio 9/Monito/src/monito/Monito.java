/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monito;
import java.awt.*;
import javax.swing.*;
/**
 *
 * @author spark_000
 */
public class Monito extends JFrame{

    public Monito()
        {
        super("Monito");
        setSize(200,300);
        show();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        }
    public void paint(Graphics g)
    {
        super.paint(g);
        g.drawString("Un monito!!", 10, 50);
        
        //cara
        g.drawArc(50,60,50,50,0,360);
        g.drawArc(60,70,30,30,180,180);
        g.fillOval(65,75,5,5);
        g.fillOval(80,75,5,5);
        //Cuerpo
        g.drawLine(75,110,75,200);
        //Brazos
        g.drawLine(75,120,45,160);
        g.drawLine(75,120,105,160);
        //Piernas
        g.drawLine(75,200,45,200);
        g.drawLine(75,200,105,200);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      new Monito();
    }
    
}
