/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visorimagenes;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.*;

/**
 *
 * @author spark_000
 */
    

public class VisorImagenes extends JFrame {
    
    public VisorImagenes(){
        super("Visor de Imagenes de Windows 3.1416");
        iniciarComponentes();
    }
    
    private void iniciarComponentes(){
        
        //Diseño lo de adentro
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(800,600);
        
        //Diseño del contenido
        JPanel panel = new JPanel();
        add(panel);
        JButton jb_Abrir = new JButton("Abrir");
        panel.add(jb_Abrir);
        JButton jb_Limpiar = new JButton("Limpiar");
        panel.add(jb_Limpiar);
        JScrollPane js_Scroll = new JScrollPane();
        js_Scroll.setPreferredSize(new Dimension(625,550));
        panel.add(js_Scroll,BorderLayout.SOUTH);
        JLabel jlab = new JLabel();
        
        //Funcionalidad
        
        //Boton Abrir
        jb_Abrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionListener evt) {
                
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                    JFileChooser jfc = new JFileChooser();
                    
                    if(jfc.showOpenDialog(jb_Abrir) == JFileChooser.APPROVE_OPTION)
                    {
                        java.io.File f = jfc.getSelectedFile();
                        
                        jlab.setIcon(new ImageIcon(f.toString()));
                        
                        jlab.setHorizontalAlignment(JLabel.CENTER);
                        
                        js_Scroll.getViewport().add(jlab);
                    }
            }
        });
        
        //Boton Limpiar
        jb_Limpiar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jlab.setIcon(null);
            }
        });
        show();
    }
    
    //Funcionalidad de Abrir
     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new VisorImagenes();
        
    }
    
}
