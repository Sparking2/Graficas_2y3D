/*
	Ejercicio 3
	Haga un programa que reciba como parametro una lista de números.
	El programa deberá ordenarlos de manera descendente e imprimir
	la lista ordenada

*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;

public class Lista
{
	public static void main (String [] args)
	{
		ArrayList <Integer> listita = new ArrayList <Integer>();
		for(int i = 0; i<args.length; i++)
		{
			listita.add(Integer.parseInt(args[i]));
		}

		Collections.sort(listita);
		
		for(int i = 0; i < listita.size(); i++)
			{
				System.out.println(listita.get(i));
				
			}

			
	}
}
	
