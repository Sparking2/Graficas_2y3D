/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elmouse;

import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author INFORMATICA
 */

public class ElMouse extends JFrame implements MouseListener, MouseMotionListener {
JLabel elLabel;
    public ElMouse()
    {
        JFrame cosa = new JFrame();
        cosa.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        cosa.setSize(400,300);
        elLabel = new JLabel("Hola sere tu guia");
        cosa.add(elLabel);
        cosa.addMouseListener(this);    
        cosa.show();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new ElMouse();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        elLabel.setText("ME PICASTE!!!");
    }

    @Override
    public void mousePressed(MouseEvent e) {
        elLabel.setText("SUELTAME!!!");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        elLabel.setText("Gracias por Soltarme!!!");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        elLabel.setText("Sal de mi casa!!!");
    }

    @Override
    public void mouseExited(MouseEvent e) {
       elLabel.setText("Gracias si me lo pidieras serias más amable!!!");
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        elLabel.setText("Me estoy mareano, sueltame!!!");
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        elLabel.setText("Hey tranqui!!!");
    }

     
}
