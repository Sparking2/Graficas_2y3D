import java.util.ArrayList;

/**
 * Created by xXGNWXxNero on 5/24/2016.
 */
public class Transformaciones {

    public ArrayList<Point3D> traslacion(float Tx, float Ty, float Tz,ArrayList<Point3D> puntos){
        for(int x=0;x<puntos.size();x++){
            puntos.get(x).x+=Tx;
            puntos.get(x).y+=Ty;
            puntos.get(x).z+=Tz;
        }
        return puntos;
    }

    public ArrayList<Point3D> escala(int Ex, int Ey, int Ez,ArrayList<Point3D> puntos){
        float x=Ex*.02f,y=Ey*.02f, z=Ez*.02f;
        float distX=x*puntos.get(3).x,distY=y*puntos.get(3).y,distZ=z*puntos.get(3).z;//420,280,-20--380,280,-20////428.4,280,-20--387.6,280,-20

        for(int m=0;m<puntos.size();m++){
            puntos.get(m).x=x*puntos.get(m).x+puntos.get(m).x;
            puntos.get(m).y=y*puntos.get(m).y+puntos.get(m).y;
            puntos.get(m).z=z*puntos.get(m).z+puntos.get(m).z;
        }
        traslacion(-distX,-distY,-distZ,puntos);
        return puntos;
    }

    public ArrayList<Point3D> rotacion(float Rx, float Ry, float Rz,ArrayList<Point3D> puntos){
        Point3D centro=distanciaOrigenCentro(puntos);//regresa la posicion del centro de mi figura en el mapa
        traslacion(-centro.x,-centro.y,-centro.z,puntos);//movemos al origen
        if(Rx!=0){
            Rx=(float)Math.toRadians(Rx);
            for(int x=0;x<puntos.size();x++){
                puntos.get(x).y =(float) (puntos.get(x).y * Math.cos(Rx) - puntos.get(x).z * Math.sin(Rx));
                puntos.get(x).z =(float) (puntos.get(x).y * Math.sin(Rx) + puntos.get(x).z * Math.cos(Rx));
            }
        }
        if(Ry!=0){
            Ry=(float)Math.toRadians(Ry);
            for(int x=0;x<puntos.size();x++){
                puntos.get(x).x = (float) (puntos.get(x).x * Math.cos(Ry) + puntos.get(x).z * Math.sin(Ry));
                puntos.get(x).z = (float) ( - puntos.get(x).x *  Math.sin(Ry) + puntos.get(x).z * Math.cos(Ry));
            }
        }
        if(Rz!=0){
            Rz=(float)Math.toRadians(Rz);
            for(int x=0;x<puntos.size();x++){
                puntos.get(x).x =(float) (puntos.get(x).x * Math.cos(Rz) - puntos.get(x).y * Math.sin(Rz));
                puntos.get(x).y =(float) (puntos.get(x).x * Math.sin(Rz) + puntos.get(x).y * Math.cos(Rz));
            }
        }
        System.out.println(centro.x );
        System.out.println(centro.y );
        System.out.println(centro.z );

        traslacion(centro.x,centro.y,centro.z,puntos);//movemos al origen
        return puntos;
    }

    private Point3D distanciaOrigenCentro(ArrayList<Point3D> puntos){
        return  new Point3D((puntos.get(0).x+puntos.get(3).x)/2f, (puntos.get(4).y+puntos.get(0).y)/2f, (puntos.get(1).z+puntos.get(0).z)/2f);//obtenemos la posicion del centro en el mapa
    }
}
