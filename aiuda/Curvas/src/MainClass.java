import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

/**
 * Created by xXGNWXxNero on 5/27/2016.
 */
public class MainClass extends JFrame implements KeyListener{
    BufferedImage buffer;
    int TRX=0,TRY=0,TRZ=0, ETRA=0;
    Boolean estilo;
    Curva curva;
    Transformaciones trans;
    public static void main(String[] args) {
        new MainClass();
    }

    public MainClass() {
        super("Transformaciones");
        this.setSize(800, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(false);
        this.addKeyListener(this);
        estilo=true;
        trans=new Transformaciones();
        curva=new Curva(1,1,-1, 400);

        buffer = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
        for(int x=0;x<10;x++){
            buffer.getGraphics().clearRect(0,0,800,600);
            curva.DibujarCurva(10,estilo, buffer);
            this.getGraphics().drawImage(buffer,0,0,null);
        }
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyChar()=='|'){
            ETRA=ETRA==0?1:ETRA==1?2:0;
        }

        switch (e.getKeyChar()){
            case 'd':
                TRX+=1;
                break;
            case 'a':
                TRX-=1;
                break;
            case 's':
                TRY+=1;
                break;
            case 'w':
                TRY-=1;
                break;
            case'q':
                TRZ+=1;
                break;
            case 'e':
                TRZ-=1;
                break;
            case ' ':
                estilo=!estilo;
                break;
        }
        switch (ETRA){
            case 0:
                curva.setPuntos3D(trans.traslacion(TRX*5,TRY*5,TRZ*5,curva.getPuntos3D()));
                break;
            case 1:
                curva.setPuntos3D(trans.escala(TRX,TRY,TRZ,curva.getPuntos3D()));
                break;
            case 2:
                curva.setPuntos3D(trans.rotacion(TRX*1,TRY*1,TRZ*1 ,curva.getPuntos3D()));
                break;
        }

        buffer.getGraphics().clearRect(0,0,800,600);
        curva.DibujarCurva(100,estilo,buffer);
        this.getGraphics().drawImage(buffer,0,0,this);
        TRX=0;
        TRY=0;
        TRZ=0;
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
