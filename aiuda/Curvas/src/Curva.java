import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by xXGNWXxNero on 5/27/2016.
 */
public class Curva {
    int px=0, py=0,pz=0,dx=0,dy=0;
    float m=0,b=0;
    ArrayList<Point3D> puntos3D;
    Graphics gra;

    public  Curva(int x, int y, int z, int puntos){
        setPlano(x,y,z);
        puntos3D=new ArrayList<Point3D>();
        puntos3D=obtenerPuntos3D((float)puntos);
    }

    public  void DibujarCurva(int puntos, Boolean estilo, BufferedImage b){
        gra=b.getGraphics();

        ArrayList<Point> puntos2D=puntos3D2D("paralela",puntos3D);
        enlazarPuntos(puntos2D,estilo);
    }

    private void enlazarPuntos(ArrayList<Point>puntos2D, Boolean estilo){
        Point puntoFinal=puntos2D.get(puntos2D.size()-1);
        if(estilo){
            for(int x=1;!puntos2D.get(x).equals(puntoFinal);x++){
                lineaDDA(puntos2D.get(x-1).x,puntos2D.get(x-1).y,puntos2D.get(x).x,puntos2D.get(x).y, new Color(229, 255, 234));
            }
        }
        else{
            for(int x=1;!puntos2D.get(x).equals(puntoFinal);x++){
                lineaDDA(puntos2D.get(x).x,puntos2D.get(x).y,puntos2D.get(x).x,puntos2D.get(x).y, new Color(255,200,200));//sin enlazar
            }
        }

    }

    public ArrayList<Point3D> getPuntos3D(){
        return puntos3D;
    }

    private ArrayList<Point3D> obtenerPuntos3D(Float cantPuntos){//las funciones para sacar x,y,z
        puntos3D=new ArrayList<Point3D>();
        float pi=3.14159f;
        for (float x=-pi;x<pi;x+=pi/cantPuntos){
            puntos3D.add(new Point3D((float)(x - 70 * (Math.cos(x))),(float)(x - 70 * (2 * (Math.cos(x) * Math.cos(x)))),(float)Math.sin(2 * x)));
            //puntos3D.add(new Point3D(float)(x - 70 * (5 * Math.cos(x)),(float)(x - 70 * (Math.sin(x))  ),(float)x/5));
            /*puntos3D.add(
                    new Point3D(
                    (float) (x-70 *( Math.cos(x)+ 1/2f*Math.cos(7*x) + 1/3f * Math.sin(17*x))+400),
                    (float) (x-70 *( Math.sin(x)+ 1/2f*Math.sin(7*x) + 1/2f *Math.cos(17*x))+300),
                    1 //(float) (10*Math.cos(x)))
            ));*/
            //figura rara
            //puntos3D.add(new Point3D((float) (x-10*Math.sin(x))+100, (float) (4-10*Math.cos(x)) +400, 0));//rosa
            //puntos3D.add(new Point3D((float) (50*x), (float) (50*Math.sin(x)) +400, (float)( 50*Math.cos(x))));//curva simple
            //puntos.add(new Point3D((float) (x-30*Math.sin(x))+100, (float) (10-30*Math.cos(x)) +400, 0));
        }
        return puntos3D;
    }
    public void setPuntos3D(ArrayList<Point3D> puntos3D){
        this.puntos3D=puntos3D;
    }








    private ArrayList<Point> puntos3D2D(String proyeccion, ArrayList<Point3D> puntos3D){
        ArrayList<Point> puntos2D=new ArrayList<Point>();
        switch (proyeccion){
            case "paralela":
                for(int x=0;x<puntos3D.size();x++)
                    puntos2D.add(paralela(puntos3D.get(x)));
                break;
            case "perspectiva":
                for(int x=0;x<puntos3D.size();x++)
                    puntos2D.add(perspectiva(puntos3D.get(x)));
                break;
        }
        return puntos2D;
    }

    private void lineaDDA(int x0, int y0, int x1, int y1, Color c){
        dx = x1 - x0;
        dy = y1 - y0;//new Color(1,1,1);
        //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
        putPixel(x0,y0,c);

        if (Math.abs(dx) > Math.abs(dy)) {
            m = (float) dy / (float) dx;
            b = y0 - m*x0;
            if(dx<0)
                dx =  -1;
            else
                dx =  1;
            while (x0 != x1) {
                x0 += dx;
                y0 = Math.round(m*x0 + b);
                //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
                putPixel(x0,y0,c);
            }
        }
        else if (dy != 0) {
            m= (float) dx / (float) dy;
            b = x0 - m*y0;
            if(dy<0)
                dy =  -1;
            else
                dy =  1;
            while (y0 != y1) {
                y0 += dy;
                x0 = Math.round(m*y0 + b);
                //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
                putPixel(x0,y0,c);
            }
        }
    }

    private void putPixel(int x, int y, Color c){
        gra.setColor(c);
        gra.drawLine(x, y, x, y);

    }

    private Point perspectiva(Point3D punto){
        double u=-pz/(punto.z-pz);
        double x=px+(punto.x-px)*u;
        double y=py+(punto.y-py)*u;
        return  new Point((int)x,(int)y);
    }
    private Point paralela(Point3D punto){
        double x=punto.x-(px*punto.z)/pz;
        double y=punto.y-(py*punto.z)/pz;
        return new Point((int)x,(int)y);
    }

    public void setPlano(int px, int py, int pz){
        this.px=px;
        this.py=py;
        this.pz=pz;
    }
}
