import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;

public class graficas {
    int dx=0,dy=0;
    float m=0,b=0;
    Graphics gra;
    int col=Color.BLACK.getRGB();
    public void setLienzo(BufferedImage b){
        gra=b.getGraphics();
    }

    public void lineaDDA(int x0, int y0, int x1, int y1, Color c){
        dx = x1 - x0;
        dy = y1 - y0;//new Color(1,1,1);
        //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
        putPixel(x0,y0,c);

        if (Math.abs(dx) > Math.abs(dy)) {
            m = (float) dy / (float) dx;
            b = y0 - m*x0;
            if(dx<0)
                dx =  -1;
            else
                dx =  1;
            while (x0 != x1) {
                x0 += dx;
                y0 = Math.round(m*x0 + b);
                //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
                putPixel(x0,y0,c);
            }
        }
        else if (dy != 0) {
            m= (float) dx / (float) dy;
            b = x0 - m*y0;
            if(dy<0)
                dy =  -1;
            else
                dy =  1;
            while (y0 != y1) {
                y0 += dy;
                x0 = Math.round(m*y0 + b);
                //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
                putPixel(x0,y0,c);
            }
        }
    }

    public void putPixel(int x, int y, Color c){
        gra.setColor(c);
        gra.drawLine(x, y, x, y);
    }






    /*public void inundacion(int x, int y, BufferedImage g,Color c){

        try{
            if(g.getRGB(x, y-1)==col){
                putPixel(x,y-1,c);
                inundacion(x,y-1,g,c);
            }
            if(g.getRGB(x+1, y)==col){
                putPixel(x+1,y,c);
                    inundacion(x+1,y,g,c);


            }
            if(g.getRGB(x, y+1)==col){
                putPixel(x,y+1,c);
                inundacion(x,y+1,g,c);
            }

            if(g.getRGB(x-1, y)==col){

                    putPixel(x-1,y,c);
                    inundacion(x-1,y,g,c);

            }

        }
        catch(StackOverflowError e){
            inundacion(x,y,g,c);
        }

    }*/

    //color de referencia es el color limite

    public void inundacion(int x, int y, Color ref, BufferedImage b, Color c) {
        Queue<Point> ListPuntos = new LinkedList<Point>();
        Point p;
        if (b.getRGB(x, y) != ref.getRGB()) {
            ListPuntos.add(new Point(x, y));

            while (ListPuntos.isEmpty() == false) {
                p = ListPuntos.remove();
                if ((b.getRGB(p.x, p.y) != ref.getRGB()) && (b.getRGB(p.x, p.y) != c.getRGB())) {
                    putPixel(p.x, p.y, c);
                    ListPuntos.add(new Point(p.x, p.y-1));
                    ListPuntos.add(new Point(p.x-1, p.y));
                    ListPuntos.add(new Point(p.x+1, p.y));
                    ListPuntos.add(new Point(p.x, p.y+1));
                }
            }
        }
    }


}
