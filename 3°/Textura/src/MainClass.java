import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

public class MainClass extends JFrame implements KeyListener{
    BufferedImage buffer;
    int TRX=0,TRY=0,TRZ=0, ETRA=0;
    Boolean estilo;
    Plataforma plataforma;
    Transformaciones trans;
    String proyeccion;
    Color c;
    public static void main(String[] args) {
        new MainClass();
    }

    public MainClass() {
        super("Transformaciones");
        this.setSize(800, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(false);
        this.addKeyListener(this);
        c=new Color(200,200,200);
        proyeccion="paralela";
        estilo=true;
        trans=new Transformaciones();
        plataforma=new Plataforma(1,1,-100, 10,10,50, proyeccion);//plano(3 variables), lineas x, lineas y(veces que se repetira el ciclo onda), puntos por linea

        buffer = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
        for(int x=0;x<10;x++){
            buffer.getGraphics().clearRect(0,0,800,600);
            plataforma.dibujarPlataforma(estilo,  buffer, c);
            this.getGraphics().drawImage(buffer,0,0,null);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyChar()=='|'){
            ETRA=ETRA==0?1:ETRA==1?2:0;
        }

        switch (e.getKeyChar()){
            case 'd':
                TRX+=3;
                break;
            case 'a':
                TRX-=3;
                break;
            case 's':
                TRY+=3;
                break;
            case 'w':
                TRY-=3;
                break;
            case'q':
                TRZ+=3;
                break;
            case 'e':
                TRZ-=3;
                break;
            case ' ':
                estilo=!estilo;
                break;
        }
        switch (ETRA){
            case 0:
                plataforma.setMalla(trans.traslacion(TRX*5,TRY*5,TRZ*5,plataforma.getMalla()));
                break;
            case 1:
                plataforma.setMalla(trans.escala(TRX,TRY,TRZ,plataforma.getMalla()));
                break;
            case 2:
                plataforma.setMalla(trans.rotacion(TRX*1,TRY*1,TRZ*1 ,plataforma.getMalla()));
                break;
        }

        buffer.getGraphics().clearRect(0,0,800,600);
        plataforma.dibujarPlataforma(estilo, buffer, c);
        this.getGraphics().drawImage(buffer,0,0,this);
        TRX=0;
        TRY=0;
        TRZ=0;
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
