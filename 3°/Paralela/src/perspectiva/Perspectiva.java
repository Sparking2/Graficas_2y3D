/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perspectiva;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
 
public class Perspectiva extends JFrame implements KeyListener{
    
        String modelo = "cubo";
        String modo = "paralelo";
        //Los valores de mi camara
        int CameraX = 0, CameraY = 0, CameraZ = 50;
        //Frame en el que trabajo
        JFrame frame;
        //Creo mi camara
        MyCamera camara = new MyCamera(CameraX,CameraY,CameraZ);
        //"Buffer" es donde esta guardado que se hacer, en este caso dibujar
        Buffer figura;
        //Buffers de respaldo
        BufferedImage buffer,bufferRespaldo;
        //Creo un Hilo que dibujara
        private Thread hilo;
        
        
        Perspectiva(){
            //Creo el JFrame con su nombre
            frame = new JFrame("Perspectiva Paralela");
            // El buffer que tamaño tendra
            buffer = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
            //El buffer de respalo
            bufferRespaldo = new BufferedImage(800,600,BufferedImage.TYPE_INT_RGB);
            //tamaño del frame
            frame.setSize(800,600);    
            //No quiero que se haga más grande
            frame.setResizable(false);
            //Lo vuelvo visible
            frame.setVisible(true);
            //Que se cierre por default
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            //El frame debe de oir mis teclas
            frame.addKeyListener(this);
            //creo mi objeto que dibujara
            figura = new Buffer();  
            //inicio el hilo
            hilo = new Thread();
            hilo.start();
            repaint();
            
        }
    
   public void repaint(){
       while(true){
        frame.getGraphics().drawImage(buffer, 0, 0, this);
        buffer.getGraphics().clearRect(0, 0, 800, 600);
        figura.dibujar(modelo, buffer.getGraphics(), camara, modo);

       }
   }
        
    void muestra(){
        System.out.println("X: " + camara.Xp() + " Y: " + camara.Yp() + " Z: " + camara.Zp());
    }
        
    public static void main(String[] args) {
        new Perspectiva();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //System.out.println(e.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyChar()){
            
            case 'q':
                System.out.println("-1 a camara en X");
                CameraX = CameraX - 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 'w':
                System.out.println("+1 a camara en X");
                CameraX = CameraX + 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 'a':
                System.out.println("-1 a camara en Y");
                CameraY = CameraY - 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 's':
                System.out.println("+1 a camara en Y");
                CameraY = CameraY + 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 'z':
                System.out.println("-1 a camara en Z");
                CameraZ = CameraZ - 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 'x':
                System.out.println("+1 a camara en Z");
                CameraZ = CameraZ + 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
        }
            
                
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void run(){
        while(true){
            repaint(); 
        }
    }
    
}
