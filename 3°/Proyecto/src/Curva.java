import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.concurrent.AbstractExecutorService;
import java.util.regex.Matcher;

/**
 * Created by xXGNWXxNero on 5/27/2016.
 */
public class Curva {
    int px=0, py=-5,pz=10;
    String proyeccion;
    ArrayList<Point3D> puntos3D;
    graficas grafics;

    public  Curva(int x, int y, int z, int puntos, int distY,int diferencial,Point3D puntoInicial, String proyeccion){
        grafics=new graficas();
        this.proyeccion=proyeccion;
        setPlano(x,y,z);
        puntos3D=new ArrayList<Point3D>();
        puntos3D=obtenerPuntos3D((float)puntos, distY, diferencial, puntoInicial);
    }
    // esta funcion define como sera la plataforma
    //diferencia en el plano inicial de una curva a otra,
    private ArrayList<Point3D> obtenerPuntos3D(Float cantPuntos, int distY, int diferencial, Point3D puntoInicial){
        puntos3D=new ArrayList<Point3D>();
        float pi=3.14159f;
        for (float x=0;x<distY*pi;x+=pi/cantPuntos){//el numero de puntos que habra por cada curva
            puntos3D.add(new Point3D(
                    (float) (( 2 + Math.cos(x)) * Math.cos(diferencial))*50 + puntoInicial.x,
                    (float) ((x+1)*60) + puntoInicial.y,
                    (float) (( 2 + Math.cos(x)) * Math.sin(diferencial))*50 + puntoInicial.z
            ));
        }
        return puntos3D;
    }


    public void enlazarPuntosMalla(ArrayList<Point3D> arrayAct, ArrayList<Point3D> arrayAnt, Boolean estilo, BufferedImage b, Color c){
        grafics.setLienzo(b);
        ArrayList<Point> puntos2DAct=puntos3D2D(arrayAct);
        ArrayList<Point> puntos2DAnt=puntos3D2D(arrayAnt);
        int tamañoCurva=puntos2DAct.size(), aumento=250/tamañoCurva;
        Color color;

        for (int x=0;x<tamañoCurva;x++){
            color=new Color(255 ,215,0);
            enlazarPuntos(puntos2DAct.get(x), puntos2DAnt.get(x),estilo, color);
            if(x+1<tamañoCurva){
                enlazarPuntos(puntos2DAct.get(x), puntos2DAct.get(x+1), estilo, color);
                enlazarPuntos(puntos2DAct.get(x), puntos2DAnt.get(x+1),estilo , color);
            }
        }
    }

    public void dibujarPuntosMalla(ArrayList<Point3D> arrayAct, ArrayList<Point3D> arrayAnt, BufferedImage b, Color c){
        grafics.setLienzo(b);
        ArrayList<Point> puntos2DAct=puntos3D2D(arrayAct);
        ArrayList<Point> puntos2DAnt=puntos3D2D(arrayAnt);
        int tamañoCurva=puntos2DAct.size();
        for (int x=0;x<tamañoCurva;x++){
            if(x+1<puntos2DAct.size()&& x-1>0){
                if((Math.abs(puntos2DAct.get(x).x) != Math.abs(puntos2DAnt.get(x+1).x)) && (Math.abs(puntos2DAct.get(x).y)!=Math.abs(puntos2DAnt.get(x+1).y))){
                    grafics.inundacion((puntos2DAct.get(x).x+puntos2DAnt.get(x+1).x)/2,(puntos2DAct.get(x).y+puntos2DAnt.get(x+1).y)/2, c,b,Color.BLUE);
                }
            }

        }
    }

    private void enlazarPuntos(Point a, Point b, Boolean estilo, Color c){
        if(estilo){
            grafics.lineaDDA(a.x, a.y, b.x, b.y, c);
        }
        else{
            grafics.putPixel(a.x,a.y, c); //sin enlazar
            grafics.putPixel(b.x,b.y, c);
        }
    }

    private ArrayList<Point> puntos3D2D(ArrayList<Point3D> puntos3D){
        ArrayList<Point> puntos2D=new ArrayList<Point>();
        switch (this.proyeccion){
            case "paralela":
                for(int x=0;x<puntos3D.size();x++)
                    puntos2D.add(paralela(puntos3D.get(x)));
                break;
            case "perspectiva":
                for(int x=0;x<puntos3D.size();x++)
                    puntos2D.add(perspectiva(puntos3D.get(x)));
                break;
        }
        return puntos2D;
    }

    public ArrayList<Point3D> getPuntos3D(){
        return puntos3D;
    }

    public void setPuntos3D(ArrayList<Point3D> puntos3D){
        this.puntos3D=puntos3D;
    }

    private Point perspectiva(Point3D punto){
        double u=-pz/(punto.z-pz);
        double x=px+(punto.x-px)*u;
        double y=py+(punto.y-py)*u;
        return  new Point((int)x,(int)y);
    }

    private Point paralela(Point3D punto){
        double x=punto.x-(px*punto.z)/pz;
        double y=punto.y-(py*punto.z)/pz;
        return new Point((int)x,(int)y);
    }

    public void setPlano(int px, int py, int pz){
        this.px=px;
        this.py=py;
        this.pz=pz;
    }
}
