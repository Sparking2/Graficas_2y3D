
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import javax.swing.JFrame;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javazoom.jl.player.Player;
import javazoom.jl.decoder.JavaLayerException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author spark
 */


public class MainClass extends JFrame implements KeyListener,Runnable{
    
    int [][]Z_Buffer;
    Boolean estilo;
    Plataforma plataforma;
    Point3D origen;
    String proyeccion;
    BufferedImage buffer;
    Cubo cubo;
    Transformaciones trans;
    int TRX=0,TRY=0,TRZ=0, ETRA=2;
    JFrame theframe;
    String estadoTransformacion="012";//0=translacion, 1= escala, 2= rotacion
    private Thread hilo;
    TransformacionesCilindro transCil;
    Color c;
    
    public static void main(String[] args) {
        new MainClass();
        try{
           FileInputStream fis;
           Player player;
           fis = new FileInputStream("E:\\Users\\spark\\Documents\\GitHub\\Graficas_2y3D\\3°\\Proyecto\\Epic.mp3");
           BufferedInputStream bis = new BufferedInputStream(fis);
           
           player = new Player(bis);
           player.play();
        }catch(JavaLayerException e){
            e.printStackTrace();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
        
    }
    
    public MainClass() {
        theframe = new JFrame();
        theframe.setTitle("Proyecto");
        proyeccion="paralela";
        estilo = true;
        theframe.setSize(1024,768);
        theframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        theframe.setVisible(true);
        theframe.setResizable(false);
        theframe.addKeyListener(this);
        trans = new Transformaciones();
        transCil = new TransformacionesCilindro();
        buffer = new BufferedImage(1024, 768, BufferedImage.TYPE_INT_RGB);
        c = new Color(200,200,200);
        cubo = new Cubo(400, 300, 0, 30,30,30);
        cubo.setPlano(1,1,1);
        cubo.dibujarCubo(buffer,"paralela", Color.RED);
        origen = new Point3D(450,400,0);
        plataforma=new Plataforma(1,1,-100, 80, 2, 30, origen, proyeccion);
         for(int x=0;x<50;x++)
            theframe.getGraphics().drawImage(buffer,0,0, theframe);
        hilo = new Thread(this);
        hilo.start();
    }
    
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyChar()=='|'){
            ETRA=ETRA==0?1:ETRA==1?2:0;
        }

        switch (e.getKeyChar()){
            case 'd':
                TRX+=1;
                break;
            case 'a':
                TRX-=1;
                break;
            case 's':
                TRY+=1;
                break;
            case 'w':
                TRY-=1;
                break;
            case'q':
                TRZ+=1;
                break;
            case 'e':
                TRZ-=1;
                break;
        }
        
        switch (ETRA){
            case 0:
                cubo.setPuntos3D(trans.traslacion(TRX*5,TRY*5,TRZ*5,cubo.getPuntos3D()));
                break;
            case 1:
                cubo.setPuntos3D(trans.escala(TRX,TRY,TRZ,cubo.getPuntos3D()));
                break;
            case 2:
                cubo.setPuntos3D(trans.rotacion(TRX*1,TRY*1,TRZ*1 ,cubo.getPuntos3D()));
                break;
        }
        
        /*buffer.getGraphics().clearRect(0,0,800,600);
        cubo.dibujarCubo(buffer,"paralela", Color.BLUE);
        theframe.getGraphics().drawImage(buffer,0,0, theframe);*/
        TRX=0;
        TRY=0;
        TRZ=0;
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public void CuboMove(){
                
                TRY+=1;
                cubo.setPuntos3D(trans.rotacion(TRX*1,TRY*1,TRZ*1 ,cubo.getPuntos3D()));
                buffer.getGraphics().clearRect(0,0,1024,768);
                cubo.dibujarCubo(buffer,"paralela", Color.RED);
                //theframe.getGraphics().drawImage(buffer,0,0, theframe);
                TRY = 0;
    }
    public void Cilindro(){
        
    }
    
    @Override
    public void run() {
           while(true){
               try{
                Thread.sleep(10);
                CuboMove();
                plataforma.dibujarPlataforma(estilo, false,  buffer, c);
                theframe.getGraphics().drawImage(buffer, 0, 0, theframe);
               }catch (Exception e){
                   
               }
               
           }
    }
}

