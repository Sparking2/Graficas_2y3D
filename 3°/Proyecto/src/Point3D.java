/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author spark
 */
public class Point3D {
    public float x;
    public float y;
    public float z;
    
    public Point3D(float x, float y, float z){
        this.x=x;
        this.y=y;
        this.z=z;
    }
}
