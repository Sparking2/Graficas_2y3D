/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perspectiva;
//Hola

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
 
public class Perspectiva extends Thread implements KeyListener{
    
        String modelo = "cubo";
        String modo = "paralelo";
        String Transform = "";
        
        //Los valores de mi camara
        int CameraX = 25, CameraY = 24, CameraZ = 50;
        //Frame en el que trabajo
        JFrame frame;
        //Creo mi camara
        MyCamera camara = new MyCamera(CameraX,CameraY,CameraZ);
        //"Buffer" es donde esta guardado que se hacer, en este caso dibujar
        Buffer figura;
        //Buffers de respaldo
        BufferedImage buffer,bufferRespaldo;
        //Creo un Hilo que dibujara
        //private final Thread hilo;
        
        
        Perspectiva(){
            //Creo el JFrame con su nombre
            frame = new JFrame("Perspectiva Paralela");
            // El buffer que tamaño tendra
            buffer = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
            //El buffer de respalo
            bufferRespaldo = new BufferedImage(800,600,BufferedImage.TYPE_INT_RGB);
            //tamaño del frame
            frame.setSize(800,600);    
            //No quiero que se haga más grande
            //frame.setResizable(false);
            //Lo vuelvo visible
            frame.setVisible(true);
            //Que se cierre por default
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            //El frame debe de oir mis teclas
            frame.addKeyListener(this);
            //creo mi objeto que dibujara
            figura = new Buffer();
           
            repaint();
            
        }
    
   public void repaint(){
       int x = 0, y = 0;
       
       while(true){
        frame.getGraphics().drawImage(buffer, 0, 0, frame);
        buffer.getGraphics().clearRect(0, 0, 800, 600);
        figura.dibujar(modelo, buffer.getGraphics(), camara, modo);
        //figura.Rotacion(1, 'z');
        //figura.Rotacion(1,'z');
        

       }
   }
        
    void muestra(){
        System.out.println("X: " + camara.Xp() + " Y: " + camara.Yp() + " Z: " + camara.Zp());
    }
        
    public static void main(String[] args) {
        new Perspectiva();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //System.out.println(e.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        switch(e.getKeyChar()){
            
            case 'q':
                System.out.println("-1 a camara en X");
                CameraX = CameraX - 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 'w':
                System.out.println("+1 a camara en X");
                CameraX = CameraX + 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 'a':
                System.out.println("-1 a camara en Y");
                CameraY = CameraY - 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 's':
                System.out.println("+1 a camara en Y");
                CameraY = CameraY + 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 'z':
                System.out.println("-1 a camara en Z");
                CameraZ = CameraZ - 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 'x':
                System.out.println("+1 a camara en Z");
                CameraZ = CameraZ + 1;
                camara.UpdateCamera(CameraX,CameraY,CameraZ);
                muestra(); 
            break;
            
            case 'm':
                if(modo == "perspectiva"){
                    modo = "paralelo";
                }else if(modo == "paralelo"){
                    modo = "perspectiva";
                }
                    
                System.out.println("Modo cambiado a " + modo);
            break;
            
            case 't':
                Transform = "Traslacion";
                System.out.println("Transform Mode actual " + Transform);
                break;
            case 'e':
                Transform = "Escalamiento";
                System.out.println("Transform Mode actual " + Transform);
                break;
            case 'r':
                Transform = "Rotacion";
                System.out.println("Transform Mode actual " + Transform);
                break;
        }
        
        if(Transform == "Traslacion"){
            switch(e.getKeyCode()){
                case 97:
                    // mover en x <-
                    figura.Traslacion(1 , 'x');
                    break;
                case 98:
                    figura.Traslacion(-1 , 'x');
                    break;
                case 100:
                    figura.Traslacion(1 , 'y');
                    break;
                case 101:
                    figura.Traslacion(-1 , 'y');
                    break;
                case 103:
                    figura.Traslacion(1 , 'z');
                    break;
                case 104:
                    figura.Traslacion(-1 , 'z');
                    break;
            }
        }
        
       if(Transform == "Escalamiento"){
           switch(e.getKeyCode()){
                case 97:
                    // mover en x <-
                    figura.Escalamiento(1 , 'x');
                    break;
                case 98:
                    figura.Escalamiento(-1 , 'x');
                    break;
                case 100:
                    figura.Escalamiento(1 , 'y');
                    break;
                case 101:
                    figura.Escalamiento(-1 , 'y');
                    break;
                case 103:
                    figura.Escalamiento(1 , 'z');
                    break;
                case 104:
                    figura.Escalamiento(-1 , 'z');
                    break;
            }
       }
            
       if(Transform == "Rotacion"){
           switch(e.getKeyCode()){
                case 97:
                    // mover en x <-
                    figura.Rotacion(1 , 'x');
                    break;
                case 98:
                    figura.Rotacion(-1 , 'x');
                    break;
                case 100:
                    figura.Rotacion(1 , 'y');
                    break;
                case 101:
                    figura.Rotacion(-1 , 'y');
                    break;
                case 103:
                    figura.Rotacion(1 , 'z');
                    break;
                case 104:
                    figura.Rotacion(-1 , 'z');
                    break;
            }
       }         
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void run(){
        while(true){
            repaint();
            figura.Rotacion(50, 'z');
        }
    }
    
}
