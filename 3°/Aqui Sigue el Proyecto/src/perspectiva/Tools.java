
package perspectiva;

import javafx.geometry.Point2D;

public class Tools {
    public Tools(){
        
    }

    public point2d paralela(punto3d p , MyCamera camara){

        float x = p.x - (p.z * camara.Xp() / camara.Zp() );
        float y = p.y - (p.z * camara.Yp() / camara.Zp() );
        return new point2d((int)x, (int)y);
    }
    
    public point2d perspectiva(punto3d p , MyCamera camara){
        float u = (float)camara.Zp() / (p.z + camara.Zp());
        int x = Math.round( (float)(p.x - camara.Xp() * u + camara.Xp() ) );
        int y = Math.round( (float)(p.y - camara.Yp() * u + camara.Yp() ) );;
        return new point2d(x,y);
    }
}
