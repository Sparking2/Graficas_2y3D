
package perspectiva;

import javafx.geometry.Point2D;

public class Tools {
    public Tools(){
        
    }

    public point2d paralela(punto3d p , MyCamera camara){

        int x = p.x - (p.z * (int)camara.Xp() / (int)camara.Zp() );
        int y = p.y - (p.z * (int)camara.Yp() / (int)camara.Zp() );
        return new point2d(x, y);
    }
    
    public point2d perspectiva(punto3d p , MyCamera camara){
        float u = (float)camara.Zp() / (p.z + camara.Zp());
        int x = Math.round( (float)(p.x - camara.Xp() * u + camara.Xp() ) );
        int y = Math.round( (float)(p.y - camara.Yp() * u + camara.Yp() ) );;
        return new point2d(x,y);
    }
}
