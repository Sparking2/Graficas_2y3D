
package perspectiva;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javafx.geometry.Point2D;

public class Buffer {
    Graphics gra;
    BufferedImage buffer;
    
    public Buffer(){
        buffer = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
    }
    
    public void PutPixel(int x, int y, Color c){
           buffer.setRGB(0, 0, c.getRGB());
           gra.drawImage(buffer,x,y,null);
    }
    
    public void DibujarBresenham (int x0,int y0, int x1, int y1,Graphics g, Color col){
        gra = g;
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1)){
            int p = 2 * rodzil;
            if (p > -dy){
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            
            if (p < dx){
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
            }
        PutPixel(x0, y0, col);  
        }
    }
    
    public void DibujarBresenham (point2d p1,point2d p2,Graphics g, Color col){
        gra = g;
        
        int x0 = p1.px();
        int x1 = p2.px();
        int y0 = p1.py();
        int y1 = p2.py();
        
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1)){
            int p = 2 * rodzil;
            if (p > -dy){
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            
            if (p < dx){
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
            }
        PutPixel(x0, y0, col);  
        }
    }
    
    public void CirculoBresenham(int Xc, int Yc, int Rd, Graphics g, Color cosa, int grosor){
        gra = g;
        PutPixel(Xc, Yc + Rd, cosa);
        double p = 3 - 2* Rd;
        for (int xk = 0,yk = Rd;xk <= yk; xk+=1){
            if(p < 0){
                ocholados(Xc,Yc,xk,yk,g,cosa,grosor);
                p += 2*xk + 3;                       
            }else{
                yk -= 1;
                ocholados(Xc,Yc,xk,yk,g,cosa,grosor);
                 p += 2 * ( xk -yk) + 5;
                }
        } 
    } 

    public void ocholados(int Xc, int Yc, int xk, int yk,Graphics g, Color cosa, int grosor){
          gra = g;
          for(int i = 0;i <grosor; i++){
             PutPixel((int)Xc + i + xk,(int)Yc + i + yk,cosa); //1 
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i + xk,(int)Yc + i + yk,cosa); //1
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i - xk,(int)Yc + i + yk,cosa); // 8
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i + xk,(int)Yc + i - yk,cosa); // 4
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i - xk,(int)Yc + i - yk,cosa); // 5
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i + yk,(int)Yc + i + xk,cosa); // 2
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i - yk,(int)Yc + i + xk,cosa); // 7
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i + yk,(int)Yc + i - xk,cosa); // 3
          }
          for(int i = 0;i <grosor; i++){
          PutPixel((int)Xc + i - yk,(int)Yc + i - xk,cosa); // 6
          }
    }
    
    public void DibujarBresenham (int x0,int y0, int x1, int y1,int grosor,Color c,Graphics g){
        gra = g;
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1))
        {
            int p = 2 * rodzil;
            if (p > -dy)
            {
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            if (p < dx) {
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
        }
        PutPixel(x0, y0, c);
        for(int i = 0;i < grosor; i++)
                {
                PutPixel(x0 + i, y0 + i, c);
                }
            
        }
        
    }

    public int GetPixel(){
        gra.getColor();
        int color = 0;
        
        return color;
    }
    
    public void dibujar(String figura, Graphics g, MyCamera camara, String mode){
        if(mode == "paralelo"){
        switch(figura){
            case "cubo":
                //creo mi caja de herramientas
                Tools tlbx = new Tools();
                
                //creo los puntos que usare para dibujar
                point2d p2d1 = new point2d(0,0);
                point2d p2d2 = new point2d(0,0);
                
                //Mis puntos 3D del cubo
                punto3d p0 = new punto3d(250,250,5);
                punto3d p1 = new punto3d(300,250,5);
                punto3d p2 = new punto3d(250,300,5);
                punto3d p3 = new punto3d(300,300,5);
                punto3d p4 = new punto3d(250,250,50);
                punto3d p5 = new punto3d(300,250,50);
                punto3d p6 = new punto3d(250,300,50);
                punto3d p7 = new punto3d(300,300,50);
                
                //Obtengo Obtengo y dibujo puntos
                p2d1 = tlbx.paralela(p0, camara);
                p2d2 = tlbx.paralela(p1, camara);
                DibujarBresenham(p2d1, p2d2, g, Color.red);
                
                p2d2 = tlbx.paralela(p2, camara);
                DibujarBresenham(p2d1, p2d2, g, Color.red);
                
                p2d2 = tlbx.paralela(p4, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d1 = tlbx.paralela(p1, camara);
                p2d2 = tlbx.paralela(p5, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d2 = tlbx.paralela(p3,camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                //p2d1 = tlbx.paralela(p3, camara);
                p2d1 = tlbx.paralela(p2, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d1 = tlbx.paralela(p6, camara);
                p2d2 = tlbx.paralela(p2,camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d2 = tlbx.paralela(p4, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d2 = tlbx.paralela(p7, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d1 = tlbx.paralela(p7, camara);
                p2d2 = tlbx.paralela(p3, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d2 = tlbx.paralela(p5, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d1 = p2d2;
                p2d2 = tlbx.paralela(p4, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                break;
            default:
                System.out.println("te la pelas");
            }//Fin del Switch
        }//fin del if
        
        if(mode == "perspectiva"){
            switch(figura){
            case "cubo":
                //creo mi caja de herramientas
                Tools tlbx = new Tools();
                
                //creo los puntos que usare para dibujar
                point2d p2d1 = new point2d(0,0);
                point2d p2d2 = new point2d(0,0);
                
                //Mis puntos 3D del cubo
                punto3d p0 = new punto3d(250,250,5);
                punto3d p1 = new punto3d(300,250,5);
                punto3d p2 = new punto3d(250,300,5);
                punto3d p3 = new punto3d(300,300,5);
                punto3d p4 = new punto3d(250,250,50);
                punto3d p5 = new punto3d(300,250,50);
                punto3d p6 = new punto3d(250,300,50);
                punto3d p7 = new punto3d(300,300,50);
                
                //Obtengo Obtengo y dibujo puntos
                p2d1 = tlbx.perspectiva(p0, camara);
                p2d2 = tlbx.perspectiva(p1, camara);
                DibujarBresenham(p2d1, p2d2, g, Color.red);
                
                p2d2 = tlbx.perspectiva(p2, camara);
                DibujarBresenham(p2d1, p2d2, g, Color.red);
                
                p2d2 = tlbx.perspectiva(p4, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d1 = tlbx.perspectiva(p1, camara);
                p2d2 = tlbx.perspectiva(p5, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d2 = tlbx.perspectiva(p3,camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                //p2d1 = tlbx.paralela(p3, camara);
                p2d1 = tlbx.perspectiva(p2, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d1 = tlbx.perspectiva(p6, camara);
                p2d2 = tlbx.perspectiva(p2,camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d2 = tlbx.perspectiva(p4, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d2 = tlbx.perspectiva(p7, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d1 = tlbx.perspectiva(p7, camara);
                p2d2 = tlbx.perspectiva(p3, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d2 = tlbx.perspectiva(p5, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                p2d1 = p2d2;
                p2d2 = tlbx.perspectiva(p4, camara);
                DibujarBresenham(p2d1,p2d2,g,Color.red);
                
                break;
            default:
                System.out.println("te la pelas");
            }//Fin del Switch
        }
        
    }//fin de dibujar
}
