
package graficas;

import java.util.ArrayList;
import javafx.geometry.Point2D;

public class punto3d {
    public int x;
    public int y;
    public int z;
    
    public punto3d(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    @Override
    public String toString() {
        String s = "Punto{x:" + x + " y: " + y + " z: " + z + "}";
        return s;
    } 
    public static ArrayList<punto3d> clonar(ArrayList<punto3d> lista)
    {
        ArrayList<punto3d> clone = new ArrayList<punto3d>(lista.size());
        for(punto3d item: lista) 
            clone.add(item.clone());
        return clone;
    }
    protected punto3d clone()
    {
        return new punto3d(x, y, z);
    }
}
