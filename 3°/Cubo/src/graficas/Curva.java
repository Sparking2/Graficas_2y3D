
package graficas;

import java.util.ArrayList;
import javafx.geometry.Point2D;

public class Curva {
  
    private ArrayList<punto3d> vertices;
    private ArrayList<Point2D> vertices2D;

    public Curva(ArrayList<punto3d> vertices) {
        this.vertices = vertices;
    }

    public ArrayList<punto3d> getVertices() {
        return vertices;
    }

    public ArrayList<Point2D> getVertices2D() {
        return vertices2D;
    }

    public void setVertices(ArrayList<punto3d> vertices) {
        this.vertices = vertices;
    }

    public void setVertices2D(ArrayList<Point2D> vertices2D) {
        this.vertices2D = vertices2D;
    }
    public static ArrayList<Curva> clonar(ArrayList<Curva> c)
    {
        ArrayList<Curva> clone = new ArrayList<Curva>(c.size());
        for(Curva item: c)
        {
            clone.add(item.clone());
        }
        return clone;
    }
    protected Curva clone()
    {
        return new Curva(punto3d.clonar(vertices));
    }
}
