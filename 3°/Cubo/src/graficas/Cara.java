
package graficas;

public class Cara {
    
    public punto3d p1, p2, p3,p4,pc;
    public int Xmax, Ymax, Zmax;
    
    public Cara(punto3d p1, punto3d p2, punto3d p3, punto3d p4){
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        //Calculamos el punto central
        int Xmin = Math.min(Math.min(p1.x, p2.x), Math.min(p3.x, p4.x));
        int Ymin = Math.min(Math.min(p1.y, p2.y), Math.min(p3.y, p4.y));
        int Zmin = Math.min(Math.min(p1.z, p2.z), Math.min(p3.z, p4.z));
        int Xmax = Math.max(Math.max(p1.x, p2.x), Math.max(p3.x, p4.x));
        int Ymax = Math.max(Math.max(p1.y, p2.y), Math.max(p3.y, p4.y));
        int Zmax = Math.max(Math.max(p1.z, p2.z), Math.max(p3.z, p4.z));
        //Obtenemos la Xmin y Ymin
        pc = new punto3d((Xmax + Xmin) / 2, (Ymax + Ymin) / 2, (Zmax + Zmin) / 2);
        this.Xmax = Xmax;
        this.Zmax = Zmax;
        this.Ymax = Ymax;
    }
    
    @Override
    public String toString() {
        return new String("Punto1: " + p1.toString() + "\nPunto2: " + p2.toString() + "\nPunto3: " + p3.toString() + "\nPunto4: " + p4.toString());
    }
    
}
