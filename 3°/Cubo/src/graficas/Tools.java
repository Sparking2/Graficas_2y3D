package graficas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Point2D;

public class Tools {
    //Tipos de Proyecciones
    public static final int PERSPECTIVA = 1;
    public static final int PARALELA = 2;
    
    //Los ejes de movimiento
     public static final int X_AXIS = 1;
    public static final int Y_AXIS = 2;
    public static final int Z_AXIS = 3;
    public static final int ALL_AXIS = 4;
    private static final double ONE_RADIAN = Math.PI / 180;
    
    private BufferedImage buffer;
    private Graphics graf;
    private ImageObserver imgObs;
    private BufferedImage pix;
    private int[][] Z_Buffer;
    private ArrayList<punto3d> lista;
    
    //Tipos de Cubo
    private Cube cuboParalelo;
    private Cube cuboPerspectivo;
    
    public Tools(BufferedImage buffer, Graphics graf, ImageObserver imgObs){
        this.buffer = buffer;
        this.graf = graf;
        this.imgObs = imgObs;
        pix = new BufferedImage(1,1,BufferedImage.TYPE_INT_RGB);
        lista = new ArrayList<>();
        InicializarZBuffer();
    }//fin del constructor
    
    private void InicializarZBuffer(){
               Z_Buffer = new int[this.buffer.getWidth()][this.buffer.getHeight()];
        for(int x = 0; x < Z_Buffer.length; x++)
        {
            for(int y = 0; y < Z_Buffer[x].length; y++)
            {
                Z_Buffer[x][y] = Integer.MIN_VALUE;
            }//fin del for en y
        }//fin del for en x
    }//fin de Z Buffer
    
    private void putPixel(int x, int y, Color c){
        pix.setRGB(0, 0, c.getRGB());
        graf.drawImage(pix, x, y, imgObs);
    }//fin de putPixel
    
    public void drawLine(int x0, int y0, int x1, int y1, Color c){
                int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int rodzil = dx - dy;
        
        int posun_x,posun_y;
        
        if(x0 < x1) posun_x = 1; else posun_x = -1;
        if(y0 < y1) posun_y = 1; else posun_y = -1;
        
        while ((x0 != x1) || (y0 != y1)){
            int p = 2 * rodzil;
            if (p > -dy){
                rodzil = rodzil - dy;
                x0 = x0 + posun_x;
            }
            
            if (p < dx){
            rodzil = rodzil + dx;
            y0 = y0 + posun_y;
            }
        putPixel(x0, y0, c);  
        }
    }//fin de drawLine
    
    private Point2D Paralela(punto3d p){
        punto3d puntoP = new punto3d(-50,40,100);
        int x = p.x - (p.z * puntoP.x / puntoP.z);
        int y = p.y - (p.z * puntoP.y / puntoP.z);
        return new Point2D(x,y);
    }//fin de paralela
    
    private Point2D Perspectiva(punto3d p){
        punto3d puntoP = new punto3d(50,120,100);
        float u = (float)puntoP.z / (p.z + puntoP.z);
        int x = Math.round((float)(p.x - puntoP.x) * u + puntoP.x);
        int y = Math.round((float)(p.y - puntoP.y) * u + puntoP.y);
        return new Point2D(x,y);
    }//fin de Perspectiva
    
    public void Translate(int incx, int incy, int incz, Cube c)
    {
        //BorrarCubo(c);
        Limpiar();
        c.xc += incx;
        c.yc += incy;
        c.zc += incz;
        //Temp
        c.xcTemp += incx;
        c.ycTemp += incy;
        c.zcTemp += incz;
        
        for(punto3d point3D : c.getVertices())
        {
            point3D.x += incx;
            point3D.y += incy;
            point3D.z += incz;
        }
        ArrayList<punto3d> puntos3D = c.getTmp();//c.getVertices();
        for(punto3d punto3D : puntos3D)
        {
            punto3D.x += incx;
            punto3D.y += incy;
            punto3D.z += incz;
            //System.out.println(punto3D.x);
        }
        //Draw(c, puntos3D, c.getProyeccion());//Se dubija en base a los puntos temporales
        Draw(c, c.getProyeccion());
    }//fin de tranlacion
    
    public void Scale(float sx, float sy, float sz, Cube c)
    {
        //System.out.println("Sx: " + sx + " Sy: " + sy + " Sz: " + sz);
        //BorrarCubo(c);
        Limpiar();
        for(punto3d punto3D : c.getVertices())
        {
            int dx = Math.round(((c.xc - punto3D.x ) * sx) - (c.xc - punto3D.x));
            int dy = Math.round(((c.yc - punto3D.y ) * sy) - (c.yc - punto3D.y));
            int dz = Math.round(((c.zc - punto3D.z ) * sz) - (c.zc - punto3D.z));
            punto3D.x -= dx;
            punto3D.y -= dy;
            punto3D.z -= dz;
           //System.out.println("x: " + punto3D.x + " y: " + punto3D.y  + " z: " + punto3D.z);
        }
        ArrayList<punto3d> puntos3D = c.getTmp();//c.getVertices();
        for(punto3d punto3D : puntos3D)
        {
            int dx = Math.round(((c.xcTemp - punto3D.x ) * sx) - (c.xcTemp - punto3D.x));
            int dy = Math.round(((c.ycTemp - punto3D.y ) * sy) - (c.ycTemp - punto3D.y));
            int dz = Math.round(((c.zcTemp - punto3D.z ) * sz) - (c.zcTemp - punto3D.z));
            punto3D.x -= dx;
            punto3D.y -= dy;
            punto3D.z -= dz;
           //System.out.println("x: " + punto3D.x + " y: " + punto3D.y  + " z: " + punto3D.z);
        }
        //Volvemos a dibujar porque escala mal si esta rotado
        if(c.RotateX != 0) Rotate(c.RotateX, c, X_AXIS);
        else if(c.RotateY != 0) Rotate(c.RotateY, c, Y_AXIS);
        else if(c.RotateZ != 0) Rotate(c.RotateZ, c, Z_AXIS);
        else if(c.RotateX2 != 0) Rotate2(c.RotateX2, c, X_AXIS);
        else if(c.RotateY2 != 0) Rotate2(c.RotateY2, c, Y_AXIS);
        else if(c.RotateZ2 != 0) Rotate2(c.RotateZ2, c, Z_AXIS);
        else Draw(c, c.getProyeccion());
        //Draw(c, puntos3D, c.getProyeccion());//Se dubija en base a los puntos temporales*/
    }// fin de escala
    
    public void Rotate(int grados, Cube c, int axis)
    {
        //BorrarCubo(c);
        Limpiar();
        ArrayList<punto3d> puntos3D = punto3d.clonar(c.getVertices());
        //ArrayList<punto3d> puntosTemp3D = c.getTmp();
        //Convertimos a radianes y sacamos cos y sen
        double radian = grados * ONE_RADIAN;
        double sen = Math.sin(radian), cos = Math.cos(radian);
        //double radian, sen, cos;
        int x, y, z;
        c.RotateX2 = 0; c.RotateY2 = 0; c.RotateZ2 = 0;
        switch(axis)
        {
            case X_AXIS:
                c.RotateX = grados; c.RotateY = 0; c.RotateZ = 0;
                //Rotamos los puntos centrales
                y = Math.round((float)(c.yc * cos - c.zc * sen));
                z = Math.round((float)(c.yc * sen + c.zc * cos));
                c.ycTemp = y;
                c.zcTemp = z;
                //System.out.println("XC:" + c.xcTemp + " YC:" + c.ycTemp + " ZC:" + c.zcTemp);
                for(int i = 0; i < puntos3D.size(); i++)
                {
                    punto3d p = puntos3D.get(i);
                    y = Math.round((float)(p.y * cos - p.z * sen));
                    z = Math.round((float)(p.y * sen + p.z * cos));
                    p.y = y;
                    p.z = z;
                   // System.out.println("x: " + p.x + " y: " + p.y  + " z: " + p.z);
                }
                break;
            case Y_AXIS:
                c.RotateY = grados; c.RotateX = 0; c.RotateZ = 0;
                //Rotamos los puntos centrales
                x = Math.round((float)(c.xc * cos + c.zc * sen));
                z = Math.round((float)(-c.xc * sen + c.zc * cos));
                c.xcTemp = x;
                c.zcTemp = z;
                for(punto3d p : puntos3D)
                {
                    x = Math.round((float)(p.x * cos + p.z * sen));
                    z = Math.round((float)(-p.x * sen + p.z * cos));
                    p.x = x;
                    p.z = z;
                    
                }
                break;
            case Z_AXIS:
                c.RotateZ = grados; c.RotateY = 0; c.RotateX = 0;
                //Rotamos los puntos centrales
                x = Math.round((float)(c.xc * cos - c.yc * sen));
                y = Math.round((float)(c.xc * sen + c.yc * cos));
                c.xcTemp = x;
                c.ycTemp = y;
                for(punto3d p : puntos3D)
                {
                    x = Math.round((float)(p.x * cos - p.y * sen));
                    y = Math.round((float)(p.x * sen + p.y * cos));
                    p.x = x;
                    p.y = y;
                    
                }
                break;
        }
        //System.out.println("Punto central{X:" + c.xcTemp + " Y:" + c.ycTemp + " Z:" + c.zcTemp);
        c.setTmp(puntos3D);
        //Draw(c, puntos3D, c.getProyeccion());
        Draw(c, c.getProyeccion());
    }//fin de rotacion
    
    public void Draw(Cube c, int proyeccion)
    {
        //Guardamos en el objeto la proyeccion que suará
        c.setProyeccion(proyeccion);
        
        ArrayList<Point2D> puntos2D = new ArrayList<>(8);
        ArrayList<punto3d> puntos3D = c.getTmp();//c.getVertices();
        //Hacemos proyección en los 8 vertices
        switch (proyeccion) {
            case PARALELA:
                for(punto3d punto3D : puntos3D)
                {
                    //System.out.println(punto3D.toString());
                    puntos2D.add(Paralela(punto3D));
                }   break;
            case PERSPECTIVA:
                for(punto3d punto3D : puntos3D)
                {
                    //System.out.println(punto3D.toString());
                    puntos2D.add(Perspectiva(punto3D));
                }   break;
            default:
                return;
        }
        //Lo guardamos en la variable del Cubo
        c.setVertices2D(puntos2D);
        /*for(Point2D punto2D : puntos2D)
        {
            System.out.println(punto2D.toString());
        }*/
        Point2D p1 = puntos2D.get(0);
        Point2D p2 = puntos2D.get(1);
        Point2D p3 = puntos2D.get(2);
        Point2D p4 = puntos2D.get(3);
        Point2D p5 = puntos2D.get(4);
        Point2D p6 = puntos2D.get(5);
        Point2D p7 = puntos2D.get(6);
        Point2D p8 = puntos2D.get(7);
        //Primera cara
        drawLine((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY(), Color.red);
        drawLine((int)p1.getX(), (int)p1.getY(), (int)p3.getX(), (int)p3.getY(), Color.red);
        drawLine((int)p2.getX(), (int)p2.getY(), (int)p4.getX(), (int)p4.getY(), Color.red);
        drawLine((int)p3.getX(), (int)p3.getY(), (int)p4.getX(), (int)p4.getY(), Color.red);
        //Cara trasera
        drawLine((int)p5.getX(), (int)p5.getY(), (int)p6.getX(), (int)p6.getY(), Color.red);
        drawLine((int)p5.getX(), (int)p5.getY(), (int)p7.getX(), (int)p7.getY(), Color.red);
        drawLine((int)p6.getX(), (int)p6.getY(), (int)p8.getX(), (int)p8.getY(), Color.red);
        drawLine((int)p7.getX(), (int)p7.getY(), (int)p8.getX(), (int)p8.getY(), Color.red);
        //Unir caras
        drawLine((int)p1.getX(), (int)p1.getY(), (int)p5.getX(), (int)p5.getY(), Color.red);
        drawLine((int)p2.getX(), (int)p2.getY(), (int)p6.getX(), (int)p6.getY(), Color.red);
        drawLine((int)p3.getX(), (int)p3.getY(), (int)p7.getX(), (int)p7.getY(), Color.red);
        drawLine((int)p4.getX(), (int)p4.getY(), (int)p8.getX(), (int)p8.getY(), Color.red);
        //Diagonales
        /*drawLine((int)p2.getX(), (int)p2.getY(), (int)p3.getX(), (int)p3.getY(), Color.red);
        drawLine((int)p3.getX(), (int)p3.getY(), (int)p5.getX(), (int)p5.getY(), Color.red);
        drawLine((int)p5.getX(), (int)p5.getY(), (int)p8.getX(), (int)p8.getY(), Color.red);
        drawLine((int)p8.getX(), (int)p8.getY(), (int)p2.getX(), (int)p2.getY(), Color.red);
        drawLine((int)p8.getX(), (int)p8.getY(), (int)p3.getX(), (int)p3.getY(), Color.red);
        drawLine((int)p5.getX(), (int)p5.getY(), (int)p2.getX(), (int)p2.getY(), Color.red);*/
        
        //Actualizamos el ZBuffer
        InicializarZBuffer();
        CalculatePoint3D(puntos3D.get(0), puntos3D.get(1));
        CalculatePoint3D(puntos3D.get(0), puntos3D.get(2));
        CalculatePoint3D(puntos3D.get(1), puntos3D.get(3));
        CalculatePoint3D(puntos3D.get(2), puntos3D.get(3));
        
        CalculatePoint3D(puntos3D.get(4), puntos3D.get(5));
        CalculatePoint3D(puntos3D.get(4), puntos3D.get(6));
        CalculatePoint3D(puntos3D.get(5), puntos3D.get(7));
        CalculatePoint3D(puntos3D.get(6), puntos3D.get(7));
        
        CalculatePoint3D(puntos3D.get(0), puntos3D.get(4));
        CalculatePoint3D(puntos3D.get(1), puntos3D.get(5));
        CalculatePoint3D(puntos3D.get(2), puntos3D.get(6));
        CalculatePoint3D(puntos3D.get(3), puntos3D.get(7));
        
        //Cara Front
        Cara front = new Cara(puntos3D.get(0), puntos3D.get(1), puntos3D.get(2), puntos3D.get(3));
        //Rellenar(front);
        //Top
        //Cara top = new Cara(puntos3D.get(1), puntos3D.get(3), puntos3D.get(5), puntos3D.get(7));
        //Rellenar(top);
        //Cara Front
        Cara back = new Cara(puntos3D.get(4), puntos3D.get(5), puntos3D.get(5), puntos3D.get(6));
        //Rellenar(back);
    }//fin de Dinujar
    
     private void Rellenar(Cara cara)
    {
        //System.out.println(cara.toString());
        Inundar(cara.pc, cara);
        if(!lista.isEmpty())
        {
            System.out.println("No estoy vacia");
            for(int i = 0; i < lista.size(); i++)
            {
                punto3d p = lista.get(i);
                Inundar(p, cara);
            }
        }
    }//fin del relleno
     
         public void Rotate2(int grados, Cube c, int axis)
    {
        //BorrarCubo(c);
        Limpiar();
        ArrayList<punto3d> puntos3D = punto3d.clonar(c.getVertices());
        //ArrayList<Point3D> puntosTemp3D = c.getTmp();
        //Convertimos a radianes y sacamos cos y sen
        double radian = grados * ONE_RADIAN;
        double sen = Math.sin(radian), cos = Math.cos(radian);
        //double radian, sen, cos;
        int x, y, z, dx, dy, dz;
        c.RotateX = 0; c.RotateY = 0; c.RotateZ = 0;
        switch(axis)
        {
            case X_AXIS:
                c.RotateX2 = grados; c.RotateY2 = 0; c.RotateZ2 = 0;
                //Rotamos los puntos centrales
                y = Math.round((float)(c.yc * cos - c.zc * sen));
                z = Math.round((float)(c.yc * sen + c.zc * cos));
                dy = c.yc - y;
                dz = c.zc - z;
                //System.out.println("dy: " + dy + " dz: " + dz);
                for(int i = 0; i < puntos3D.size(); i++)
                {
                    punto3d p = puntos3D.get(i);
                    y = Math.round((float)(p.y * cos - p.z * sen));
                    z = Math.round((float)(p.y * sen + p.z * cos));
                    p.y = y + dy;
                    p.z = z + dz;
                    //System.out.println("x: " + p.x + " y: " + p.y  + " z: " + p.z);
                }
                break;
            case Y_AXIS:
                c.RotateY2 = grados; c.RotateX2 = 0; c.RotateZ2 = 0;
                //Rotamos los puntos centrales
                x = Math.round((float)(c.xc * cos + c.zc * sen));
                z = Math.round((float)(-c.xc * sen + c.zc * cos));
                dx = c.xc - x;
                dz = c.zc - z;
                for(punto3d p : puntos3D)
                {
                    x = Math.round((float)(p.x * cos + p.z * sen));
                    z = Math.round((float)(-p.x * sen + p.z * cos));
                    p.x = x + dx;
                    p.z = z + dz;
                    
                }
                break;
            case Z_AXIS:
                c.RotateZ2 = grados; c.RotateY2 = 0; c.RotateX2 = 0;
                //Rotamos los puntos centrales
                x = Math.round((float)(c.xc * cos - c.yc * sen));
                y = Math.round((float)(c.xc * sen + c.yc * cos));
                dx = c.xc - x;
                dy = c.yc - y;
                for(punto3d p : puntos3D)
                {
                    x = Math.round((float)(p.x * cos - p.y * sen));
                    y = Math.round((float)(p.x * sen + p.y * cos));
                    p.x = x + dx;
                    p.y = y + dy;
                    
                }
                break;
            case ALL_AXIS:
                //puntos3D = c.getTmp();
                c.RotateX2 = grados; c.RotateY2 = grados; c.RotateZ2 = grados;
                //Rotamos los puntos centrales
                y = Math.round((float)(c.yc * cos - c.zc * sen));
                z = Math.round((float)(c.yc * sen + c.zc * cos));
                dy = c.yc - y;
                dz = c.zc - z;
                //System.out.println("dy: " + dy + " dz: " + dz);
                for(int i = 0; i < puntos3D.size(); i++)
                {
                    punto3d p = puntos3D.get(i);
                    y = Math.round((float)(p.y * cos - p.z * sen));
                    z = Math.round((float)(p.y * sen + p.z * cos));
                    p.y = y + dy;
                    p.z = z + dz;
                }
                //Y
                x = Math.round((float)(c.xc * cos + c.zc * sen));
                z = Math.round((float)(-c.xc * sen + c.zc * cos));
                dx = c.xc - x;
                dz = c.zc - z;
                for(punto3d p : puntos3D)
                {
                    x = Math.round((float)(p.x * cos + p.z * sen));
                    z = Math.round((float)(-p.x * sen + p.z * cos));
                    p.x = x + dx;
                    p.z = z + dz;
                    
                }
                x = Math.round((float)(c.xc * cos - c.yc * sen));
                y = Math.round((float)(c.xc * sen + c.yc * cos));
                dx = c.xc - x;
                dy = c.yc - y;
                for(punto3d p : puntos3D)
                {
                    x = Math.round((float)(p.x * cos - p.y * sen));
                    y = Math.round((float)(p.x * sen + p.y * cos));
                    p.x = x + dx;
                    p.y = y + dy;
                    
                }
                break;
        }
        //System.out.println("Punto central{X:" + c.xcTemp + " Y:" + c.ycTemp + " Z:" + c.zcTemp);
        c.setTmp(puntos3D);
        //Draw(c, puntos3D, c.getProyeccion());
        Draw(c, c.getProyeccion());
    }//el final de la rotacion sobre si mismo
    
    private void Inundar(punto3d pc, Cara cara){   
        Point2D p2D = Paralela(pc);
        punto3d p3DR = null, p3DD = null, p3DL = null, p3DU = null;
        if(pc.z >= Z_Buffer[(int)p2D.getX()][(int)p2D.getY()])
        {
            //System.out.println(p2D.getX());
            if(buffer.getRGB((int)p2D.getX(), (int)p2D.getY()) != Color.red.getRGB() || pc.z != Z_Buffer[(int)p2D.getX()][(int)p2D.getY()])
            {
                if(buffer.getRGB((int)p2D.getX(), (int)p2D.getY()) != Color.black.getRGB())
                {
                    putPixel((int)p2D.getX(), (int)p2D.getY(), Color.black);
                    try
                    {
                        //Derecha
                        p3DR = new punto3d(pc.x++, pc.y, pc.z);
                        p3DR.z += CalculateZ(cara.Xmax, cara.Zmax, cara.pc.z, pc.x);
                        //Inundar(p3D, cara);
                        
                        //Abajo
                        p3DD = new punto3d(pc.x, pc.y++, pc.z);
                        p3DD.z += CalculateZ(cara.Ymax, cara.Zmax, cara.pc.z, pc.y);
                        //Inundar(p3D, cara);
                        
                        //Izquierda
                        p3DL = new punto3d(pc.x--, pc.y, pc.z);
                        p3DL.z += CalculateZ(cara.Xmax, cara.Zmax, cara.pc.z, pc.x);
                        //Inundar(p3D, cara);
                        
                        //Arriba
                        p3DU = new punto3d(pc.x, pc.y--, pc.z);
                        p3DU.z += CalculateZ(cara.Ymax, cara.Zmax, cara.pc.z, pc.y);
                        //Inundar(p3D, cara);
                        Inundar(p3DR, cara);
                        Inundar(p3DD, cara);
                        Inundar(p3DL, cara);
                        Inundar(p3DU, cara);
                    }//fin del try
                    catch(StackOverflowError s)
                    {
                        lista.add(p3DR);
                        lista.add(p3DD);
                        lista.add(p3DL);
                        lista.add(p3DU);
                    }//fin del catch
                }//fin del if del color
            }//fin del if del buffer
        }//fin del if
    }//fin de inundar
    
    private int CalculateZ(int Xmax, int Zmax, int pcZ, int pX){
        return Math.round((float)(Zmax - pcZ) / (Xmax - pX));
    }
    
        private void CalculatePoint3D(punto3d p0, punto3d p1)
    {
        int x, y, p, A, B;
        int incx, incy;
        
        Point2D p02D = Paralela(p0);
        Point2D p12D = Paralela(p1);
        //Agregamos el primer punto
        try
        {
            if(Z_Buffer[(int)p02D.getX()][(int)p02D.getY()] < p0.z)
                Z_Buffer[(int)p02D.getX()][(int)p02D.getY()] = p0.z;
        } catch (ArrayIndexOutOfBoundsException e){}
        
        int dx = (int)(p12D.getX() - p02D.getX());
        int dy = (int)(p12D.getY() - p02D.getY());
        
        incx = (dx > 0) ? 1 : ((dx == 0) ? 0 : -1);
        incy = (dy > 0) ? 1 : ((dy == 0) ? 0 : -1);
        dx = Math.abs(dx);
        dy = Math.abs(dy);
        x =  (int)p02D.getX();
        y =  (int)p02D.getY();
        
        int iZ = 1;
        if (dx > dy) {
            //Calculamos la constante de aumento en Z
            float constZ = (float)(p1.z - p0.z) / (float)(p12D.getX() - p02D.getX());
            if(p02D.getX() > p12D.getX() || p0.z < p1.z)
                constZ *= -1;
            
            A = 2 * dy;
            B = 2 * (dy - dx);
            p = (2 * dy) - dx;

            while (x != p12D.getX()) {
                x = x + incx;
                if (p < 0) {       
                    p = p + A;
                }
                else {
                    y = y + incy;
                    p = p + B;
                }
                //Calculamos el valor de z
                int z = p0.z + Math.round(constZ * iZ);
                try
                {
                    if(Z_Buffer[x][y] < z)
                        Z_Buffer[x][y]= z;
                }
                catch(ArrayIndexOutOfBoundsException e){}
                iZ++;
            } 
        }
        else {
            //Calculamos la constante de aumento en Z
            float constZ = (float)(p1.z - p0.z) / (float)(p12D.getY() - p02D.getY());
            if(p02D.getY() > p12D.getY() || p0.z < p1.z)
                constZ *= -1;
            
            A = 2 * dx;
            B = 2 * (dx - dy);
            p = (2 * dx) - dy;

            while (y != p12D.getY()) {
                y = y + incy;
                if (p < 0) {       
                    p = p + A;
                }
                else {
                    x = x + incx;
                    p = p + B;
                }
                //Calculamos el valor de z
                int z = p0.z + Math.round(constZ * iZ);
                try
                {
                    if(Z_Buffer[x][y] < z)
                        Z_Buffer[x][y]= z;
                }
                catch(ArrayIndexOutOfBoundsException e){}
                iZ++;
            } 
            
        }
    }
        
   public void Draw(Cube c, ArrayList<punto3d> puntos3D, int proyeccion){   
        ArrayList<Point2D> puntos2D = new ArrayList<>(8);
        //Hacemos proyección en los 8 vertices
        switch (proyeccion) {
            case PARALELA:
                for(punto3d punto3D : puntos3D)
                {
                    puntos2D.add(Paralela(punto3D));
                    System.out.println(punto3D.toString());
                }
                break;
            case PERSPECTIVA:
                for(punto3d punto3D : puntos3D)
                    puntos2D.add(Perspectiva(punto3D));
                break;
            default:
                return;
        }
        //Lo guardamos en la variable del Cubo
        //c.setVertices2D(puntos2D);
        
        Point2D p1 = puntos2D.get(0);
        Point2D p2 = puntos2D.get(1);
        Point2D p3 = puntos2D.get(2);
        Point2D p4 = puntos2D.get(3);
        Point2D p5 = puntos2D.get(4);
        Point2D p6 = puntos2D.get(5);
        Point2D p7 = puntos2D.get(6);
        Point2D p8 = puntos2D.get(7);
        //Primera cara
        drawLine((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY(), Color.red);
        drawLine((int)p1.getX(), (int)p1.getY(), (int)p3.getX(), (int)p3.getY(), Color.red);
        drawLine((int)p2.getX(), (int)p2.getY(), (int)p4.getX(), (int)p4.getY(), Color.red);
        drawLine((int)p3.getX(), (int)p3.getY(), (int)p4.getX(), (int)p4.getY(), Color.red);
        //Cara trasera
        drawLine((int)p5.getX(), (int)p5.getY(), (int)p6.getX(), (int)p6.getY(), Color.red);
        drawLine((int)p5.getX(), (int)p5.getY(), (int)p7.getX(), (int)p7.getY(), Color.red);
        drawLine((int)p6.getX(), (int)p6.getY(), (int)p8.getX(), (int)p8.getY(), Color.red);
        drawLine((int)p7.getX(), (int)p7.getY(), (int)p8.getX(), (int)p8.getY(), Color.red);
        //Unir caras
        drawLine((int)p1.getX(), (int)p1.getY(), (int)p5.getX(), (int)p5.getY(), Color.red);
        drawLine((int)p2.getX(), (int)p2.getY(), (int)p6.getX(), (int)p6.getY(), Color.red);
        drawLine((int)p3.getX(), (int)p3.getY(), (int)p7.getX(), (int)p7.getY(), Color.red);
        drawLine((int)p4.getX(), (int)p4.getY(), (int)p8.getX(), (int)p8.getY(), Color.red);
    }
   
    public void Limpiar(){
        graf.setColor(Color.BLACK);
        graf.fillRect(0, 0, buffer.getWidth(), buffer.getHeight());
    }
    
     public void Draw(Superficie c, int proyeccion)
    {
        //Guardamos en el objeto la proyeccion que suará
        c.setProyeccion(proyeccion);
        
        ArrayList<Curva> curvas = c.getTmp();
        switch (proyeccion) {
            case PARALELA:
                for(Curva curva : curvas)
                {
                    ArrayList<Point2D> puntos2D = new ArrayList<>();
                    ArrayList<punto3d> vertices = curva.getVertices();
                    for(int i = 0; i < vertices.size() - 1; i++)
                    {
                        puntos2D.add(Paralela(vertices.get(i)));
                    }
                    //Se acabo una curva
                    curva.setVertices2D(puntos2D);
                    for(int i = 0; i < puntos2D.size() - 1; i++)
                    {
                        Point2D p1  = puntos2D.get(i);
                        Point2D p2  = puntos2D.get(i+1);
                        drawLine((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY(), Color.white);
                    }
                    //puntos2D.clear();
                } 
                //Realiza el mallado
                for(int i = 0; i < curvas.size() - 1; i++)
                {
                    ArrayList<Point2D> pC1 = curvas.get(i).getVertices2D();
                    ArrayList<Point2D> pC2 = curvas.get(i+1).getVertices2D();
                    for(int m = 0; m < pC1.size() - 1; m++)
                    {
                        Point2D p1C1 = pC1.get(m);
                        Point2D p1C2 = pC2.get(m);
                        Point2D p2C1 = pC1.get(m+1);
                        Point2D p2C2 = pC2.get(m+1);
                        drawLine((int)p1C1.getX(), (int)p1C1.getY(), (int)p2C2.getX(), (int)p2C2.getY(), Color.white);
                        drawLine((int)p2C1.getX(), (int)p2C1.getY(), (int)p1C2.getX(), (int)p1C2.getY(), Color.white);
                    }
                }
                //Unir la ultima curva
                ArrayList<Point2D> pC1t = curvas.get(0).getVertices2D();
                ArrayList<Point2D> pC2t = curvas.get(curvas.size()-1).getVertices2D();
                for(int m = 0; m < pC1t.size() - 1; m++)
                {
                    Point2D p1C1 = pC1t.get(m);
                    Point2D p1C2 = pC2t.get(m);
                    Point2D p2C1 = pC1t.get(m+1);
                    Point2D p2C2 = pC2t.get(m+1);
                    drawLine((int)p1C1.getX(), (int)p1C1.getY(), (int)p2C2.getX(), (int)p2C2.getY(), Color.white);
                    drawLine((int)p2C1.getX(), (int)p2C1.getY(), (int)p1C2.getX(), (int)p1C2.getY(), Color.white);
                }
                break;
            case PERSPECTIVA:
                for(Curva curva : curvas)
                {
                    ArrayList<Point2D> puntos2D = new ArrayList<>();
                    ArrayList<punto3d> vertices = curva.getVertices();
                    for(int i = 0; i < vertices.size() - 1; i++)
                    {
                        puntos2D.add(Perspectiva(vertices.get(i)));
                    }
                    //Se acabo la una curva
                    curva.setVertices2D(puntos2D);
                    for(int i = 0; i < puntos2D.size() - 1; i++)
                    {
                        Point2D p1  = puntos2D.get(i);
                        Point2D p2  = puntos2D.get(i+1);
                        drawLine((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY(), Color.white);
                    }
                    //puntos2D.clear();
                } 
                for(int i = 0; i < curvas.size() - 1; i++)
                {
                    ArrayList<Point2D> pC1 = curvas.get(i).getVertices2D();
                    ArrayList<Point2D> pC2 = curvas.get(i+1).getVertices2D();
                    for(int m = 0; m < pC1.size(); m++)
                    {
                        Point2D p1 = pC1.get(m);
                        Point2D p2 = pC2.get(m);
                        drawLine((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY(), Color.white);
                    }
                }
            default:
                return;
        }
        //Lo guardamos en la variable del Cubo
        //c.setVertices2D(puntos2D);
        /*for(Point2D punto2D : puntos2D)
        {
            System.out.println(punto2D.toString());
        }*/
        
    }
     
    public void Translate(int incx, int incy, int incz, Superficie c){
        Limpiar();
        //c.getCurvas().clear();
        c.xc += incx;
        c.yc += incy;
        c.zc += incz;
        for(Curva curva : c.getCurvas()){
            for(punto3d p : curva.getVertices()){
                p.x += incx;
                p.y += incy;
                p.z += incz;
            }
        }
        //Temp
        c.xcTemp += incx;
        c.ycTemp += incy;
        c.zcTemp += incz;
        for(Curva curva : c.getTmp()){
            for(punto3d p : curva.getVertices()){
                p.x += incx;
                p.y += incy;
                p.z += incz;
            }
        }
        //c.CreacionCurvas();
        Draw(c, c.getProyeccion());
    }
    
  public void Scale(float sx, float sy, float sz, Superficie c){
        Limpiar();
        for(Curva curva : c.getCurvas())
        {
            for(punto3d punto3D : curva.getVertices())
            {
                int dx = Math.round(((c.xc - punto3D.x ) * sx) - (c.xc - punto3D.x));
                int dy = Math.round(((c.yc - punto3D.y ) * sy) - (c.yc - punto3D.y));
                int dz = Math.round(((c.zc - punto3D.z ) * sz) - (c.zc - punto3D.z));
                punto3D.x -= dx;
                punto3D.y -= dy;
                punto3D.z -= dz;
            }
        }
       
        for(Curva curva : c.getTmp())
        {
             for(punto3d punto3D : curva.getVertices())
            {
                int dx = Math.round(((c.xcTemp - punto3D.x ) * sx) - (c.xcTemp - punto3D.x));
                int dy = Math.round(((c.ycTemp - punto3D.y ) * sy) - (c.ycTemp - punto3D.y));
                int dz = Math.round(((c.zcTemp - punto3D.z ) * sz) - (c.zcTemp - punto3D.z));
                punto3D.x -= dx;
                punto3D.y -= dy;
                punto3D.z -= dz;
            }
        }
        //Volvemos a dibujar porque escala mal si esta rotado
        /*if(c.RotateX != 0) Rotate(c.RotateX, c, X_AXIS);
        else if(c.RotateY != 0) Rotate(c.RotateY, c, Y_AXIS);
        else if(c.RotateZ != 0) Rotate(c.RotateZ, c, Z_AXIS);*/
        if(c.RotateX2 != 0) Rotate2(c.RotateX2, c, X_AXIS);
        else if(c.RotateY2 != 0) Rotate2(c.RotateY2, c, Y_AXIS);
        else if(c.RotateZ2 != 0) Rotate2(c.RotateZ2, c, Z_AXIS);
        else Draw(c, c.getProyeccion());
        //Draw(c, puntos3D, c.getProyeccion());//Se dubija en base a los puntos temporales*/
    }
  public void Rotate2(int grados, Superficie c, int axis){
        Limpiar();
        ArrayList<Curva> curvas = Curva.clonar(c.getCurvas());
        //ArrayList<punto3d> puntosTemp3D = c.getTmp();
        //Convertimos a radianes y sacamos cos y sen
        double radian = grados * ONE_RADIAN;
        double sen = Math.sin(radian), cos = Math.cos(radian);
        //double radian, sen, cos;
        int x, y, z, dx, dy, dz;
        c.RotateX = 0; c.RotateY = 0; c.RotateZ = 0;
        switch(axis)
        {
            case X_AXIS:
                c.RotateX2 = grados; c.RotateY2 = 0; c.RotateZ2 = 0;
                //Rotamos los puntos centrales
                y = Math.round((float)(c.yc * cos - c.zc * sen));
                z = Math.round((float)(c.yc * sen + c.zc * cos));
                dy = c.yc - y;
                dz = c.zc - z;
                for(Curva curva : curvas)
                {
                     for(punto3d p : curva.getVertices())
                    {
                        y = Math.round((float)(p.y * cos - p.z * sen));
                        z = Math.round((float)(p.y * sen + p.z * cos));
                        p.y = y + dy;
                        p.z = z + dz;
                    }
                }
                break;
            case Y_AXIS:
                c.RotateY2 = grados; c.RotateX2 = 0; c.RotateZ2 = 0;
                //Rotamos los puntos centrales
                x = Math.round((float)(c.xc * cos + c.zc * sen));
                z = Math.round((float)(-c.xc * sen + c.zc * cos));
                dx = c.xc - x;
                dz = c.zc - z;
                for(Curva curva : curvas)
                {
                    for(punto3d p : curva.getVertices())
                    {
                        x = Math.round((float)(p.x * cos + p.z * sen));
                        z = Math.round((float)(-p.x * sen + p.z * cos));
                        p.x = x + dx;
                        p.z = z + dz;

                    }
                }
                break;
            case Z_AXIS:
                c.RotateZ2 = grados; c.RotateY2 = 0; c.RotateX2 = 0;
                //Rotamos los puntos centrales
                x = Math.round((float)(c.xc * cos - c.yc * sen));
                y = Math.round((float)(c.xc * sen + c.yc * cos));
                dx = c.xc - x;
                dy = c.yc - y;
                for(Curva curva : curvas)
                {
                    for(punto3d p : curva.getVertices())
                    {
                        x = Math.round((float)(p.x * cos - p.y * sen));
                        y = Math.round((float)(p.x * sen + p.y * cos));
                        p.x = x + dx;
                        p.y = y + dy;

                    }
                }
                break;
        }
        //System.out.println("Punto central{X:" + c.xcTemp + " Y:" + c.ycTemp + " Z:" + c.zcTemp);
        c.setTmp(curvas);
        //Draw(c, puntos3D, c.getProyeccion());
        Draw(c, c.getProyeccion());
    }

  public static class Cube{
        private ArrayList<punto3d> vertices;
        private ArrayList<punto3d> tmp;
        private ArrayList<Point2D> vertices2D;
        private int xc, yc, zc, u;
        private int xcTemp, ycTemp, zcTemp;
        private int proyeccion;
        public int RotateX, RotateY, RotateZ;
        public int RotateX2, RotateY2, RotateZ2;
        
        public Cube(ArrayList<punto3d> vertices, int xc, int yc, int zc, int u) {
            this.vertices = vertices;
            this.u = u;
            this.xc = xc;
            this.yc = yc;
            this.zc = zc;
        }
        public Cube(int xc, int yc, int zc, int u) {
            this.vertices = new ArrayList<>();
            this.vertices2D = new ArrayList<>();
            this.u = u;
            this.xc = xc;
            this.yc = yc;
            this.zc = zc;
            
            int px = u * 10; //Cada unida equivale a 10 px
            int u_2 = px /2;
            //Calculamos los 8 vertices que conforman el cubo
            //punto3d p1, p2, p3, p4, p5, p6, p7, p8;
            vertices.add(new punto3d(xc - u_2, yc + u_2, zc + u_2));
            vertices.add(new punto3d(xc - u_2, yc - u_2, zc + u_2));
            vertices.add(new punto3d(xc + u_2, yc + u_2, zc + u_2));
            vertices.add(new punto3d(xc + u_2, yc - u_2, zc + u_2));
            vertices.add(new punto3d(xc - u_2, yc + u_2, zc - u_2));
            vertices.add(new punto3d(xc - u_2, yc - u_2, zc - u_2));
            vertices.add(new punto3d(xc + u_2, yc + u_2, zc - u_2));
            vertices.add(new punto3d(xc + u_2, yc - u_2, zc - u_2));
            
            tmp = punto3d.clonar(vertices);
            xcTemp = xc;
            ycTemp = yc;
            zcTemp = zc;
        }
        public void setVertices(ArrayList<punto3d> vertices) {
            this.vertices = vertices;
        }
        public ArrayList<punto3d> getVertices() {
            return vertices;
        }
        public void setVertices2D(ArrayList<Point2D> vertices2D)
        {
            this.vertices2D = vertices2D;
        }
        public ArrayList<Point2D> getVertices2D() {
            return vertices2D;
        }

        public void setProyeccion(int proyeccion) {
            this.proyeccion = proyeccion;
        }

        public int getProyeccion() {
            return proyeccion;
        }

        public void setTmp(ArrayList<punto3d> tmp) {
            this.tmp = tmp;
        }

        public ArrayList<punto3d> getTmp() {
            return tmp;
        }
  }
  
      public static class Superficie
    {
        private ArrayList<Curva> curvas;
        private ArrayList<Curva> tmp;
        private ArrayList<Point2D> vertices2D;
        private int xc, yc, zc, u;
        private int xcTemp, ycTemp, zcTemp;
        private int proyeccion;
        public int RotateX, RotateY, RotateZ;
        public int RotateX2, RotateY2, RotateZ2;

        public Superficie(int xc, int yc, int zc, int u, boolean curva) {
            this.xc = xc;
            this.yc = yc;
            this.zc = zc;
            this.u = u * 10;
            this.curvas = new ArrayList<>();
            if(curva == true)
                CreacionCurvas();
            else
                CreacionCurvas2();
            tmp = Curva.clonar(curvas);
        }
        private void CreacionCurvas()
        {
            //Creamos las curvas con que se compondra la figura
            int xLim = this.xc + this.u;
            int yLim = this.yc + this.u;
            ArrayList<punto3d> verticesCurva = new ArrayList<>();
            for(int x =  this.xc - this.u; x < xLim; x+=10)
            {
                for(int y = this.yc - this.u; y < yLim; y+=5 )
                {
                    punto3d p3D = CalcularZSuperficie(x, y);
                    //System.out.println(p3D.toString());
                    verticesCurva.add(p3D);
                }
                this.curvas.add(new Curva(punto3d.clonar(verticesCurva)));
                verticesCurva.clear();
            }
        }
        public void setVertices(ArrayList<Curva> curvas) {
            this.curvas = curvas;
        }
        public ArrayList<Curva> getCurvas() {
            return curvas;
        }
        
        private punto3d CalcularZSuperficie(int x, int y)
        {
            int z = (int)(Math.pow(x, 2) + Math.pow(y, 2)) / 1000 + zc;
            return new punto3d(x, y, z);
        }
        private void CreacionCurvas2()
        {
            //Creamos las curvas con que se compondra la figura
            //int xLim = this.xc + this.u;
            //int yLim = this.yc + this.u;
            ArrayList<punto3d> verticesCurva = new ArrayList<>();
            for(double t = 1; t < 360; t+=5)//Gira la figura
            {
                for(double o = 1; o < 360; o+=5)//Le da la altura
                {
                    punto3d p3D = CalcularZSuperficie2(o, t);
                    //System.out.println(p3D.toString());
                    verticesCurva.add(p3D);
                }
                this.curvas.add(new Curva(punto3d.clonar(verticesCurva)));
                verticesCurva.clear();
            }
        }
        private punto3d CalcularZSuperficie2(double t, double o)
        {
            double tR = t * ONE_RADIAN;
            double oR = o * ONE_RADIAN;
            int x = (int) Math.round((2 + Math.cos(tR)) * Math.cos(oR)* 30) + this.xc;
            //int y = (int) Math.round((2 + Math.cos(tR)) * Math.sin(oR)) * 10 + this.yc;
            //int z = (int)tR * 10 + this.zc;
            int y = (int) Math.round(tR * 30 + this.yc);
            int z = (int) Math.round((2 + Math.cos(tR)) * Math.sin(oR) * 30) + this.zc;
    
            
            return new punto3d(x, y, z);
        }
        public void setProyeccion(int proyeccion) {
            this.proyeccion = proyeccion;
        }

        public int getProyeccion() {
            return proyeccion;
        }

        public ArrayList<Curva> getTmp() {
            return tmp;
        }

        public void setTmp(ArrayList<Curva> tmp) {
            this.tmp = tmp;
        }
        
    }
  
      public static class LaCurva{
         private ArrayList<Curva> curvas;
        private ArrayList<Curva> tmp;
        private ArrayList<Point2D> vertices2D;
        private int xc, yc, zc, u;
        private int xcTemp, ycTemp, zcTemp;
        private int proyeccion;
        public int RotateX, RotateY, RotateZ;
        public int RotateX2, RotateY2, RotateZ2;
      }
  
}
