package graficas;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import graficas.Tools.Cube;
import graficas.Tools.Superficie;

public class MainClass extends JFrame implements KeyListener, Runnable {
    
    //Cada uno de los modos que podre usar
    private static final int TRANSLACION = 1;
    private static final int ESCALA = 2;
    private static final int ROTACION = 3;
    private static final int ROTACION_SIMISMO = 4;
    private static final int CUBO_PARALELA = 5;
    private static final int CUBO_PERSPECTIVA = 6;
    private static final int SUPERFICIE_PERSPECTIVA = 7;
    private static final int SUPERFICIE_PARALELA = 8;
    
    private BufferedImage buffer;
    private Graphics g;
    private Tools f;
    private Cube cubo;
    private Superficie superficie;
    private int Modalidad;
    private int figura;
    private float sx = 1, sy = 1, sz = 1;
    private int tetaX = 0, tetaY = 0, tetaZ = 0;
    private int tetaX2 = 0, tetaY2 = 0, tetaZ2 = 0;
    private Thread hilo;
    
    public MainClass(){
        super("Figuras");
        setSize(800,600);
        setLocation(400,300);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        addKeyListener(this);
        
        //Modo en el que iniciara
        Modalidad = TRANSLACION;
        //Lo primero que dibujara
        figura = CUBO_PARALELA;
        //Mi buffer
        buffer = new BufferedImage(800,600,BufferedImage.TYPE_INT_RGB);
        g = buffer.getGraphics();
        f = new Tools(buffer,g,this);
        //Genero el cubo que dibujare
        superficie = new Superficie(200,300,100,100,true);
        cubo = new Cube(400,300,100,10);
        //lo dibujo y elijo el modo de verlo
        f.Draw(cubo,Tools.PARALELA);
        //f.Draw(superficie, Tools.PARALELA);
        getGraphics().drawImage(buffer, 0, 0, this);
        
        hilo = new Thread(this);
        hilo.start();
    }// Fin del constructor MainClass
    
    public static void main(String[] args){
        new MainClass();
    }//Fin del main
    
    private void ProcesarTecla(KeyEvent e){
        System.out.println(e.getKeyCode());
        switch(e.getKeyCode()){
            case KeyEvent.VK_RIGHT:
                figura = figura + 1;
                if(figura > SUPERFICIE_PARALELA)
                    figura = CUBO_PARALELA;
                tetaX = 0; tetaX2 = 0; tetaY = 0; tetaY2 = 0; tetaZ = 0; tetaZ2 = 0;
                sx = 1; sy = 1; sz = 1;
                switch(figura){
                    case CUBO_PARALELA:
                        figura = CUBO_PARALELA;
                        f.Limpiar();
                        cubo = new Cube(200,300,40,10);
                        f.Draw(cubo,Tools.PARALELA);
                        break;
                    case CUBO_PERSPECTIVA:
                        figura = CUBO_PERSPECTIVA;
                        f.Limpiar();
                        cubo = new Cube(200,300,100,10);
                        f.Draw(cubo,Tools.PERSPECTIVA);
                        break;
                    case SUPERFICIE_PERSPECTIVA:
                        figura = SUPERFICIE_PERSPECTIVA;
                        f.Limpiar();
                        cubo = null;
                        superficie = new Superficie(200,300,100,100,true);
                        f.Draw(superficie,Tools.PERSPECTIVA);
                        break;
                    case SUPERFICIE_PARALELA:
                        figura = SUPERFICIE_PARALELA;
                        f.Limpiar();
                        superficie = new Superficie(100,100,10,10,false);
                        f.Draw(superficie,Tools.PARALELA);
                        break;
                    default:
                        break;
                        
                }//fin del switch
                return;
        }//Fin del switch
        
        switch(e.getKeyCode()){
            case KeyEvent.VK_1:
                Modalidad = TRANSLACION;
                System.out.println("Modo Actual: " + Modalidad);
                break;
            case KeyEvent.VK_2:
                Modalidad = ESCALA;
                System.out.println("Modo Actual: " + Modalidad);
                break;
            case KeyEvent.VK_3:
                Modalidad = ROTACION;
                System.out.println("Modo Actual: " + Modalidad);
                break;
            case KeyEvent.VK_4:
                Modalidad = ROTACION_SIMISMO;
                System.out.println("Modo Actual: " + Modalidad);
                break;
        }//Fin de Switch
        
        switch(Modalidad){
            case TRANSLACION:
                if(figura == CUBO_PARALELA || figura == CUBO_PERSPECTIVA){
                    if(e.getKeyCode() == KeyEvent.VK_D)
                        f.Translate(1, 0, 0, cubo);
                    if(e.getKeyCode() == KeyEvent.VK_A)
                        f.Translate(-1, 0, 0, cubo);
                    if(e.getKeyCode() == KeyEvent.VK_W)
                        f.Translate(0, -1, 0, cubo);
                    if(e.getKeyCode() == KeyEvent.VK_S)
                        f.Translate(0, 1, 0, cubo);
                    if(e.getKeyCode() == KeyEvent.VK_E)
                        f.Translate(0, 0, 1, cubo);
                    if(e.getKeyCode() == KeyEvent.VK_Q)
                        f.Translate(0, 0, -1, cubo);
                }// fin del if
                else if(figura == SUPERFICIE_PARALELA || figura == SUPERFICIE_PERSPECTIVA){
                    if(e.getKeyCode() == KeyEvent.VK_D)
                        f.Translate(1, 0, 0, superficie);
                    if(e.getKeyCode() == KeyEvent.VK_A)
                        f.Translate(-1, 0, 0, superficie);
                    if(e.getKeyCode() == KeyEvent.VK_W)
                        f.Translate(0, -1, 0, superficie);
                    if(e.getKeyCode() == KeyEvent.VK_S)
                        f.Translate(0, 1, 0, superficie);
                    if(e.getKeyCode() == KeyEvent.VK_E)
                        f.Translate(0, 0, 1, superficie);
                    if(e.getKeyCode() == KeyEvent.VK_Q)
                        f.Translate(0, 0, -1, superficie);
                }//fin del else if
                break;
                
            case ESCALA:
                if(figura == CUBO_PARALELA || figura == CUBO_PERSPECTIVA){
                    if(e.getKeyCode() == KeyEvent.VK_D)
                    {
                        if(sx < 1) sx = 1;
                        sx += 0.01;
                        f.Scale(sx, 1, 1, cubo);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_A)
                    {
                        if(sx > 1) sx = 1;
                        sx -= 0.01;
                        f.Scale(sx, 1, 1, cubo);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_W)
                    {
                        if(sy < 1) sy = 1;
                        sy += 0.01;
                        f.Scale(1, sy, 1, cubo);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_S)
                    {
                        if(sy > 1) sy = 1;
                        sy -= 0.01;
                        f.Scale(1, sy, 1, cubo);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_E)
                    {
                        if(sz < 1) sz = 1;
                        sz += 0.01;
                        f.Scale(1, 1, sz, cubo);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_Q)
                    {
                        if(sz > 1) sz = 1;
                        sz -= 0.01;
                        f.Scale(1, 1, sz, cubo);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_R)
                    {
                        if(sx < 1) sx = 1;
                        if(sy < 1) sy = 1;
                        if(sz < 1) sz = 1;
                        sx += 0.01; sy += 0.01; sz += 0.01;
                        f.Scale(sx, sy, sz, cubo);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_F)
                    {
                        if(sx > 1) sx = 1;
                        if(sy > 1) sy = 1;
                        if(sz > 1) sz = 1;
                        sx -= 0.01; sy -= 0.01; sz -= 0.01;
                        f.Scale(sx, sy, sz, cubo);
                    }
                }//fin del if
                else if (figura == SUPERFICIE_PARALELA || figura == SUPERFICIE_PERSPECTIVA){
                    if(e.getKeyCode() == KeyEvent.VK_D)
                    {
                        if(sx < 1) sx = 1;
                        sx += 0.01;
                        f.Scale(sx, 1, 1, superficie);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_A)
                    {
                        if(sx > 1) sx = 1;
                        sx -= 0.01;
                        f.Scale(sx, 1, 1, superficie);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_W)
                    {
                        if(sy < 1) sy = 1;
                        sy += 0.01;
                        f.Scale(1, sy, 1, superficie);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_S)
                    {
                        if(sy > 1) sy = 1;
                        sy -= 0.01;
                        f.Scale(1, sy, 1, superficie);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_E)
                    {
                        if(sz < 1) sz = 1;
                        sz += 0.01;
                        f.Scale(1, 1, sz, superficie);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_Q)
                    {
                        if(sz > 1) sz = 1;
                        sz -= 0.01;
                        f.Scale(1, 1, sz, superficie);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_R)
                    {
                        if(sx < 1) sx = 1;
                        if(sy < 1) sy = 1;
                        if(sz < 1) sz = 1;
                        sx += 0.01; sy += 0.01; sz += 0.01;
                        f.Scale(sx, sy, sz, superficie);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_F)
                    {
                        if(sx > 1) sx = 1;
                        if(sy > 1) sy = 1;
                        if(sz > 1) sz = 1;
                        sx -= 0.01; sy -= 0.01; sz -= 0.01;
                        f.Scale(sx, sy, sz, superficie);
                    }
                }//fin del else if
                break;
            
            case ROTACION:
                if(e.getKeyCode() == KeyEvent.VK_D)
                 {
                     //if(tetaX < 0) tetaX = 0;
                     tetaX++;
                     f.Rotate(tetaX, cubo, Tools.X_AXIS);
                 }
                 if(e.getKeyCode() == KeyEvent.VK_A)
                 {
                     //if(tetaX > 0) tetaX = 0;
                     tetaX--;
                     f.Rotate(tetaX, cubo, Tools.X_AXIS);
                 }
                 if(e.getKeyCode() == KeyEvent.VK_W)
                 {
                     //if(tetaY < 0) tetaY = 0;
                     tetaY++;
                     f.Rotate(tetaY, cubo, Tools.Y_AXIS);
                 }
                 if(e.getKeyCode() == KeyEvent.VK_S)
                 {
                     //if(tetaY > 0) tetaY = 0;
                     tetaY--;
                     f.Rotate(tetaY, cubo, Tools.Y_AXIS);
                 }
                 if(e.getKeyCode() == KeyEvent.VK_E)
                 {
                     //if(tetaZ < 0) tetaZ = 0;
                     tetaZ++;
                     f.Rotate(tetaZ, cubo, Tools.Z_AXIS);
                 }
                 if(e.getKeyCode() == KeyEvent.VK_Q)
                 {
                     //if(tetaZ > 0) tetaZ = 0;
                     tetaZ--;
                     f.Rotate(tetaZ, cubo, Tools.Z_AXIS);
                 }
                break;
                
            case ROTACION_SIMISMO:
                if(figura == CUBO_PARALELA || figura == CUBO_PERSPECTIVA)
                {
                    if(e.getKeyCode() == KeyEvent.VK_D)
                    {
                        //if(tetaX < 0) tetaX = 0;
                        tetaX2++;
                        f.Rotate2(tetaX2, cubo, Tools.X_AXIS);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_A)
                    {
                        //if(tetaX > 0) tetaX = 0;
                        tetaX2--;
                        f.Rotate2(tetaX2, cubo, Tools.X_AXIS);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_W)
                    {
                        //if(tetaY < 0) tetaY = 0;
                        tetaY2++;
                        f.Rotate2(tetaY2, cubo, Tools.Y_AXIS);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_S)
                    {
                        //if(tetaY > 0) tetaY = 0;
                        tetaY2--;
                        f.Rotate2(tetaY2, cubo, Tools.Y_AXIS);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_E)
                    {
                        //if(tetaZ < 0) tetaZ = 0;
                        tetaZ2++;
                        f.Rotate2(tetaZ2, cubo, Tools.Z_AXIS);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_Q)
                    {
                        //if(tetaZ > 0) tetaZ = 0;
                        tetaZ2--;
                        f.Rotate2(tetaZ2, cubo, Tools.Z_AXIS);
                    }
                }
                else if(figura == SUPERFICIE_PARALELA || figura == SUPERFICIE_PERSPECTIVA)
                {
                    if(e.getKeyCode() == KeyEvent.VK_D)
                    {
                        //if(tetaX < 0) tetaX = 0;
                        tetaX2++;
                        f.Rotate2(tetaX2, superficie, Tools.X_AXIS);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_A)
                    {
                        //if(tetaX > 0) tetaX = 0;
                        tetaX2--;
                        f.Rotate2(tetaX2, superficie, Tools.X_AXIS);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_W)
                    {
                        //if(tetaY < 0) tetaY = 0;
                        tetaY2++;
                        f.Rotate2(tetaY2, superficie, Tools.Y_AXIS);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_S)
                    {
                        //if(tetaY > 0) tetaY = 0;
                        tetaY2--;
                        f.Rotate2(tetaY2, superficie, Tools.Y_AXIS);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_E)
                    {
                        //if(tetaZ < 0) tetaZ = 0;
                        tetaZ2++;
                        f.Rotate2(tetaZ2, superficie, Tools.Z_AXIS);
                    }
                    if(e.getKeyCode() == KeyEvent.VK_Q)
                    {
                        //if(tetaZ > 0) tetaZ = 0;
                        tetaZ2--;
                        f.Rotate2(tetaZ2, superficie, Tools.Z_AXIS);
                    }
                }
                break;
        }//fin de Switch
    }// Fin de ProcesarTecla
    
    @Override
    public void paint(Graphics g){
        //super.paint(g);
        g.drawImage(buffer, 0, 0, this);
    }
    
    /*public void repaint(Graphics g){
        super.paint(g);
        g.drawImage(buffer, 0, 0, this);
    }*/
    
    @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {
        ProcesarTecla(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void run() {
        int t = 0;
        int t2 = 0;
        while(true){
            try{
                Thread.sleep(15);
                t++;
                t2++;
                
                if(t > 360){
                    t = 0;
                }
              
               if(t2 == 460)
                   t2 = 0;
                
                f.Rotate2(t, cubo, Tools.ALL_AXIS);
            }//fin del try
            catch (Exception e){
                
            }//fin del catch
            repaint();
            
        }//fin del while XD
    }
    
}
