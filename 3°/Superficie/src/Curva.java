import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Curva {
    int px=0, py=0,pz=0,dx=0,dy=0;
    float m=0,b=0;
    String proyeccion;
    ArrayList<Point3D> puntos3D;
    Graphics gra;

    public  Curva(int x, int y, int z, int puntos, int distY,int diferencial, String proyeccion){
        this.proyeccion=proyeccion;
        setPlano(x,y,z);
        puntos3D=new ArrayList<Point3D>();
        puntos3D=obtenerPuntos3D((float)puntos, distY, diferencial);
    }

    //diferencia en el plano inicial de una curva a otra, esta funcion define como sera la plataforma
    private ArrayList<Point3D> obtenerPuntos3D(Float cantPuntos, int distY, int diferencial){
        puntos3D=new ArrayList<Point3D>();
        float pi=3.14159f;
        for (float x=0;x<distY*pi;x+=pi/cantPuntos){//el numero de puntos que habra por cada curva
            puntos3D.add(new Point3D((float) (x*10) + diferencial, (float) (4-10*Math.cos(x))+diferencial +200,(float) (4-20 * Math.sin(x)) ));//rosa
        }
        return puntos3D;
    }

    public  void DibujarCurva(int puntos, Boolean estilo, BufferedImage b, Color c){
        gra=b.getGraphics();

        ArrayList<Point> puntos2D=puntos3D2D(puntos3D);
        //estilo==true? dibuja puntos y lineas: dibuja solo puntos
        for (int x=0;x<puntos2D.size()-1;x++){
            enlazarPuntos(puntos2D.get(x), puntos2D.get(x+1), estilo, c);
        }
    }

    public void enlazarPuntosMalla(ArrayList<Point3D> arrayAct, ArrayList<Point3D> arrayAnt, Boolean estilo, BufferedImage b, Color c){
        gra=b.getGraphics();
        ArrayList<Point> puntos2DAct=puntos3D2D(arrayAct);
        ArrayList<Point> puntos2DAnt=puntos3D2D(arrayAnt);
        int tamañoCurva=puntos2DAct.size();
        for (int x=0;x<tamañoCurva;x++){
            enlazarPuntos(puntos2DAct.get(x), puntos2DAnt.get(x),estilo, c);
            if(x+1<puntos2DAct.size())
                enlazarPuntos(puntos2DAct.get(x), puntos2DAnt.get(x+1),estilo, c);
        }
    }

    private void enlazarPuntos(Point a, Point b, Boolean estilo, Color c){
        if(estilo){
            lineaDDA(a.x, a.y, b.x, b.y, c);
        }
        else{
            putPixel(a.x,a.y, c); //sin enlazar
            putPixel(b.x,b.y, c);
        }
    }

    private ArrayList<Point> puntos3D2D(ArrayList<Point3D> puntos3D){
        ArrayList<Point> puntos2D=new ArrayList<Point>();
        switch (this.proyeccion){
            case "paralela":
                for(int x=0;x<puntos3D.size();x++)
                    puntos2D.add(paralela(puntos3D.get(x)));
                break;
            case "perspectiva":
                for(int x=0;x<puntos3D.size();x++)
                    puntos2D.add(perspectiva(puntos3D.get(x)));
                break;
        }
        return puntos2D;
    }

    public ArrayList<Point3D> getPuntos3D(){
        return puntos3D;
    }

    public void setPuntos3D(ArrayList<Point3D> puntos3D){
        this.puntos3D=puntos3D;
    }

    private void lineaDDA(int x0, int y0, int x1, int y1, Color c){
        dx = x1 - x0;
        dy = y1 - y0;//new Color(1,1,1);
        //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
        putPixel(x0,y0,c);

        if (Math.abs(dx) > Math.abs(dy)) {
            m = (float) dy / (float) dx;
            b = y0 - m*x0;
            if(dx<0)
                dx =  -1;
            else
                dx =  1;
            while (x0 != x1) {
                x0 += dx;
                y0 = Math.round(m*x0 + b);
                //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
                putPixel(x0,y0,c);
            }
        }
        else if (dy != 0) {
            m= (float) dx / (float) dy;
            b = x0 - m*y0;
            if(dy<0)
                dy =  -1;
            else
                dy =  1;
            while (y0 != y1) {
                y0 += dy;
                x0 = Math.round(m*y0 + b);
                //if(x0>xA&&x0<xB&&y0>yA&&y0<yB)
                putPixel(x0,y0,c);
            }
        }
    }

    private void putPixel(int x, int y, Color c){
        gra.setColor(c);
        gra.drawLine(x, y, x, y);
    }

    private Point perspectiva(Point3D punto){
        double u=-pz/(punto.z-pz);
        double x=px+(punto.x-px)*u;
        double y=py+(punto.y-py)*u;
        return  new Point((int)x,(int)y);
    }

    private Point paralela(Point3D punto){
        double x=punto.x-(px*punto.z)/pz;
        double y=punto.y-(py*punto.z)/pz;
        return new Point((int)x,(int)y);
    }

    public void setPlano(int px, int py, int pz){
        this.px=px;
        this.py=py;
        this.pz=pz;
    }
}
