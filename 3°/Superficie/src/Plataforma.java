import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Plataforma {
    ArrayList<Curva> malla;
    int tamX=0;
    float puntosPorLinea;
    public Plataforma(int px, int py, int pz, int tamX, int tamY,int puntosPorLinea, String proyeccion){
        malla = new ArrayList<Curva>();
        this.tamX=tamX;
        this.puntosPorLinea=puntosPorLinea/tamY;
        for(int x=0;x<this.tamX;x++){
            malla.add(new Curva(px, py, pz, (int)this.puntosPorLinea ,tamY, 30*x, proyeccion));//establecemos el plano, la funcion curva le da la forma
        }
    }

    public  void dibujarPlataforma(Boolean estilo, BufferedImage b, Color c){
        for(int x=0;x<tamX;x++){
            malla.get(x).DibujarCurva((int)puntosPorLinea,  estilo, b, c);
        }
        enlazarMalla(estilo, b, c);
    }

    private  void enlazarMalla(Boolean estilo, BufferedImage b, Color c){
        if(malla.size()>1){
            for (int x=1;x<malla.size();x++){//el sig dibujara hacia su anterior
                malla.get(x).enlazarPuntosMalla(malla.get(x).getPuntos3D(), malla.get(x-1).getPuntos3D(),estilo, b, c);
            }
        }
    }

    public void setMalla(ArrayList<Curva> malla){
        this.malla=malla;
    }

    public ArrayList<Curva> getMalla(){
        return this.malla;
    }
}
